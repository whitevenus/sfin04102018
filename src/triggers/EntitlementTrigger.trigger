trigger EntitlementTrigger on Entitlement (before insert, before update, before delete){
    
    if(MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt()){
        
        /**==============================================================================================================================
* Prevents Entitlement record from being created while the Execute Trigger Flag is set to true
* Prevents Entitlement record from being updated while the Execute Trigger Flag is set to true
* Prevents Entitlement record from being deleted while the Execute Trigger Flag is set to true
==================================================================================================================================**/
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isDelete || Trigger.isUpdate)){
            
            if(!test.isRunningTest()){
                
                //GeneralUtilities.preventCommit(Trigger.new);
                
            }
        }//end of Is Before Insert, Delete If Block
    }//end of ExecuteTriggerFlagFromApplicationOptionMdt If Block
}//end of Trigger
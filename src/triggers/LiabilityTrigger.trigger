trigger LiabilityTrigger on Liability__c (before delete){
    
    if(MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt()){
        
        /**==============================================================================================================================
* Prevents Liability record from being deleted while the Execute Trigger Flag is set to true
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isDelete){
            
            if(!test.isRunningTest()){
                
                GeneralUtilities.preventCommit(Trigger.new);
                
            }
        }//end of Is Before Delete If Block
    }//end of ExecuteTriggerFlagFromApplicationOptionMdt If Block
}//end of Trigger
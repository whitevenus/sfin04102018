trigger ConsultantTrigger on Consultant__c (before insert, before delete){
    
    if(MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt()){
        
        /**==============================================================================================================================
* Prevents Consultant record from being created while the Execute Trigger Flag is set to true
* Prevents Consultant record from being deleted while the Execute Trigger Flag is set to true
==================================================================================================================================**/
        if(Trigger.isBefore && (Trigger.isInsert || Trigger.isDelete)){
            
            if(!test.isRunningTest()){
                
                GeneralUtilities.preventCommit(Trigger.new);
                
            }
        }//end of Is Before Insert, Delete If Block
    }//end of ExecuteTriggerFlagFromApplicationOptionMdt If Block
}//end of Trigger
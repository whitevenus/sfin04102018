trigger CaseTrigger on Case (before insert, before update, before delete){
    
    if(MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt()){
        
        /**==============================================================================================================================
* Prevents Case record from being deleted while the Execute Trigger Flag is set to true
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isDelete){
            
            if(!test.isRunningTest()){
                
                GeneralUtilities.preventCommit(Trigger.new);
                
            }
        }//end of Is Before Delete If Block
        
        /**==============================================================================================================================
* Before Insert Section
* Validate if Audit and DC Case Record Types (Loan Audit, Reckless Lending, Prescription, Debt Counselling and Judgement) have duplicates
* Duplicate means a case with any status with same record type and same Related liability
* In case of Judgement, duplicate means a case with any status with same record type and same Judgement reference
* In case of DC, duplicate means a case with same record type and where status is not closed
* Debt Rescheduling record type will not be deduped as per business rule
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isInsert){
            
            try{
                
                Map<Id, List<Case>> duplicateCaseMap = new Map<Id, List<Case>>();
                Set <Id> liabilityIdSet = new Set <Id>();
                Set <Id> judgementIdSet = new Set <Id>();
                Set <Id> contactIdSet = new Set <Id>();
                
                List <Case> caseList = new List <Case>();
                List <Case> auditCaseList = new List <Case>();
                List <Case> debtCounsellingCaseList = new List <Case>();
                Boolean hasDebtCounsellingCase = False;
                
                //Split Trigger.new list into Map with RecordTypeId as Key
                //IMPORTANT: We need to refactor this code to make use of GeneralUtilities.convertListToMapWithStringKeyForLookup for Line 43
                Map<Id, List<Case>> caseRecordTypeMap = GeneralUtilities.convertListToMapWithIdKey(Trigger.new, StringConstants.RECORD_TYPE_ID_STRING_NAME);
                
                //Get Record Type Ids for Audit Cases in Org
                Id loanAuditRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_AUDIT_LOAN_AUDIT);
                Id recklessLendingRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_AUDIT_RECKLESS_LENDING);
                Id prescriptionRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_PRESCRIPTION);
                Id judgementRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_JUDGEMENT_EAO_SOFT_COLLECTION);
                
                //Get Record Type Ids for DC Cases in Org
                Id debtCounsellingRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_DEBT_COUNSELLING);
                
                //Create Audit Case Logic
                //logic needs to be done before validating
                //so that we validate after record type has been changed
                List <Metadata_Application_Option__mdt> applicationOptionMdtList = MetadataUtilities.getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
                
                if(applicationOptionMdtList != Null && applicationOptionMdtList.size() == 1){
                    
                    Set <Id> uniqueIdSet = new Set <Id>();
                    List <Case> caseCreationList = new List <Case>();
                    List <String> recordTypeList = applicationOptionMdtList[0].Audit_Case_Record_Types_for_Case_Trigger__c.split(StringConstants.SEMI_COLON_STRING);
                    
                    GeneralUtilities.customLogger('Inside New Case Logic');
                    
                    for(String recordType : recordTypeList){
                        
                        //IMPORTANT: We need to refactor this code to make use of GeneralUtilities.convertListToMapWithStringKeyForLookup
                        //After the refactoring Remove Line 149
                        //Get Map Values using Record Type Developer Name as Key instead
                        Id recordTypeId = GeneralUtilities.getRecordTypeId(recordType);
                        List <Case> currentCaseList = caseRecordTypeMap.get(recordTypeId);
                        
                        if(currentCaseList != Null && currentCaseList.Size() > 0){
                            
                            for(Case currentCase : currentCaseList){
                                
                                caseCreationList.add(currentCase);
                                uniqueIdSet.add(currentCase.Related_Liability__c);
                                
                            }
                        }
                    }//end recordTypeList If Block
                    
                    GeneralUtilities.customLogger('caseCreationList Size ' + caseCreationList.Size());
                    GeneralUtilities.customLogger('uniqueIdSet Size ' + uniqueIdSet.Size());
                    
                    if(caseCreationList != Null && caseCreationList.Size() > 0){
                        
                        //Find All Liabilities with record type Creditor related to Trigger.New Case List
                        List <Liability__c> liabilityList = [SELECT Id, Recurring_Amount__c, Financial_Assessment__c, Date_Loan_Prescribed__c
                                                             FROM Liability__c
                                                             WHERE Id IN: uniqueIdSet
                                                             AND RecordType.DeveloperName =: StringConstants.LIABILITY_RECORD_TYPE_CREDITOR_LOAN];
                        
                        GeneralUtilities.customLogger('liabilityList Size ' + liabilityList.Size());
                        
                        if(liabilityList != Null && liabilityList.Size() > 0){
                            
                            //Use this Map to get Financial Assessment Ids from liabilityList
                            Map<Id, List<Liability__c>> liabilityMap = GeneralUtilities.convertListToMapWithIdKey(liabilityList, StringConstants.FINANCIAL_ASSESSMENT_STRING_NAME);
                            
                            //Use this Map to create Map with Liability Id as Key
                            Map<Id, List<Liability__c>> liabilityWithIdKeyMap = GeneralUtilities.convertListToMapWithIdKey(liabilityList, StringConstants.LIABILITY_ID_STRING_NAME);
                            
                            //Use Financial Assessment Ids to find Income values
                            //Get Total Client Net Income for Financial Assessment
                            //Cannot use Roll Up Summary field on Financial Assessment because they are calculated asynchronously
                            List <Income__c> incomeList = [SELECT Net_Amount_Formula__c, Financial_Assessment__c
                                                           FROM Income__c
                                                           WHERE Financial_Assessment__c IN: liabilityMap.KeySet()
                                                           AND Beneficiary__c =: StringConstants.INCOME_BENEFICIARY_TYPE_CLIENT];
                            
                            GeneralUtilities.customLogger('incomeList Size ' + incomeList.Size());
                            
                            for(Case currentCase : caseCreationList){
                                
                                Boolean recordTypeChanged = False;
                                
                                //we need to find the assessment for this case
                                //at this point the Assessment field will be null
                                //only the case update flow will set the assessment field
                                List <Liability__c> currentLiabilityList = liabilityWithIdKeyMap.get(currentCase.Related_Liability__c);
                                
                                GeneralUtilities.customLogger('currentLiabilityList Size ' + currentLiabilityList.Size());
                                
                                if(currentLiabilityList != Null && currentLiabilityList.Size() == 1){
                                    
                                    Liability__c liability = currentLiabilityList[0];
                                    
                                    if(liability.Date_Loan_Prescribed__c != Null){
                                        
                                        currentCase.RecordTypeId = prescriptionRecordTypeId;
                                        recordTypeChanged = True;
                                        
                                    }
                                    
                                    if(incomeList != Null && incomeList.Size() > 0
                                       && recordTypeChanged == False){
                                           
                                           //Use this Map to get Financial Assessment Ids
                                           Map<Id, List<Income__c>> incomeMap = GeneralUtilities.convertListToMapWithIdKey(incomeList, StringConstants.FINANCIAL_ASSESSMENT_STRING_NAME);
                                           
                                           Id financialAssessmentId = liability.Financial_Assessment__c;
                                           
                                           if(financialAssessmentId != Null){
                                               
                                               //Get Assessment Liabilities and Incomes
                                               List <Liability__c> caseLiabilityList = liabilityMap.get(financialAssessmentId);
                                               List <Income__c> caseIncomeList = incomeMap.get(financialAssessmentId);
                                               
                                               if(caseLiabilityList != Null && caseLiabilityList.Size() > 0
                                                  && caseIncomeList != Null && caseIncomeList.Size() > 0){
                                                      
                                                      Decimal totalMonthlyDebtCommitments = StringConstants.DECIMAL_INITIAL_RECURRING_AMOUNT;
                                                      Decimal totalContactNetIncome = StringConstants.DECIMAL_INITIAL_RECURRING_AMOUNT;
                                                      
                                                      for(Liability__c currentLiability : caseLiabilityList){
                                                          
                                                          totalMonthlyDebtCommitments += currentLiability.Recurring_Amount__c;
                                                          
                                                      }
                                                      
                                                      for(Income__c income : caseIncomeList){
                                                          
                                                          totalContactNetIncome += income.Net_Amount_Formula__c;
                                                          
                                                      }
                                                      
                                                      if(totalMonthlyDebtCommitments > StringConstants.DECIMAL_INITIAL_RECURRING_AMOUNT
                                                         && totalContactNetIncome > StringConstants.DECIMAL_INITIAL_RECURRING_AMOUNT){
                                                             
                                                             Decimal currentDiRatio = totalMonthlyDebtCommitments / totalContactNetIncome;
                                                             
                                                             if(currentDiRatio > applicationOptionMdtList[0].Audit_RL_DI_Ratio_Threshold__c){
                                                                 
                                                                 currentCase.RecordTypeId = recklessLendingRecordTypeId;
                                                                 recordTypeChanged = True;
                                                                 
                                                             }
                                                         }
                                                  }//end caseIncomeList && caseLiabilityList If Block
                                           }//end financialAssessmentId If Block
                                       }//end incomeList If Block
                                    
                                    //Check if Record Type has already been changed
                                    if(!recordTypeChanged){
                                        
                                        currentCase.RecordTypeId = loanAuditRecordTypeId;
                                        
                                    }//end recordTypeChanged If Block
                                }//end currentLiabilityList If Block
                            }//end caseCreationList For Loop
                        }//end liabilityList If Block
                    }//end caseCreationList If Block
                }// end applicationOptionMdtList If Block
                
                for(Case currentCase : Trigger.new){
                    
                    //If we have Audit cases, add Liability Id to set
                    if(currentCase.RecordTypeId == loanAuditRecordTypeId || currentCase.RecordTypeId == recklessLendingRecordTypeId
                       || currentCase.RecordTypeId == prescriptionRecordTypeId){
                           
                           liabilityIdSet.add(currentCase.Related_Liability__c);
                           
                           //If we have Judgement cases, add Judgement Id to set
                       }else if(currentCase.RecordTypeId == judgementRecordTypeId){
                           
                           judgementIdSet.add(currentCase.Judgement_Reference__c);
                           
                           //If we have DC cases, set flag to true
                       }else if(currentCase.RecordTypeId == debtCounsellingRecordTypeId){
                           
                           hasDebtCounsellingCase = True;
                       }
                    
                    //Build set for Parent Contact Id
                    contactIdSet.add(currentCase.ContactId);
                }//end For Loop
                
                //Build a potential duplicate list for Audit Cases
                if((liabilityIdSet != Null && liabilityIdSet.Size() > 0) || (judgementIdSet != Null && judgementIdSet.Size() > 0)){
                    
                    auditCaseList = [SELECT RecordTypeId
                                     FROM Case
                                     WHERE (Related_Liability__c IN: liabilityIdSet OR Related_Liability__c = null)
                                     AND (Judgement_Reference__c IN: judgementIdSet OR Judgement_Reference__c = null)
                                     AND ContactId IN: contactIdSet];
                    
                    if(auditCaseList != Null && auditCaseList.Size() > 0){
                        
                        duplicateCaseMap = GeneralUtilities.convertListToMapWithIdKey(auditCaseList, StringConstants.RECORD_TYPE_ID_STRING_NAME);
                        
                    }
                }
                
                //Build a final duplicate list for DC Cases
                if(hasDebtCounsellingCase){
                    
                    debtCounsellingCaseList = [SELECT ContactId
                                               FROM Case
                                               WHERE ContactId IN: contactIdSet
                                               AND RecordTypeId =: debtCounsellingRecordTypeId
                                               AND Status != :StringConstants.CASE_STATUS_CLOSED];
                    
                }
                
                for(Case currentCase : Trigger.new){
                    
                    if((currentCase.RecordTypeId == loanAuditRecordTypeId || currentCase.RecordTypeId == recklessLendingRecordTypeId
                        || currentCase.RecordTypeId == prescriptionRecordTypeId || currentCase.RecordTypeId == judgementRecordTypeId)
                       && auditCaseList != Null && auditCaseList.Size() > 0 ){
                           
                           List <Case> duplicateCaseList = duplicateCaseMap.get(currentCase.RecordTypeId);
                           
                           if(duplicateCaseList != Null && duplicateCaseList.Size() > 0){
                               
                               currentCase.adderror(StringConstants.ERROR_MESSAGE_DUPLICATE_CASE);
                               
                           }
                       }
                    
                    if(debtCounsellingCaseList != Null && debtCounsellingCaseList.Size() > 0 &&
                       currentCase.RecordTypeId == debtCounsellingRecordTypeId){
                           
                           for(Case duplicateCase : debtCounsellingCaseList){
                               
                               if(duplicateCase.ContactId == currentCase.ContactId){
                                   
                                   currentCase.adderror(StringConstants.ERROR_MESSAGE_DUPLICATE_CASE);
                                   
                               }
                           }
                       }
                    
                }//end of for loop
                
            }catch(Exception ex){
                
                GeneralUtilities.logException(ex);
                
            }//end of Try Catch
        }//end of Is Before Insert If Block
        
        /**==============================================================================================================================
* Before Update Section
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isUpdate){
            
            try{
                
                List <Metadata_Application_Option__mdt> applicationOptionMdtList = MetadataUtilities.getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
                
                if(applicationOptionMdtList != Null && applicationOptionMdtList.size() == 1){
                    
                    List <Case> simplicityCaseList = new List <Case>();
                    
                    //IMPORTANT: We need to refactor this code to make use of GeneralUtilities.convertListToMapWithStringKeyForLookup in Line 165
                    Map<Id, List<Case>> caseRecordTypeMap = GeneralUtilities.convertListToMapWithIdKey(Trigger.new, StringConstants.RECORD_TYPE_ID_STRING_NAME);
                    List <String> recordTypeList = applicationOptionMdtList[0].Simplicity_Integration_Case_Record_Types__c.split(StringConstants.SEMI_COLON_STRING);
                    
                    for(String recordType : recordTypeList){
                        
                        //IMPORTANT: We need to refactor this code to make use of GeneralUtilities.convertListToMapWithStringKeyForLookup
                        //After the refactoring Remove Line 149
                        //Get Map Values using Record Type Developer Name as Key
                        Id recordTypeId = GeneralUtilities.getRecordTypeId(recordType);
                        List <Case> currentCaseList = caseRecordTypeMap.get(recordTypeId);
                        
                        if(currentCaseList != Null && currentCaseList.Size() > 0){
                            
                            simplicityCaseList.addAll(currentCaseList);
                            
                        }
                    }
                    
                    if(simplicityCaseList != Null && simplicityCaseList.Size() > 0){
                        
                        for(Case currentCase : simplicityCaseList){
                            
                            //Check if case Status has changed
                            if(currentCase.Status == applicationOptionMdtList[0].Case_Status_For_Simplicity_Integration__c
                               && currentCase.Status != trigger.oldMap.get(currentCase.Id).Status){
                                   
                                   //Check if Contact already exists in Simplicity
                                   List <Contact> contactList = [SELECT Id
                                                                 FROM Contact
                                                                 WHERE Id =: currentCase.ContactId
                                                                 AND Simplicity_Reference_Number__c = Null
                                                                 LIMIT 1];
                                   
                                   if(contactList != Null && contactList.Size() == 1){
                                       
                                       //Create contact and related information in simplicity
                                       IntegrationHandler.createContactRelatedInfoInSimplicity(currentCase.ContactId, currentCase.Id, currentCase.Financial_Assessment__c);
                                       
                                   }
                               }
                        }//end of for loop
                    }//end of debtCounsellingCaseList If Block
                    
                    //Get Record Type Ids for Aftercare in Org
                    Id requestRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_REQUESTS);
                    Id balanceRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_BALANCE_MANAGEMENT);
                    Id complaintsRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_COMPLAINTS);
                    
                    //Validate Aftercare Case
                    for(Case currentCase : Trigger.new){
                        
                        if(currentCase.RecordTypeId == requestRecordTypeId || currentCase.RecordTypeId == balanceRecordTypeId
                           || currentCase.RecordTypeId == complaintsRecordTypeId){
                               
                               if(currentCase.Subject != trigger.oldMap.get(currentCase.Id).Subject){
                                   //Add Logic
                               }else{
                                   
                                   if(currentCase.ParentId == Null){
                                       
                                       currentCase.adderror(StringConstants.ERROR_MESSAGE_NO_PARENT_FOR_AFTERCARE_CASE);
                                       
                                   }
                               }
                           }
                    }
                }//end of applicationOptionMdtList If Block
            }catch(Exception ex){
                
                GeneralUtilities.logException(ex);
                
            }//end of Try Catch
        }//end of Is Before Update If Block
    }//end of ExecuteTriggerFlagFromApplicationOptionMdt If Block
}//end of Trigger
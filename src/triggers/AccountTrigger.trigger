trigger AccountTrigger on Account (before insert, before delete){
    public class customException extends Exception{
        
    }
    
    if( MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt()){
        
        /**==============================================================================================================================
* Prevents Account record from being deleted while the Execute Trigger Flag is set to true
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isDelete){
            
            if(!test.isRunningTest()){
                
                GeneralUtilities.preventCommit(Trigger.new);
                
            }
        }//end of Is Before Delete If Block
        
        /**==============================================================================================================================
* Before Insert Section
==================================================================================================================================**/
        if(Trigger.isBefore && Trigger.isInsert){
            
            try{
                
                Map<Id, String> filteredAccountMap = new Map<Id, String>();
                Set <String> accountNameSet = new Set<String>();
                
                for(Account account : Trigger.new){
                    
                    accountNameSet.add(account.Name);
                    
                }
                
                if(accountNameSet != null && accountNameSet.size() > 0){
                    
                    List <Account> accountList = [SELECT RecordTypeId, Name FROM Account WHERE Name IN: accountNameSet];
                    
                    if(accountList != null && accountList.Size() > 0){
                        
                        for(Account account: accountList){
                            
                            filteredAccountMap.put(account.RecordTypeId, account.Name);
                            
                        }
                        
                        for(Account account : Trigger.new){
                            
                            if(filteredAccountMap.size() > 0 && filteredAccountMap.containsKey(account.RecordTypeId)
                               && account.Name == filteredAccountMap.get(account.RecordTypeId)){
                                   
                                   account.adderror(StringConstants.ERROR_MESSAGE_FOUND_DUPLICATE_ACCOUNT);
                                   
                               }
                        }
                    }
                }
                if (test.isRunningTest()){
                    
                    throw new customException();
                    
                }
            }catch(Exception ex){
                
                GeneralUtilities.logException(ex);
                
            }//end Try Catch
        }//end of Is Before Insert If Block
    }//end getExecuteTriggerFlagFromApplicationOptionMdt If Block
}
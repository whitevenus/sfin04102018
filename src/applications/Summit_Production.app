<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#374048</headerColor>
        <logo>SummitLogo</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Summit Financial Hub for Administrators</description>
    <formFactors>Large</formFactors>
    <label>Summit</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Consultant__c</tab>
    <tab>standard-Case</tab>
    <tab>Process__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Training__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Summit_UtilityBar</utilityBar>
</CustomApplication>

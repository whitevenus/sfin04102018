<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Income_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Income_Compact_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Income_Compact_Layout</fullName>
        <fields>Contact_Name__c</fields>
        <fields>Financial_Assessment__c</fields>
        <fields>Beneficiary__c</fields>
        <fields>Type__c</fields>
        <fields>Net_Amount_Formula__c</fields>
        <label>Income Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Beneficiary__c</fullName>
        <externalId>false</externalId>
        <label>Beneficiary</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Client</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Contact_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Financial_Assessment__r.Contact__r.FirstName + &apos; &apos; + Financial_Assessment__r.Contact__r.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Incomes</relationshipLabel>
        <relationshipName>Incomes</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Financial_Assessment__c</fullName>
        <externalId>false</externalId>
        <label>Financial Assessment</label>
        <referenceTo>Financial_Assessment__c</referenceTo>
        <relationshipLabel>Incomes</relationshipLabel>
        <relationshipName>Incomes</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Gross_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Gross Amount</label>
        <precision>18</precision>
        <required>true</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Last_Worked_On_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Last Worked On By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Incomes</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Last_Worked_On_Date__c</fullName>
        <externalId>false</externalId>
        <label>Last Worked On Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Net_Amount_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Gross_Amount__c -  Total_Amount_Deductions__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Net Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Net Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>OLD_BUDGET_ID__c</fullName>
        <externalId>false</externalId>
        <label>OLD BUDGET ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Simplicity_Reference_Number__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>Simplicity Reference Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Total_Amount_Deductions_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Total_Amount_Deductions__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Amount Deductions</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Amount_Deductions__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Total Amount Deductions</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Salary</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Extra Work</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Interest</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Investment Income</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Maintenance as per Court Order</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Overtime</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Rental Income</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Subsidy</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Travel allowance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Trust Income</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Voluntary Maintenance</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Commission</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Unspecified by Simplicity</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Income</label>
    <nameField>
        <displayFormat>INCOME-{000000}</displayFormat>
        <label>Income Reference</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Incomes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Financial_Assessment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Beneficiary__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Net_Amount_Formula__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Financial_Assessment__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Beneficiary__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Net_Amount_Formula__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Financial_Assessment__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Beneficiary__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Type__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Net_Amount_Formula__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Financial_Assessment__c</searchFilterFields>
        <searchFilterFields>Beneficiary__c</searchFilterFields>
        <searchFilterFields>Type__c</searchFilterFields>
        <searchFilterFields>Net_Amount_Formula__c</searchFilterFields>
        <searchResultsAdditionalFields>Financial_Assessment__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Beneficiary__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Net_Amount_Formula__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Financial_Assessment_Is_Active</fullName>
        <active>true</active>
        <errorConditionFormula>Financial_Assessment__r.Is_Active__c = False</errorConditionFormula>
        <errorMessage>Please note this Income cannot be changed because its related financial assessment is not active.</errorMessage>
    </validationRules>
</CustomObject>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC RL 3D</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Hi {0}  NB: Our legal team needs your approval to challenge {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} with reckless 
lending when we submit your payment plan to the Magistrate’s Court. Please check your email or give us a call for details.
 Summit Financial Partners | 087 806 1011 | fw@summitfin.co.za</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">SMS Template Message</value>
    </values>
</CustomMetadata>

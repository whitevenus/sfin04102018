<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC 3C</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">Client’s final payment plan,Why balances don’t always match</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DC 3C – Final Payment Plan – Client has Counter Proposals (Mag Court)</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC 2A</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">Client’s draft payment plan</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DC 2A – Provisional Payment Plan sent to CPs</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC 3C</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Creditors requested higher instalments on your payment plan. We must proceed to Court. NB: Check your email or call to SIGN your plan. Summit|0878061011</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">SMS Template Message</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>COL 4C</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Hi {0}  It seems you’ve missed another debt counselling payment. This is a FINAL WARNING to catch up on all outstanding payments before your application is suspended. If payment has been made, email proof of payment to payments@summitfin.co.za or fax to 086 248 8273. Summit Financial Partners | 087 806 1011</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">SMS Template Message</value>
    </values>
</CustomMetadata>

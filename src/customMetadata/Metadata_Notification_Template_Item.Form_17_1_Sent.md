<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Form 17.1 Sent</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">This is a test SMS message from Salesforce for {0}</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">SMS Template Message</value>
    </values>
</CustomMetadata>

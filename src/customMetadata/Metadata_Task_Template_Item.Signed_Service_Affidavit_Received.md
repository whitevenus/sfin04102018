<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Signed Service Affidavit Received</label>
    <protected>false</protected>
    <values>
        <field>Due_Date_in_Days__c</field>
        <value xsi:type="xsd:double">30.0</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>Parent_Record_Next_Status__c</field>
        <value xsi:type="xsd:string">Service Affidavit Sent</value>
    </values>
    <values>
        <field>Related_Object_Record_Type__c</field>
        <value xsi:type="xsd:string">Legal_DC_Court</value>
    </values>
    <values>
        <field>Related_Object__c</field>
        <value xsi:type="xsd:string">Process</value>
    </values>
    <values>
        <field>Static_ID__c</field>
        <value xsi:type="xsd:string">TASK_TEMP_SIGNED_SERVICE_AFFIDAVIT</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Signed Service Affidavit Received</value>
    </values>
</CustomMetadata>

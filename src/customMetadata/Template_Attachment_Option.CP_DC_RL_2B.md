<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CP DC RL 2B</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">RL 05 - Follow up: Allegation of Reckless Lending ito NCA,Client POA</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">CP DC RL 2B: 1st follow up – Challenge letter</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC NCT 3B</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">Client NCT Order,Client Final payment plan,o	Balance Dispute infographic</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DC NCT 3B: NCT Outcome – Successful</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AFT 8B</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Please remember documents needed for your credit report dispute. Check your email or call for details. Summit | 0878061011 | complaints@summitfin.co.za</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">SMS Template Message</value>
    </values>
</CustomMetadata>

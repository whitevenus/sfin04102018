<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>What_we_need_from_you</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">https://s3.ap-south-1.amazonaws.com/summit-public-assets/What_we_need_from_you.jpg</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Email Template Image</value>
    </values>
</CustomMetadata>

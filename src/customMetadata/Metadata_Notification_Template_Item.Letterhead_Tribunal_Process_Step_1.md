<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Letterhead_Tribunal_Process_Step_1</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">https://s3.ap-south-1.amazonaws.com/summit-public-assets/Letterhead_Tribunal_Process_Step_1.png</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Email Template Image</value>
    </values>
</CustomMetadata>

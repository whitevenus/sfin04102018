<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>COL 2C</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">Debit Order Cancellation Form</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">COL 2C – Second follow up – Debit Order Cancellation</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DC RL 3A</label>
    <protected>false</protected>
    <values>
        <field>Attachment_Name__c</field>
        <value xsi:type="xsd:string">Credit Provider confirmation letter</value>
    </values>
    <values>
        <field>Template_Name__c</field>
        <value xsi:type="xsd:string">DC RL 3A: Full account written off</value>
    </values>
</CustomMetadata>

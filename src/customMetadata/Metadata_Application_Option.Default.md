<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Aftercare_Case_Record_Types__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Audit_Case_Record_Types_for_Case_Trigger__c</field>
        <value xsi:type="xsd:string">Audit_Loan_Audit;Audit_Reckless_Lending;Prescription</value>
    </values>
    <values>
        <field>Audit_RL_DI_Ratio_Threshold__c</field>
        <value xsi:type="xsd:double">0.8</value>
    </values>
    <values>
        <field>Case_Status_For_Simplicity_Integration__c</field>
        <value xsi:type="xsd:string">Form 17.1 Sent</value>
    </values>
    <values>
        <field>Client_Stop_Order_Selected_Creditors__c</field>
        <value xsi:type="xsd:string">Standard Bank</value>
    </values>
    <values>
        <field>DC_DI_Ratio_Threshold__c</field>
        <value xsi:type="xsd:double">0.25</value>
    </values>
    <values>
        <field>DC_Lenders_List__c</field>
        <value xsi:type="xsd:string">African Bank</value>
    </values>
    <values>
        <field>DR_DI_Ratio_Threshold__c</field>
        <value xsi:type="xsd:double">0.4</value>
    </values>
    <values>
        <field>DR_Lenders_List__c</field>
        <value xsi:type="xsd:string">African Bank</value>
    </values>
    <values>
        <field>DR_Monthly_Proportion_From_Lenders__c</field>
        <value xsi:type="xsd:double">0.4</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_CA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_CG__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_COB__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_CR__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_DCO__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_DEB_OM__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_DRP_REP__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_DRP_RES__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_EAO__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_FAL__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_FA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_FIN_PRO__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_Form_16__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_ID__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_LA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_MC__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_NCT_POA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_PAL__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_PAY_CO__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_POA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_PROV_PRO__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_PSS__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_Passport__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_Payslip__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_RB__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_SA__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Document_Validity_In_Days_TI_138__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>ExecuteTriggerFlag__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Loggly_Callout_Log_Level__c</field>
        <value xsi:type="xsd:string">DEBUG</value>
    </values>
    <values>
        <field>Simplicity_Callout_Access_Key__c</field>
        <value xsi:type="xsd:string">0BAE958C-44ED-4055-A485-B0431AFBD251</value>
    </values>
    <values>
        <field>Simplicity_Integration_Case_Record_Types__c</field>
        <value xsi:type="xsd:string">Debt_Counselling;Debt_Rescheduling</value>
    </values>
    <values>
        <field>Subject_ADR__c</field>
        <value xsi:type="xsd:string">Legal - ADR Process for</value>
    </values>
    <values>
        <field>Subject_Account_Collections_FWA__c</field>
        <value xsi:type="xsd:string">Account Collections Process for</value>
    </values>
    <values>
        <field>Subject_Account_Collections_Onsite__c</field>
        <value xsi:type="xsd:string">Account Collections Process for</value>
    </values>
    <values>
        <field>Subject_Aftercare_Balance_Management__c</field>
        <value xsi:type="xsd:string">Aftercare Balance Management Case for Contact</value>
    </values>
    <values>
        <field>Subject_Aftercare_Complaints__c</field>
        <value xsi:type="xsd:string">Aftercare Complaints Case for Contact</value>
    </values>
    <values>
        <field>Subject_Aftercare_Requests__c</field>
        <value xsi:type="xsd:string">Aftercare Requests Case for Contact</value>
    </values>
    <values>
        <field>Subject_Court__c</field>
        <value xsi:type="xsd:string">Legal - Audit Court Process for</value>
    </values>
    <values>
        <field>Subject_DC_Court__c</field>
        <value xsi:type="xsd:string">Legal - Magistrate Court Process for</value>
    </values>
    <values>
        <field>Subject_DC__c</field>
        <value xsi:type="xsd:string">Debt Counselling Case for Contact</value>
    </values>
    <values>
        <field>Subject_DR__c</field>
        <value xsi:type="xsd:string">Debt Rescheduling Case for Contact</value>
    </values>
    <values>
        <field>Subject_Judgement_EAO_Soft_Collect__c</field>
        <value xsi:type="xsd:string">Judgement / EAO / Collection Case for Contact</value>
    </values>
    <values>
        <field>Subject_Loan_Audit__c</field>
        <value xsi:type="xsd:string">Loan Audit Case for Contact</value>
    </values>
    <values>
        <field>Subject_NCR__c</field>
        <value xsi:type="xsd:string">Legal - NCR Process for</value>
    </values>
    <values>
        <field>Subject_Ombudsman__c</field>
        <value xsi:type="xsd:string">Legal - Ombudsman Process for</value>
    </values>
    <values>
        <field>Subject_Prescription__c</field>
        <value xsi:type="xsd:string">Prescription Case for Contact</value>
    </values>
    <values>
        <field>Subject_Reckless_Lending__c</field>
        <value xsi:type="xsd:string">Reckless Lending Case for Contact</value>
    </values>
    <values>
        <field>Subject_Tribunal_NCT__c</field>
        <value xsi:type="xsd:string">Legal - Tribunal (NCT) Process for</value>
    </values>
    <values>
        <field>Vodacom_Integration_Password__c</field>
        <value xsi:type="xsd:string">welcome123</value>
    </values>
    <values>
        <field>Vodacom_Integration_Username__c</field>
        <value xsi:type="xsd:string">summit</value>
    </values>
</CustomMetadata>

<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Upload Client in Simplicity</label>
    <protected>false</protected>
    <values>
        <field>Due_Date_in_Days__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Parent_Record_Next_Status__c</field>
        <value xsi:type="xsd:string">Client Uploaded to Simplicity</value>
    </values>
    <values>
        <field>Related_Object_Record_Type__c</field>
        <value xsi:type="xsd:string">Account_Collections_Onsite</value>
    </values>
    <values>
        <field>Related_Object__c</field>
        <value xsi:type="xsd:string">Process</value>
    </values>
    <values>
        <field>Static_ID__c</field>
        <value xsi:type="xsd:string">TASK_TEMP_UPLOAD_CLIENT_IN_SIMPLICITY</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Upload Client in Simplicity</value>
    </values>
</CustomMetadata>

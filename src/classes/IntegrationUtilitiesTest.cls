@isTest
public class IntegrationUtilitiesTest {
    
    
    static testMethod void test_getSummitAccessCredentialsDoc(){
        
        //Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpAccessTokenGrantResponse());
		Test.setMock(HttpCalloutMock.class, new MockHttpSimplicityCalloutResponse());
        //Call method to test
        //This causes a fake response to be sent from the class that implements HttpCalloutMock. 

        HttpRequest httpRequest = new HttpRequest();
        HttpResponse httpResponse = IntegrationUtilities.serviceCallout(httpRequest);
        IntegrationUtilities.getSummitAccessCredentials();

    }

  
    }
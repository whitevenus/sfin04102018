/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 30/07/2018
* Description: Represents App.SummitFin Liability Schema to be sent to Salesforce

* Change History:
* None
===================================================================================================================================**/
public class IntegrationBeanSummitFinLiability {
    
    Public String Record_Id;
    Public String Financial_Assessment;
    Public String RecordType;
    Public String Frequency;
    Public String Loan_Reference_Number;
    Public Decimal Arrears_Amount;
    Public Decimal Credit_Limit;
    Public Decimal Recurring_Amount;
    Public Decimal Interest_Rate;
    Public Decimal Loan_Amount_Granted;
    Public Decimal Loan_Capital_Outstanding;
    Public Decimal Monthly_Service_Fees;
    Public Integer Number_of_Missed_Payments;
    Public Integer Loan_Term_in_months;
    Public String Client_Behaviour;
    Public String Credit_Provider;
    Public String Effective_Start_Date;
    Public String Date_Loan_Granted;
    Public String Last_Payment_Date;
    Public String Account_Type_Code;
    Public String Comments;
    Public String Type;
    Public String Current_Payment_Method;
    Public String Agreement_Type;
    Public Boolean Active_in_SummitFin;
    
}
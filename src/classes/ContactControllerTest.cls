/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 22/06/2018
* Description: Provides test methods for Contact Controller class

* Change History:
* None
===================================================================================================================================**/
@isTest
public class ContactControllerTest {
    
    private static Account myAccount;
    private static Consultant__c myConsultant;
    private static Contact myPartnerContact;
    private static Contact myContact;
    private static Financial_Assessment__c myFinancialAssessment;
    private static Liability__c myLiabilityHouseholdExpense;
    private static Liability__c myLiabilityCreditorLoan;
    private static Case myCase;
    
    static testMethod void test_Insert_Account_Consultant_Contact_FinancialAssessment(){
        
        myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        myPartnerContact = TestDataGenerator.setContactPartnerFields(myAccount.Id, myConsultant.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_PARTNER));
        INSERT myPartnerContact;
        
        myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id, myPartnerContact.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT myContact;
        
        myFinancialAssessment = TestDataGenerator.setFinancialAssessmentFields(myContact.Id);
        INSERT myFinancialAssessment;
        
    }
    
    static testMethod void test_Insert_Liability_Case(){
        
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        
        myLiabilityHouseholdExpense = TestDataGenerator.setLiabilityHouseholdExpenseFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE),myFinancialAssessment.Id);
        INSERT myLiabilityHouseholdExpense;
        
        myLiabilityCreditorLoan = TestDataGenerator.setLiabilityCreditorLoanFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_CREDITOR_LOAN),myFinancialAssessment.Id);
        INSERT myLiabilityCreditorLoan;
        
        myCase = TestDataGenerator.setCaseFields(GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_AUDIT_RECKLESS_LENDING),myContact.Id);
        INSERT myCase;
        
    }
    
    static testMethod void test_ContactControllerSnapshot_Contact(){
        
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        
        Task myContactTask = TestDataGenerator.setContactTaskFields(myContact.Id);
        myContactTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_CONTACT;
        Insert myContactTask;
        myContactTask.Status = StringConstants.TEST_DATA_CASE_STATUS_COMPLETED;
        Update myContactTask;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(myContact);
        ApexPages.currentPage().getParameters().put('Id',myContact.Id);
        
        ContactController myContactController = new ContactController(sc);
        myContactController.Snapshot();
        
        ContactController myContactController1 = new ContactController(sc);
        myContactController1.documentUploadHandler();
        
    }
    
    static testMethod void test_ContactControllerSnapshot_Case(){
        
        test_Insert_Liability_Case();
        
        Task myCaseTask = TestDataGenerator.setCaseTaskFields(myCase.Id, myCase.OwnerId);
        myCaseTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_CASE;
        Insert myCaseTask;
        myCaseTask.Status = StringConstants.TEST_DATA_CASE_STATUS_COMPLETED;
        Update myCaseTask;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(myContact);
        ApexPages.currentPage().getParameters().put('Id',myContact.Id);
        
        ContactController myContactController = new ContactController(sc);
        myContactController.Snapshot();
        
        ContactController myContactController1 = new ContactController(sc);
        myContactController1.documentUploadHandler();
        
    }
    
    static testMethod void test_ContactControllerSnapshot_Process(){
        
        test_Insert_Liability_Case();
        
        Process__c myProcess = TestDataGenerator.setProcessCaseFields(GeneralUtilities.getRecordTypeId(StringConstants.PROCESS_RECORD_TYPE_LEGAL_NCR), myCase.Id, myCase.OwnerId);
        INSERT myProcess;
        
        Task myProcessTask = TestDataGenerator.setProcessTaskFields(myProcess.Id, myCase.OwnerId);
        myProcessTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_PROCESS;
        myProcessTask.Parent_Record_Next_Status__c = StringConstants.TEST_DATA_CASE_STATUS;
        myProcessTask.Related_Object_Record_Type__c = StringConstants.PROCESS_RECORD_TYPE_LEGAL_NCR;
        Insert myProcessTask;
        myProcessTask.Status = StringConstants.TEST_DATA_CASE_STATUS_COMPLETED;
        Update myProcessTask;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(myContact);
        ApexPages.currentPage().getParameters().put('Id',myContact.Id);
        
        ContactController myContactController = new ContactController(sc);
        myContactController.Snapshot();
        
        ContactController myContactController1 = new ContactController(sc);
        myContactController1.documentUploadHandler();
        
    }
    
}
@isTest
public class TriggerTest {
    
    private static Account myAccount;
    private static Consultant__c myConsultant;
    private static Contact myPartnerContact;
    private static Contact myContact;
    private static Financial_Assessment__c myFinancialAssessment;
    private static Liability__c myLiabilityHouseholdExpense;
    private static Liability__c myLiabilityCreditorLoan;
    private static Case myCase;
    
    static testMethod void test_Insert_Account_Consultant_Contact_FinancialAssessment(){
        
        myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        myPartnerContact = TestDataGenerator.setContactPartnerFields(myAccount.Id, myConsultant.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_PARTNER));
        INSERT myPartnerContact;
        
        myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id, myPartnerContact.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT myContact;
        
        myFinancialAssessment = TestDataGenerator.setFinancialAssessmentFields(myContact.Id);
        INSERT myFinancialAssessment;
        
    }
    
    static testMethod void test_Insert_Liability_Case(){
        
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        
        myLiabilityHouseholdExpense = TestDataGenerator.setLiabilityHouseholdExpenseFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE),myFinancialAssessment.Id);
        INSERT myLiabilityHouseholdExpense;
        
        myLiabilityCreditorLoan = TestDataGenerator.setLiabilityCreditorLoanFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_CREDITOR_LOAN),myFinancialAssessment.Id);
        INSERT myLiabilityCreditorLoan;
        
        myCase = TestDataGenerator.setCaseFields(GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_AUDIT_RECKLESS_LENDING),myContact.Id);
        INSERT myCase;
        
    }
    
    static testMethod void deleteContact(){
        
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        
        DELETE myFinancialAssessment;
        DELETE myContact;
        
    }
    
    static testMethod void deleteEntitlement(){
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        Entitlement ent = TestDataGenerator.setEntitlementFields(myAccount.Id);
        INSERT ent;
        DELETE ent;
    }
    
    static testMethod void deleteDocumentLink(){
        test_Insert_Account_Consultant_Contact_FinancialAssessment();
        Document_Link__c docLink = TestDataGenerator.setDocumentLinkFields(myContact);
        INSERT docLink;
        DELETE docLink;
        
    }
    
    static testMethod void deleteLiability(){
        
        test_Insert_Liability_Case();
        DELETE myLiabilityCreditorLoan;
        
    }
    
    
}
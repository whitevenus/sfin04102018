/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 30/07/2018
* Description: Represents App.SummitFin Liability Schema to be sent to Salesforce

* Change History:
* None
===================================================================================================================================**/
public class IntegrationBeanSummitFinJudgement {
    
    Public String Financial_Assessment;
    Public String Collecting_Attorney;
    Public String Date_Issued;
    Public String Credit_Provider;
    Public Decimal Amount;
    Public String Case_Number;
    Public String Court;
    
}
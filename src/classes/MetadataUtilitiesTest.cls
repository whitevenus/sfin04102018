@isTest
public class MetadataUtilitiesTest {
	
    static testMethod void test_getQueryResultForMetadata() {
        
       String whereClause = StringConstants.QUERY_FROM_METADATA_APPLICATION_OPTION + StringConstants.ENDPOINT_GET_DOCUMENT_LINKS + StringConstants.QUERY_END_DELIMITER;
       MetadataUtilities.getQueryResultForMetadata(StringConstants.METADATA_APPLICATION_OPTION_NAME, whereClause);
   }
    
    static testMethod void test_getApplicationOptionMetadataList(){
    	MetadataUtilities.getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
    }
    
    static testMethod void test_getExecuteTriggerFlagFromApplicationOptionMdt(){
        MetadataUtilities.getExecuteTriggerFlagFromApplicationOptionMdt();
    }
    
}
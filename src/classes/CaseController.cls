/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 02/08/2018
* Description: Extends logic for Case Object

* Change History:
* None
===================================================================================================================================**/
public class CaseController {
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided
* Invocable Methods only take collections as parameters

1. List <Case> caseListP this is our Case list sent from the onSmsCaseInvocable Process Builder
===================================================================================================================================**/
    @InvocableMethod
    public static void buildCaseSmsRequestInvocable(List <Case> caseListP){
        
        try{
            
            if(caseListP != null && caseListP.size() > 0){
                
                //List will always have 1 index element
                Case currentCase = caseListP.iterator().next();
                
                handleCommonSmsInvocableLogic(currentCase, StringConstants.EMPTY_STRING);
                
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
    }
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided

1. Case caseP this is our case sent from the Process Builder
2. queryKeyP is the key to find the meta data values for the sms that needs to be sent
===================================================================================================================================**/
    public static void handleCommonSmsInvocableLogic(Case caseP, String queryKeyP){
        
        //Only send callout from Production
        //because Sandbox SMS Notifications
        //will reduce Summit SMS Credits
        
        //IMPORTANT: Uncomment Line 49 Before Production Deployment
        //if(!GeneralUtilities.isSandBox()){
        
        //Find Mobile Number for Case Contact
        List<Contact> contactList = [SELECT MobilePhone, Name
                                     FROM Contact
                                     WHERE Id =: caseP.ContactId
                                     LIMIT 1];
        
        //Only send SMS if Mobile Number for Case Contact is found
        if(contactList != null && contactList.size() == 1 && contactList[0].MobilePhone != null && contactList[0].MobilePhone != ''){
            
            List<String> messageList = new List<String>();
            String queryKey;
            String creditProviderName;
            String liabilityType;
            
            if(String.isNotBlank(queryKeyP)){
                
                queryKey = queryKeyP;
                
            }
            
            List <Liability__c> liabilityList = [SELECT Credit_Provider__r.Name, Type__c
                                                 FROM Liability__c
                                                 WHERE Id =: caseP.Related_Liability__c
                                                 LIMIT 1];
            
            if(liabilityList != Null && liabilityList.Size() == 1){
                
                creditProviderName = liabilityList[0].Credit_Provider__r.Name;
                liabilityType = liabilityList[0].Type__c;
                
            }
            
            String clientName = contactList[0].Name;
            
            messageList.add(clientName);
            messageList.add(creditProviderName);
            messageList.add(liabilityType);
            
            //Get SMS Template from Metadata
            List <Metadata_Notification_Template_Item__mdt> smsTemplateItemMdtList = MetadataUtilities.getNotificationTemplateItemMetadataList(queryKey);
            
            if(smsTemplateItemMdtList != Null && smsTemplateItemMdtList.Size() ==1){
                
                //Format Number to meet Vodacom endpoint requirements
                //Please ensure all future changes are in line with Vodacom Service
                String phoneNumber = StringConstants.MOBILE_NUMBER_COUNTRY_VALUE
                    + contactList[0].MobilePhone.removeStart(StringConstants.MOBILE_NUMBER_START_VALUE);
                
                String message = String.format(smsTemplateItemMdtList[0].Description__c, messageList);
                
                //Send SMS
                if(String.isNotBlank(message)){
                    
                    IntegrationHandler.sendSmsToVodacomService(caseP.ContactId,
                                                               phoneNumber,
                                                               caseP.Status,
                                                               message);
                    
                }
                
            }// end of smsTemplateItemMdtList if block
        }//end contact list if block
        
        //IMPORTANT: Uncomment Line 89 Before Production Deployment
        //}
        
    }
}
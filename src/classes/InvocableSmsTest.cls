@isTest
public class InvocableSmsTest {
    static testMethod void test_InvocableSmsTestProcess(){
        Process__c testProcess = new Process__c();
        List<Process__c> processList = new List<Process__c>();
        processList.add(testProcess);
        
        //Testing invocable SMS Classes for collections
        
        InvocableSmsDcCol1A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol1B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol1C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol1D.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol2A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol2B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol2C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol3A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol3B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol3C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol3D.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4D.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4E.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol4F.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol5A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcCol5B.buildProcessSmsRequestInvocable(processList);
        
        //Testing invocable SMS Classes for DC Mag Court
        
        InvocableSmsDcMag1A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag2A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag2B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag2C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag3A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag3B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcMag4A.buildProcessSmsRequestInvocable(processList);
        
        //Testing invocable SMS Classes for NCT Tribunal
        
        InvocableSmsDcNct1A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcNct2A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcNct3A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcNct3B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsDcNct3B.buildProcessSmsRequestInvocable(processList);
        
        //Testing invocable SMS Classes for NCR
        
        InvocableSmsNCR1A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR1B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR1C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR1D.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR2A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR2B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR3A3B3C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsNCR3D3E.buildProcessSmsRequestInvocable(processList);
        
        //Testing invocable SMS Classes for OMBUD
        
        InvocableSmsOM3A5A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB1B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB1C.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB1D.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB2A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB2B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB4A.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB4B.buildProcessSmsRequestInvocable(processList);
        InvocableSmsOMB5B.buildProcessSmsRequestInvocable(processList);
        
        
    }
    
    
    static testMethod void test_InvocableSmsTestCase(){
        Case testCase = new Case();
        List<Case> caseList = new List<Case>();
        caseList.add(testCase);
        
        //Testing invocable SMS Classes for DC
        
        InvocableSmsDc1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc1B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc1C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc1D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc2C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc2D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc3A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDc3B.buildCaseSmsRequestInvocable(caseList);        
        
        //Testing invocable SMS Classes for Audit Reckless Lending
        
        InvocableSmsAudRL1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL1B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL1C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL1D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL1E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL3A3B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAudRL3C3D.buildCaseSmsRequestInvocable(caseList);
        
        InvocableSmsAudSavings.buildCaseSmsRequestInvocable(caseList);
        
        //Testing invocable SMS Classes for Audit NCT
        
        InvocableSmsAUDNCT1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAUDNCT1B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAUDNCT2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAUDNCT3A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAUDNCT3B.buildCaseSmsRequestInvocable(caseList);
        
        //Testing invocable SMS Classes for Audit
        
        InvocableSmSAud1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmSAud2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmSAud2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmSAud1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmSAud4A4B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmSAudClosing.buildCaseSmsRequestInvocable(caseList);
        
        //Testing invocable SMS Classes for Reckless Lending
        
        
        InvocableSmsRl1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl3A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl3B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl3C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl3F.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsRl3G.buildCaseSmsRequestInvocable(caseList);
        
        //Testing invocable SMS Classes for DC Reckless Lending
        
        InvocableSmsDCRl3D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDCRl3E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDCRl3G.buildCaseSmsRequestInvocable(caseList);
        
        //Testing invocable SMS Classes for DR
        
        InvocableSmsDR1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR1B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR1C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR1D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR2C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR2D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsDR3A.buildCaseSmsRequestInvocable(caseList);
        
        
        //Testing invocable SMS Classes for EAO
        
        InvocableSmsEAO1A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO2C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO2D3D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO3A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO3B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO3C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO4A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO4B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsEAO5A5B.buildCaseSmsRequestInvocable(caseList);
        
        
        //Testing invocable SMS Classes for Pres
        
        InvocableSmsPres2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsPres3A3B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsPresClosed.buildCaseSmsRequestInvocable(caseList);
        
    }
    
    static testMethod void test_InvocableSmsTestAfterCare(){
        Case testCase = new Case();
        List<Case> caseList = new List<Case>();
        caseList.add(testCase); 
        
        
        //Testing invocable SMS Classes for AfterCare
        
        
        InvocableSmsAFT1.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT10A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT10B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT11A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT12A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT12B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT1A1B.buildCaseSmsRequestInvocable(caseList);
        
        InvocableSmsAFT1C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT1D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT1E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT1F.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT1G.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT2A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT2B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT2C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT2D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT3A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT3B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT3C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT3D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT3E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT4B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT4C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT4D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT4E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT4F.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT5A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT5B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT5C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT5D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT6A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT6B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7F.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT7G.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8B.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8C.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8D.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8E.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8F.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT8G.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT9A.buildCaseSmsRequestInvocable(caseList);
        InvocableSmsAFT9B.buildCaseSmsRequestInvocable(caseList);
        
        
    }
}
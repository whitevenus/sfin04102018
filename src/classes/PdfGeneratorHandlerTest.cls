@isTest
public class PdfGeneratorHandlerTest {
    
    static testMethod void test_getNCTApplicationDataPositive(){
        
        Account myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Contact myPartnerContact = TestDataGenerator.setContactPartnerFields(myAccount.Id, myConsultant.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_PARTNER));
        INSERT myPartnerContact;
        
        Contact myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id, myPartnerContact.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT myContact;
        
        Financial_Assessment__c myFinancialAssessment = TestDataGenerator.setFinancialAssessmentFields(myContact.Id);
        INSERT myFinancialAssessment;
        
        Income__c contactIncome = TestDataGenerator.setIncomeFields(myFinancialAssessment.Id);
        INSERT contactIncome;
        
        Liability__c myLiabilityCreditorLoan = TestDataGenerator.setLiabilityCreditorLoanFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_CREDITOR_LOAN),myFinancialAssessment.Id);
        INSERT myLiabilityCreditorLoan;
        
        Liability__c myLiabilityHouseholdExpense = TestDataGenerator.setLiabilityHouseholdExpenseFields(GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE),myFinancialAssessment.Id);
        INSERT myLiabilityHouseholdExpense;
        
        Case myCase = TestDataGenerator.setCaseFields(GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_AUDIT_RECKLESS_LENDING),myContact.Id);
        INSERT myCase;
        
        Case_Liability__c myCaseLiability = TestDataGenerator.setCaseLiabilityFields(myCase.Id, myLiabilityCreditorLoan.Id);
        INSERT myCaseLiability;
        
        Process__c myProcess = TestDataGenerator.setProcessFields(GeneralUtilities.getRecordTypeId(StringConstants.PROCESS_RECORD_TYPE_LEGAL_DC_COURT), myCase.Id);
        INSERT myProcess;
        
        //Set Parameteres to Case object
        ApexPages.currentPage().getParameters().put(StringConstants.RECORD_ID_STRING, myCase.Id);
        
        PdfGeneratorHandler myPdfGeneratorHandlerForCase = new PdfGeneratorHandler();
        
        //Set Parameteres to Process object
        ApexPages.currentPage().getParameters().put(StringConstants.RECORD_ID_STRING, myProcess.Id);
        
        PdfGeneratorHandler myPdfGeneratorHandlerForProcess = new PdfGeneratorHandler();
        
    }
    
}
/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 29/06/2018
* Description: Represents Amazon Document Creation Response Schema

* Change History:
* None
===================================================================================================================================**/
public class IntegrationBeanDocumentCreationResponse {
    
    Public Integer id; //Uniquely identifies a document
    Public String name; //Document Name
    Public file file;
    Public String file_type; //Document File Type
    Public Integer api_user_id; //Uniquely identifies api user id
    Public Integer user_id; //Uniquely identifies user id
    Public String created_at; //Date Document was created
    Public String updated_at; //Date Document was updated
    
    public class file {
        
        Public String url; //Document URL

    }
}
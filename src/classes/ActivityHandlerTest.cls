/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 22/06/2018
* Description: Provides test methods for Activity Timeline Handler class

* Change History:
* Updated by Ramprashanth Ramakrishnan (25/06/2018)
===================================================================================================================================**/
@isTest
public class ActivityHandlerTest {
    
    static testMethod void test_getCaseInformation(){
        
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_CREDIT_PROVIDER);
        Id contactId = GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_DOC_ID_STRING);
        
        Case currentCase = TestDataGenerator.setCaseFields(recordTypeId, contactId);
        ActivityHandler.getCaseInformation(currentCase.AccountId);
        
    }
    
    static testMethod void test_completeTask() {
        
        Map<PageReference, sObject> pageControllerMap = new Map<PageReference, sObject>();
        
        PageReference casePageReference = Page.ActivityTimelineCasePage;
        PageReference processPageReference = Page.ActivityTimelineProcessPage;
        
        Case caseController = new Case();
        Process__c processController = new Process__c();
        
        pageControllerMap.put(casePageReference, caseController);
        pageControllerMap.put(processPageReference, processController);
        
        Test.startTest();
        
        //Consultant currentConsultant = TestDataGenerator.
        Account currentAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_ATTORNEY));
        INSERT currentAccount;
         
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Contact currentContact = TestDataGenerator.setContactFields(currentAccount.id, myConsultant.id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT currentContact;
        
        Case currentCase = TestDataGenerator.setCaseFields(GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_DEBT_COUNSELLING), currentContact.id);
        INSERT currentCase;
        
        Task caseTask = TestDataGenerator.setCaseTaskFields(currentCase.Id, TestDataGenerator.getActiveUser());
        INSERT caseTask;
        
        for( PageReference pageReference : pageControllerMap.keySet() ){
            
            System.Test.setCurrentPage(pageReference);
            
            ApexPages.Standardcontroller standardController = new ApexPages.Standardcontroller(currentCase);
            ApexPages.currentPage().getParameters().put(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_ID, currentCase.id);
            ApexPages.currentPage().getParameters().put(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_TASK_ID, caseTask.id);
            
            ActivityHandler activityHandler = new ActivityHandler();
            activityHandler.completeTask();
            
        }
        
        Test.stopTest();
        
    }
    
    static testMethod void test_CaseTask(){
        
        Set <Id> caseRecordTypeIdSet = new Set<Id>();
        List <Case> caseList = new List<Case>();
        List <Process__c> processList = new List<Process__c>();
        
        //Prepare Contact Creation Data
        Account myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Id contactClientRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT);
        Contact myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id, contactClientRecordTypeId);
        INSERT myContact;
        
        
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_DEBT_COUNSELLING);
        
        Case currentCase = TestDataGenerator.setCaseFields(recordTypeId, myContact.Id);
        INSERT currentCase;


        //Prepare Task Validation Data
        Set <String> taskStaticIdSet = new Set<String> {StringConstants.TASK_TEMP_FINAL_ACCEPTANCE_LETTER_RECEIVED,
            StringConstants.TASK_TEMP_PROVISIONAL_ACCEPTANCE_LETTER_RECEIVED,
            StringConstants.TASK_TEMP_CERTIFICATE_OF_BALANCE_RECEIVED,
            StringConstants.TASK_TEMP_REQUEST_CLIENT_MANDATE,
            StringConstants.TASK_TEMP_VERIFY_CASE_DOCUMENTS};

                for(String staticId : taskStaticIdSet){
                    
                    Date taskDueDate = System.today() + 1;
                    ActivityHandler.validateTask(staticId, StringConstants.TASK_RELATED_OBJECT_CASE, currentCase.Id, taskDueDate);
                    
                }



    }
    
    static testMethod void test_completeContactTask() {
        
        Map<PageReference, sObject> pageControllerMap = new Map<PageReference, sObject>();
        
        PageReference contactPageReference = Page.ActivityTimelineContactPage;
        
        
        Contact contactController = new Contact();
        
        
        pageControllerMap.put(contactPageReference, contactController);
        
        
        Test.startTest();
        
        //Consultant currentConsultant = TestDataGenerator.
        Account currentAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_ATTORNEY));
        INSERT currentAccount;
         
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Contact currentContact = TestDataGenerator.setContactFields(currentAccount.id, myConsultant.id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT currentContact;
        
        
        Task contactTask = TestDataGenerator.setContactTaskFields(currentContact.id);
        INSERT contactTask;
        
       
       
       System.Test.setCurrentPage(contactPageReference);

        ApexPages.Standardcontroller standardController = new ApexPages.Standardcontroller(currentContact);
            ApexPages.currentPage().getParameters().put(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_ID, currentContact.id);
            ApexPages.currentPage().getParameters().put(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_TASK_ID, contactTask.id);
            
            ActivityHandler activityHandler = new ActivityHandler();
            activityHandler.completeTask();
        
        Test.stopTest();
        
    }
    
    static testMethod void test_ProcessTask() {
        
        List <Process__c> processList = new List<Process__c>();
        Set <Id> caseRecordTypeIdSet = new Set<Id>();
        
        //Prepare Contact Creation Data
        Account myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Contact myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id, GeneralUtilities.getRecordTypeId(StringConstants.CONTACT_RECORD_TYPE_CONTACT_CLIENT));
        INSERT myContact;
        
        Id caseRecordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_DEBT_COUNSELLING);
        Case currentCase = TestDataGenerator.setCaseFields(caseRecordTypeId, myContact.Id);
        INSERT currentCase;
        
        //Prepare Process Creation Data
        Set <String> processRecordTypeNameSet = new Set<String> {StringConstants.PROCESS_RECORD_TYPE_LEGAL_COURT,
            StringConstants.PROCESS_RECORD_TYPE_LEGAL_ADR,
            StringConstants.PROCESS_RECORD_TYPE_LEGAL_NCR,
            StringConstants.PROCESS_RECORD_TYPE_LEGAL_OMBUDSMAN};
                
                for (String recordTypeName : processRecordTypeNameSet){
                    
                    Id recordTypeId = GeneralUtilities.getRecordTypeId(recordTypeName);
                    Process__c currentProcess = TestDataGenerator.setProcessCaseFields(recordTypeId, currentCase.Id, UserInfo.getUserId());
                    
                    processList.add(currentProcess);
                    
                }
        
        INSERT processList;
        
        //Prepare Task Validation Data
        Set <String> taskStaticIdSet = new Set<String> {StringConstants.TASK_RELATED_OBJECT_PROCESS,
            StringConstants.TASK_TEMP_CLIENT_STOP_ORDER_ACTIVATED,
            StringConstants.TASK_TEMP_VERIFY_COLLECTIONS_DOCUMENTS,
            StringConstants.TASK_TEMP_SEND_FOUNDING_CONFIRMATORY_AFFIDAVIT,
            StringConstants.TASK_TEMP_NCT_ORDER_RECEIVED,
            StringConstants.TASK_TEMP_VERIFY_PROCESS_DOCUMENTS,
            StringConstants.TASK_TEMP_GET_TRIBUNAL_COURT_DATE,
            StringConstants.TASK_TEMP_COURT_DATE_ISSUED,
            StringConstants.TASK_TEMP_APPOINT_CORRESPONDING_ATTORNEY,
            StringConstants.TASK_TEMP_CANCELLATION_OF_DEBIT_ORDER_SIGNED,
            StringConstants.TASK_TEMP_FIRST_FEES_PAID,
            StringConstants.TASK_TEMP_SECOND_FEES_PAID,
            StringConstants.TASK_TEMP_NCT_ORDER_RECEIVED};

                    
                for(String staticId : taskStaticIdSet){
                    
                    Date taskDueDate = System.today() + 1;
                    
                    ActivityHandler.validateTask(staticId, StringConstants.TASK_RELATED_OBJECT_PROCESS, processList[0].Id, taskDueDate);
                    
                }

    }
}
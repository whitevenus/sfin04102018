public class PdfGeneratorHandler {
    
    Public String relatedObjectId;
    Public Set<Id> relatedLiabilityIdSet;
    Public Set<String> liabilityRespondentSet { get; set; }
    public List<Case_Liability__c> caseLiabilityList { get; set; }
    Public List <Liability__c> liabilityDataList { get; set; }
    Public List <Liability__c> liabilityListForCOBReceiveidWithin10Days { get; set; }
    Public List <Liability__c> liabilityListForCOBReceiveidAfter10Days { get; set; }
    Public List <Payment_Plan__c> paymentPlanListFinal { get; set; }
    Public Payment_Plan__c paymentPlan { get; set; }
    Public Integer liabilityDataListSize { get; set; }
    
    Public List <Contact> contactDataList { get; set; }
    Public List <Financial_Assessment__c> financialAssessmentLiabilitiesList { get; set; }
    Public List<Liability__c> householdExpenseList { get; set; }
    Public List<Liability__c> payslipDeductionList { get; set; }
    Public Decimal totalOtherIncome {get; set;}
    Public Decimal totalBalance {get; set;}
    Public Decimal totalRevisedExpenseAmount {get; set;}
    Public String heSheGenderPronoun {get; set;}
    Public String hisHerGenderPronoun {get; set;}
    Public String contactInitials {get; set;}

    /**=================================================================================================================================
* Queries data for use in a visualforce page to render in PDF format
===================================================================================================================================**/
    
    public PdfGeneratorHandler(){
        
        
        
        relatedObjectId = ApexPages.currentPage().getParameters().get(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_ID);
        String parentRecordObjectName = '';
        
        System.debug('relatedObjectId: ' + relatedObjectId);
        
        if(relatedObjectId != null){
            
            parentRecordObjectName = GeneralUtilities.getParentRecordObjectApiName(relatedObjectId);
            
        }                
        
        If(String.IsNotBlank(parentRecordObjectName) && parentRecordObjectName == StringConstants.OBJECT_NAME_PROCESS){
            
            getContactIdUsingProcessId(relatedObjectId);
            
        }else if(String.IsNotBlank(parentRecordObjectName) && parentRecordObjectName == StringConstants.OBJECT_NAME_CASE){
            
            getNCTApplicationData(relatedObjectId);
            
        }
    }
    
    
    public PageReference getNCTApplicationData(Id relatedObjectIdP){
        
        try{
            
            System.debug('relatedObjectIdP: ' + relatedObjectIdP);
            
            relatedLiabilityIdSet = new Set<Id>();
            
            caseLiabilityList = [SELECT Id, Liability__c 
                                 FROM Case_Liability__c 
                                 WHERE Case__c =: relatedObjectIdP];
            
            System.debug('caseLiabilityList: ' + caseLiabilityList.size());
            
            //Verify that caseLiabilityList is not null and has at least 1 Case Liability
            if(caseLiabilityList != Null && caseLiabilityList.size() > 0){
                
                //Add Liability id's to our "relatedLiabilityIdSet"
                for(Case_Liability__c caseLiability : caseLiabilityList){
                    
                    relatedLiabilityIdSet.add(caseLiability.Liability__c);
                    
                }
                
                System.debug('relatedLiabilityIdSet: ' + relatedLiabilityIdSet.size());
                
            }
            
            List<Case> caseList = [SELECT CreatedDate FROM Case WHERE Id =: relatedObjectIdP LIMIT 1];
            //14 Days added instead of 10 Days to compensate for the 4 weekend days
            Datetime tenBusinessDaysDate = caseList[0].CreatedDate.addDays(14);
            
            /*paymentPlanListFinal = [SELECT Id, Type__c, Interest_Rate__c, Monthly_Service_Fees__c, Liability__r.Recurring_Amount__c,
Liability__r.Credit_Provider__c, Liability__r.Loan_Reference_Number__c, Payment_Term_Cascade__c,
Period__c, From_Date__c, To_Date__c, Linked_Insurance__c,Payment_Term_No_Cascade__c 

FROM Payment_Plan__c
WHERE Type__c =: StringConstants.PAYMENT_PLAN_TYPE_FINAL
ORDER BY LastModifiedById DESC];

paymentPlan = paymentPlanListFinal[0];*/
            
            liabilityDataList = [SELECT Id, Name, Credit_Provider__r.Name, Type__c, 
                                 Loan_Reference_Number__c, Credit_Provider__r.Account_Email__c, 
                                 Credit_Provider__r.Phone, Credit_Provider__r.Registration_Number__c,
                                 Credit_Provider__r.BillingStreet, Credit_Provider__r.BillingCity, Loan_Term_in_months__c, 
                                 Credit_Provider__r.BillingState, Credit_Provider__r.BillingCountry,Last_Payment_Date__c, 
                                 Credit_Provider__r.BillingPostalCode, Interest_Rate__c, Recurring_Amount__c,Date_Certificate_of_Balance_Received__c,
                                 Loan_Capital_Outstanding__c, Term_in_months__c, Effective_Start_Date__c,Account_Code__c, Monthly_Service_Fees__c,
                                 Revised_Amount__c, Rescheduled_Recurring_Amount__c, Rescheduled_Interest_Rate__c, Payment_Term_Cascade__c,
                                 Rescheduled_Monthly_Service_Fees__c
                                 FROM Liability__c 
                                 WHERE Id IN: relatedLiabilityIdSet];
            
            System.debug('liabilityDataList: ' + liabilityDataList.size());
            
            if(liabilityDataList != Null && liabilityDataList.size() > 0){
                
                liabilityRespondentSet = new Set<String>();
                
                liabilityListForCOBReceiveidWithin10Days = new List <Liability__c>();
                liabilityListForCOBReceiveidAfter10Days = new List <Liability__c>();
                
                for(Liability__c liability: liabilityDataList){
                    
                    liabilityRespondentSet.add(liability.Credit_Provider__r.Name);
                    
                    if(tenBusinessDaysDate <= liability.Date_Certificate_of_Balance_Received__c){
                        
                        liabilityListForCOBReceiveidWithin10Days.add(liability) ;
                        
                    }else if(tenBusinessDaysDate > liability.Date_Certificate_of_Balance_Received__c){
                        
                        liabilityListForCOBReceiveidAfter10Days.add(liability) ;
                        
                    }
                    
                }
                
                System.debug('### liabilityListForCOBReceiveidWithin10Days: ' + liabilityListForCOBReceiveidWithin10Days.size());
                System.debug('### liabilityListForCOBReceiveidAfter10Days: ' + liabilityListForCOBReceiveidAfter10Days.size());
                
            }
            
            liabilityDataListSize = liabilityDataList.size()+1;

            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
        
        return null;
    }
    
    public void getContactIdUsingProcessId(Id processIdP){
        
        List<Process__c> myProcessList = [SELECT Case__c FROM Process__c WHERE Id =: processIdP];
        
        if(myProcessList != Null && myProcessList.size() == 1){
            
            Id processCaseId = myProcessList[0].Case__c;
            
            getNCTApplicationData(processCaseId);
            
            List<Case> myCaseList = [SELECT ContactId FROM Case WHERE Id =: processCaseId];
            
            if(myCaseList != Null && myCaseList.size() == 1){
                
                Id processContactId = myCaseList[0].ContactId;
                
                buildSobjectRecordTypeMap(processContactId);
                
            }
        }
    }
    
    public Map<String, List<SObject>> buildSobjectRecordTypeMap(Id contactIdP){
        
        System.debug('contactIdP: ' + contactIdP);
        
        Map<String, List<SObject>> recordTypeMap;
        
        String firstLetterFirstName;
        String firstLetterLastName;
        
        contactDataList = [SELECT Id, Name, Email, MobilePhone, Age__c,
                           Gender__c, Marital_Status__c, ID_Number__c,
                           Passport_Number__c, Title, Account_Operation__c,
                           Employee_Number__c, Account.Name, Phone,
                           FirstName, LastName,
                           (SELECT Id
                            FROM Financial_Assessments__r
                            WHERE Is_Active__c = True
                            ORDER BY CreatedDate DESC LIMIT 1)
                           FROM Contact
                           WHERE Id =: contactIdP
                           LIMIT 1];
        
        System.debug('contactDataList: ' + contactDataList.size());
        
        //Verify that contactDataList is not null and contains 1 contact
        if(contactDataList != Null && contactDataList.size() == 1){
            
            Contact contact = contactDataList[0];
            
            firstLetterFirstName = contact.FirstName.substring(0, 1);
            firstLetterLastName = contact.LastName.substring(0, 1);
            contactInitials = firstLetterFirstName + firstLetterLastName;
            System.debug('** contactInitials: ' + contactInitials);
            
            if(contact.Gender__c == StringConstants.GENDER_MALE){
                
                heSheGenderPronoun = StringConstants.HE_PRONOUN;
                hisHerGenderPronoun = StringConstants.HIS_PRONOUN;
                
            }else if(contact.Gender__c == StringConstants.GENDER_FEMALE){
                
                heSheGenderPronoun = StringConstants.SHE_PRONOUN;
                hisHerGenderPronoun = StringConstants.HER_PRONOUN;
                
            }else{
                
                heSheGenderPronoun = StringConstants.GENDER_UNKOWN_PRONOUN_FOR_HE_SHE;
                hisHerGenderPronoun = StringConstants.GENDER_UNKOWN_PRONOUN_FOR_HIS_HER;
                
            }
            
            financialAssessmentLiabilitiesList = [SELECT Id, Credit_Score__c, Total_Income__c, Total_Monthly_Household_Expenses__c,
                                                  Total_Capital_Outstanding__c, Total_Monthly_Debt_Commitments__c, Total_Contact_Net_Income__c,
                                                  (SELECT Id, Name, Type__c, Comments__c, Recurring_Amount__c, Credit_Provider__r.NAME,
                                                   RecordType.DeveloperName, Loan_Reference_Number__c, Loan_Capital_Outstanding__c, Date_Loan_Granted__c,
                                                   Revised_Amount__c, Deduction_Category__c
                                                   FROM Liabilities__r),
                                                  (SELECT Id, Type__c, Net_Amount_Formula__c 
                                                   FROM Incomes__r)
                                                  FROM Financial_Assessment__c
                                                  WHERE Id =: contactDataList[0].Financial_Assessments__r
                                                  LIMIT 1];
            
            System.debug('financialAssessmentLiabilitiesList: ' + financialAssessmentLiabilitiesList.size());
            
            //Verify that financialAssessmentLiabilitiesList is not null and contains an assessment
            if(financialAssessmentLiabilitiesList != Null && financialAssessmentLiabilitiesList.size() == 1){
                
                Financial_Assessment__c assessment = financialAssessmentLiabilitiesList[0];
                
                totalBalance = assessment.Total_Contact_Net_Income__c - (assessment.Total_Monthly_Household_Expenses__c + assessment.Total_Monthly_Debt_Commitments__c);
                
                //Verify that the assessment related liabilities is not null and contains related liabilities for the assessment
                if(assessment.Liabilities__r != Null && assessment.Liabilities__r.size() > 0){
                    
                    Schema.DescribeFieldResult fieldResult = Liability__c.RecordTypeId.getDescribe();
                    
                    //BUILD A MAP FROM A (LIABILITY)SOBJECT LIST FOR LIABILITY
                    recordTypeMap = GeneralUtilities.convertListToMapWithStringKeyForLookup(assessment.Liabilities__r,
                                                                                            fieldResult,
                                                                                            StringConstants.RECORD_TYPE_DEVELOPER_NAME_STRING_NAME);
                    
                    //CREATE A LIST FOR LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE
                    householdExpenseList = recordTypeMap.get(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE);
                    
                    System.debug('householdExpenseList: ' + householdExpenseList);
                    
                    if(householdExpenseList != Null && householdExpenseList.size() > 0){
                        
                        totalRevisedExpenseAmount = 0;
                        
                        for(Liability__c householdExpense : householdExpenseList){
                            
                            if(householdExpense.Revised_Amount__c != null && householdExpense.Revised_Amount__c > 0){
                                
                                System.debug('householdExpense: ' + householdExpense.Revised_Amount__c);
                                
                                totalRevisedExpenseAmount += householdExpense.Revised_Amount__c;
                            }
                            
                        }
                        
                    }
                    
                    //CREATE A LIST FOR LIABILITY_RECORD_TYPE_PAYSLIP_DEDUCTIONS
                    payslipDeductionList = recordTypeMap.get(StringConstants.LIABILITY_RECORD_TYPE_DEDUCTION);
                    
                    recordTypeMap.put('contactDataList', contactDataList);
                    recordTypeMap.put('financialAssessmentLiabilitiesList', financialAssessmentLiabilitiesList);
                    //recordTypeMap.put('SurplusShortFall', new List<Decimal>{SurplusShortFall});
                    
                }
                
                if(assessment.Incomes__r != Null && assessment.Incomes__r.size() > 0){
                    
                    Map<String, List<Income__c>> incomeTypeMap = GeneralUtilities.convertListToMapWithStringKey(
                        assessment.Incomes__r,
                        StringConstants.TYPE_FIELD_API_NAME);
                    
                    System.debug('incomeTypeMap: ' + incomeTypeMap.KeySet());
                    
                    List <Income__c> salaryIncomeList = new List <Income__c>();
                    
                    //Loop through Salary Type field values
                    for(String incomeType : incomeTypeMap.KeySet()){
                        
                        System.debug('incomeType: ' + incomeType);
                        
                        if(incomeType == StringConstants.SALARY_TYPE_INCOME){
                            
                            salaryIncomeList = incomeTypeMap.get(incomeType);
                            
                        }
                        
                        System.debug('salaryIncomeList: ' + salaryIncomeList.size());
                        
                    }//end incomeTypeMap for loop
                    
                    if(salaryIncomeList != Null && salaryIncomeList.size() > 0){
                        
                        Decimal totalSalary = 0;
                        
                        for(Income__c income : salaryIncomeList){
                            
                            if(income.Net_Amount_Formula__c != null && income.Net_Amount_Formula__c > 0){
                                
                                totalSalary += income.Net_Amount_Formula__c;
                                
                            }
                            
                        }
                        
                        System.debug('totalSalary: ' + totalSalary);
                        
                        if(totalSalary != null && totalSalary > 0){
                            
                            totalOtherIncome = assessment.Total_Contact_Net_Income__c - totalSalary;
                            
                        }
                        
                        System.debug('totalOtherIncome: ' + totalOtherIncome);
                        
                    }
                    
                }
                
            }
            
        }
        
        System.debug('recordTypeMap: ' + recordTypeMap.size());
        
        return recordTypeMap;
        
    }
    
}
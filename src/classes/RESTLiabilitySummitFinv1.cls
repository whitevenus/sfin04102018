/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 30/07/2018
* Description: Provides Custom REST API Endpoint for Liability Object Inbound Integration from App.SummitFin

* Change History:
* None
===================================================================================================================================**/
@RestResource(urlMapping = '/Liability/SummitFin/V1')
global with sharing class RESTLiabilitySummitFinv1{
    
    /**==============================================================================================================================
* Gets invoked when POST Request Method is used to access API endpoint
==================================================================================================================================**/
    @HttpPost
    global static void postLiability(){
        
        RestRequest restRequest = RestContext.request;
        
        //If the content type is not JSON then do not proceed with mapping fields
        if(restRequest.headers.get(StringConstants.HTTP_CONTENT_TYPE) != null
           && restRequest.headers.get(StringConstants.HTTP_CONTENT_TYPE).contains(StringConstants.HTTP_APPLICATION_JSON)){
               
               String requestBody = restRequest.requestBody.toString();
               IntegrationBeanRESTApiResponse response = IntegrationHandler.createLiabilityRecordFromSummitFinInformation(requestBody);
               
               RestResponse restResponse = RestContext.response;
               
               restResponse.statusCode = response.responses[0].statusCode;
               restResponse.addHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_JSON);
               restResponse.responseBody = Blob.valueOf(JSON.serialize(response, true));
               
           }
    }
    
    /**==============================================================================================================================
* Gets invoked when PATCH Request Method is used to access API endpoint
==================================================================================================================================**/
    @HttpPatch
    global static void patchLiability(){
        
        RestRequest restRequest = RestContext.request;
        
        //If the content type is not JSON then do not proceed with mapping fields
        if(restRequest.headers.get(StringConstants.HTTP_CONTENT_TYPE) != null
           && restRequest.headers.get(StringConstants.HTTP_CONTENT_TYPE).contains(StringConstants.HTTP_APPLICATION_JSON)){
               
               String requestBody = restRequest.requestBody.toString();
               IntegrationBeanRESTApiResponse response = IntegrationHandler.patchLiabilityRecordFromSummitFinInformation(requestBody);
               
               RestResponse restResponse = RestContext.response;
               
               restResponse.statusCode = response.responses[0].statusCode;
               restResponse.addHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_JSON);
               restResponse.responseBody = Blob.valueOf(JSON.serialize(response, true));
               
           }
    }
}
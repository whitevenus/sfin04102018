/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 22/06/2018
* Description: Represents Simplicity Contact Schema

* Change History:
* None
===================================================================================================================================**/
public class IntegrationBeanSimplicityContact {
    
    Public String id; //uuid: Uniquely identifies a client
    Public String accepteddate; //date: Date when the lead was accepted on Debt Counselling
    Public Integer actionday; //int16: Action Day. Day of the month the client pays.
    Public String guid; //guid Unique identifier for Contact in Salesforce ( GUID)
    Public addresses[] addresses;
    Public Decimal affordability; //double: Affordability. Amount of money the client can pay for Debt Counselling monthly.
    Public String applicationdate; //date: Application Date. Date that the client/lead applied for Debt Counselling.
    Public String bank_acc_nr; //Bank Account Number
    Public String bankacctype; //Enum: Bank Account Type (Current, Savings, Transmission, Subshare)
    Public String bankbranchnumber; //Bank Account No
    Public String birthdate; //date: Client’s Birth Date
    Public String branch; //The branch name that the client falls under in Simplicity.
    Public String casenumber; //Court Case Number
    Public String cell; //Client Cellphone number
    Public String clientid; //ID Number / Passport
    Public String surname; //Surname
    Public String collectiontype; //Enum: Collection Type (debit order, eft, stop order, cash)
    Public Integer costafter24; //double: After care fees after 24 months
    Public String court_order_date; //date: Court order date
    Public String date_form16_received; //date: Date form 16 signed
    Public Integer dependant_count; //Number of people reliant on the client
    Public String district; //Court district client falls under
    Public String email; //email: Client email
    Public employers[] employers;
    Public String fax; //Fax number
    Public String firstname; //Client first name
    Public String gender; //Enum: Client gender (Male, Female)
    Public String idtype; //Enum: Shows the type of ID (ID, Passport)
    Public String leadsource; //Source of the lead
    Public String martital_status; //Enum: Client Marital status ("Married": "Community of Property" }, { "Married": "Antenuptial Agreement" }, { "Married": "Joined Application" }, Divorced, { "Divorced": "Joined Application" }, Co-Inhabited, { "Co-Inhabited": "Joined Application" }, Widow, Widower, Married by Tradition, { "Married by Tradition": "Joined Application")
    Public String spouse_name; //Name of the client’s spouse
    Public String spouse_occupation; //Client’s spouse’s occupation
    Public spouse_addresses[] spouse_addresses;
    Public String spouse_race; //Enum: Race of spouse (asian, black, coloured, white)
    Public String spouse_surname; //Surname of spouse
    Public String spouse_telephone; //Spouse telephone number
    Public String spouse_title; //Enum: Title of the spouse. Mr/Mrs (MR, MRS, MSS, ADV, DR, SIR, HON, REV)
    Public String spouse_work; //Spouse Work Phone number
    Public String spouse_id; //Spouse ID Number / Passport
    Public String spouse_birthdate; //date: Client’s Spouse Birth Date
    Public String startdate; //date: Start date of the client. This is required for calculations.
    Public String status; //Enum: Client Status (under consideration, accepted, rejected, terminated, restructured, all paid up)
    Public String telephone; //Client telephone number
    Public String title; //Enum: Client title. Mr,Mrs (MR, MRS, MSS, ADV, DR, SIR, HON, REV)
    Public String town; //Current town client resides in
    Public String work; //The client’s work Telephone.
    Public creditors[] creditors;
    Public clientincome[] clientincome;
    Public deductions[] deductions;
    Public expenses[] expenses;
    Public assests[] assests;
    Public dependands[] dependands;
    Public consultants[] consultants;
    Public notes[] notes;
    
    public class addresses {
        
        Public String id; //uuid: Unique identifier
        Public String address_type; //Enum: Type of address. postal/street (postal, street)
        Public String address1; //Street or Post address
        Public String address2; //Address Line 2
        Public String address3; //Suburb/Area Name
        Public String city; //City
        Public Integer postal_code; //Postal Code
        
    }
    
    public class employers {
        
        Public String id; //uuid: Universally Unique Identifier
        Public String description; //Name of employer
        Public String employer_type; //Enum: Spouse or main applicant employer (main, spouse)
        Public String employeenumber; //Employee Number
        Public String employment_date; //date: Date employed
        
    }
    
    public class spouse_addresses {
        
        Public String id; //uuid: Unique identifier
        Public String address_type; //Enum: Type of address. postal/street (postal, street)
        Public String address1; //Street or Post address
        Public String address2; //Address Line 2
        Public String address3; //Suburb/Area Name
        Public String city; //City
        Public Integer postal_code; //Postal Code
        
    }
    
    public class creditors {
        
        Public String id; //uuid Unique identifier for Creditor
        Public String description; //Creditor Name
        Public String hyphen_id; //Unique Hyphen Reference Number
        Public Integer rid_trader; //int16 Unique identifier for a Creditor (Used in Simplicity, Must be provided for auto linking)
        Public Obligations[] Obligations;
        
    }
    
    public class clientincome {
        
        Public String id; //uuid simplicity unique reference
        Public String category; //Enum Category of the income (Income, Other Income)
        Public String description; //Name of the income
        Public Decimal amount; //Income amount of the main applicant
        Public Decimal spouse_amount; //Income amount of the applicant spouse if married
        Public String guid; //guid Unique identifier for Income in Salesforce ( GUID)
        
    }
    
    public class deductions {
        
        Public String id;
        Public String category;
        Public String description;
        Public Decimal amount;
        Public Decimal spouseamount;
        Public String guid; //guid Unique identifier for Deduction in Salesforce ( GUID)
        
    }
    
    public class expenses {
        
        Public String id; //Unique identifier
        Public String category; //Enum: Category of the expense (Household Expense, Financial Expense)
        Public String description; //Name of the Expense
        Public Decimal amount; //double amount
        Public String note;
        Public String guid; //guid Unique identifier for Expense in Salesforce ( GUID)
        Public String frequency; //Liability payment frequency
        
    }
    
    public class assests {
        
        Public String id;
        Public String category;
        Public String description;
        Public Decimal amount;
        Public String credit_provider;
        Public String guid;
        
    }
    
    public class dependands {
        
        Public String id;
        Public Integer age;
        Public String name;
        Public String relationship;
        Public String guid;
        
    }
    
    public class consultants {
        
        Public String id; //uuid: Universally Unique Identifier
        Public String consultant_type; //Enum: Type of consultant (sales, admin)
        Public String name; //Name of the consultant
        
    }
    
    public class notes {
        
        Public String id;
        Public String note_type;
        Public String description;
        
    }
    
    public class Obligations {
        
        Public String id; //uuid Unique identifier for Obligation in Simplicity ( UUID)
        Public String guid; //guid Unique identifier for Obligation in Salesforce ( GUID)
        Public String reference; //Account Reference
        Public String subreference; //Sub Account Reference
        Public String accountcode; //Enum Debt Restructure Account Type (DCRS) (Product Code / House) (PBL, SCV, SM, SPV, UF, UT)
        Public String creditorcode; //Enum NCR Account Type (Product Code / House) (BB, BC, BL, BO, BV, L, ML, O, PB, RC, RF, RO, OT)
        Public String currentpaymentmethod; //Enum Payment Method (Cash Payment, Garnish, Debit Order, Stop Order, EFT)
        Public String applyexcltoexpenses; //Apply Obligation to Expenses / Priority payment (None, Add to Expense, Priority Payment)
        Public String agreementtype; //Enum (Agreement Lawful, Agreement Unlawful, Agreement Reckless, Agreement Provision Unlawful)
        Public String creditortype; //Enum (Obligation, Insurance, Restructure Fees, Legal Fees, PDA Fees, Aftercare Fees)
        Public Decimal currentbalance; //double Account Current Balance
        Public Decimal originalbalance; //double Original Outstanding Balance
        Public Decimal originalpayment; //double Original Instalment
        Public Decimal originalinterest; //double Original Interest Rate
        Public Integer originalterm; //int16 Original Payment Term
        Public String cobdate; //date Certificate of Balance (COB) date
        Public String cobreceivedfrom; //Enum Certificate of Balance (COB) Source (Client, Credit Provider, Credit Bureau, None)
        Public Decimal creditlimit; //double Credit Limit
        Public Decimal insurance_amount; //double Linked Insurance Amount
        Public String insurance_description; //Linked Insurance Description
        Public String cob_source; //Enum Source of obligation info. (Client, Credit Provider, Credit Bureau, None)
        Public String date17_1; //date 17.1 Date
        Public String defaultdate; //Date of Default
        Public String description; //Name for the obligation
        Public Boolean excludefrondebtreview; //Exclude From Debtreview
        Public String expiredate; //Expiry Date
        Public Decimal feevatamount; //Restructure / Legal Fees Vat Amount
        Public String goodsdescription; //Goods Description
        Public Decimal monthlycharges; //Monthly Fees / Charges
        Public String openingdate; //date Opening Date
        Public Integer outstandingterm; //Leftover term
        Public Decimal payment; //Proposed Monthly Instalment (Calculated Field)
        Public String startdate;
        Public String lastpaymentdate;
        Public String frequency; //Liability repayment frequency
        Public Decimal arrearsamount; //Liability Arrears Amount
        Public Integer missedpayments;
        Public String clientbehaviour;
        Public Decimal cobinterestrate; //double COB Interest Rate
        Public Decimal cobamount; //double COB Instalment
        Public Decimal rescheduledinterestrate; //double Rescheduled Interest Rate
        Public Decimal rescheduledservicefees; //double Rescheduled Service Fees
        Public Decimal pdafees; //double Rescheduled PDA Fees
        Public Integer period;
        Public String fromdate;
        Public String todate;
        Public Integer termcascade;
        Public Integer termnocascade;
        Public Decimal restructuringfee;
        Public Decimal legalfees;
        Public Decimal aftercarefees;
    }
}
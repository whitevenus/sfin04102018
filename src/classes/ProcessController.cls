/**=================================================================================================================================
* Created By: Ram
* Created Date: 24/07/2018
* Description: Extends logic for process Object (SMS)

* Change History:
* None
===================================================================================================================================**/
public class ProcessController {
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided
* Invocable Methods only take collections as parameters

1. List <Process__c> processListP this is our process list sent from the onSmsProcessInvocable Process Builder
===================================================================================================================================**/
    @InvocableMethod
    public static void buildProcessSmsRequestInvocable(List <Process__c> processListP){
        
        try{
            
            if(processListP != null && processListP.size() > 0){
                
                Process__c process = processListP.iterator().next();
                
                handleCommonSmsInvocableLogic(process, StringConstants.EMPTY_STRING);
                
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
    }
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided

1. Process__c processP this is our process sent from the onSmsProcessInvocable Process Builder
2. queryKeyP is the key to find the meta data values for the sms that needs to be sent.
===================================================================================================================================**/
    public static void handleCommonSmsInvocableLogic(Process__c processP, String queryKeyP){
        
        //Only send callout from Production
        //because Sandbox SMS Notifications
        //will reduce Summit SMS Credits
        
        //IMPORTANT: Uncomment Line 29 Before Production Deployment
        //if(!GeneralUtilities.isSandBox()){
        
        //Find Case
        List<Case> caseList = [SELECT ContactId
                               FROM Case
                               WHERE Id =: processP.Case__c
                               LIMIT 1];
        
        if(caseList != null && caseList.size() == 1){
            
            //Find Mobile Number for Case Contact
            List<Contact> contactList = [SELECT MobilePhone, Name
                                         FROM Contact
                                         WHERE Id =: caseList[0].ContactId
                                         LIMIT 1];
            
            //Only send SMS if Mobile Number for Case Contact is found
            if(contactList != null && contactList.size() == 1 && contactList[0].MobilePhone != null && contactList[0].MobilePhone != ''){
                
                List<String> messageList = new List<String>();
                String queryKey;
                
                if(String.isNotBlank(queryKeyP)){
                    
                    queryKey = queryKeyP;
                    
                }       
                
                String clientName = contactList[0].Name;
                String courtDate = String.valueOf(processP.Court_Date__c);
                String courtLocation = processP.Court_Location__c;
                String dateOfNctApplication = String.valueOf(processP.Date_NCT_Application_Sent__c);
                String firstPaymentAmount = String.valueOf(processP.First_Payment_Amount__c);
                String firstPaymentDate = String.valueOf(processP.First_Payment_Date__c);
                String secondPaymentAmount = String.valueOf(processP.Second_Payment_Amount__c);
                String secondPaymentDate = String.valueOf(processP.Second_Payment_Date__c);

                messageList.add(clientName);
                messageList.add(courtDate);
                messageList.add(courtLocation);
                messageList.add(dateOfNctApplication);
                messageList.add(firstPaymentAmount);
                messageList.add(firstPaymentDate);
                messageList.add(secondPaymentAmount);
                messageList.add(secondPaymentDate);
                
                //Get SMS Template from Metadata
                List <Metadata_Notification_Template_Item__mdt> smsTemplateItemMdtList = MetadataUtilities.getNotificationTemplateItemMetadataList(queryKey);
                
                if(smsTemplateItemMdtList != Null && smsTemplateItemMdtList.Size() ==1){
                    
                    //Format Number to meet Vodacom endpoint requirements
                    //Please ensure all future changes are in line with Vodacom Service
                    String phoneNumber = StringConstants.MOBILE_NUMBER_COUNTRY_VALUE
                        + contactList[0].MobilePhone.removeStart(StringConstants.MOBILE_NUMBER_START_VALUE);
                    
                    String message = String.format(smsTemplateItemMdtList[0].Description__c, messageList);
                    
                    //Send SMS
                    if(String.isNotBlank(message)){
                        
                        IntegrationHandler.sendSmsToVodacomService(caseList[0].ContactId,
                                                                   phoneNumber,
                                                                   processP.Status__c,
                                                                   message);
                    }
                    
                }// end of smsTemplateItemMdtList if block
            }//end contact list if block
        }//end caseList Check
        
        //IMPORTANT: Uncomment Line 89 Before Production Deployment
        //}
        
    }
}
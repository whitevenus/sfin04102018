/**=================================================================================================================================
* Created By: Ram
* Created Date: 06/07/2018
* Description: Provides utility methods for apex collection

* Change History:
* None
===================================================================================================================================**/
public class CollectionUtils {
    
    //Checks if the list has value, and return a boolean value

    public static Boolean listIsNullorEmpty(List <SObject> sObjectListP){
        
        if(sObjectListP != null && sObjectListP.size() > 0) {

            return true;

        }

        return false;
    }

    //Checks if the map has value, and return a boolean value

    public static Boolean mapIsNullorEmpty ( Map <String, SObject> mapP ) {

        if(mapP !=null && mapP.size() > 0) {

            return true;
        }

        return false;
    }

    //Checks if the set has value, and return a boolean value
    public static Boolean setIsNullorEmpty ( Set<String> setP) {

        if(setP != null && setP.size() > 0 ){
            
            return true;

        }
        return false;
    }


}
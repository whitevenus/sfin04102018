@isTest
global class MockHttpVodacomCalloutResponse implements HttpCalloutMock {
    
    /**==============================================================================================================================
* This function will return a response when MockHttpVodacomCalloutResponse is invoked

1. HTTPRequest httpRequestP parameter is needed because the respond method is implemented from the HttpCalloutMock interface
==================================================================================================================================**/
    global HttpResponse respond(HTTPRequest httpRequestP){
        
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_XML);
        httpResponse.setBody(StringConstants.VODACOM_SEND_SMS_CALLOUT_RESPONSE_BODY);
        httpResponse.setStatusCode(StringConstants.STATUS_CODE_200);
        
        return httpResponse;
        
    }
}
@isTest
global class MockHttpAccessTokenGrantResponse implements HttpCalloutMock {
    
    /**==============================================================================================================================
* This function will return a response when MockHttpAccessTokenGrantResponse is invoked

1. HTTPRequest httpRequestP parameter is needed because the respond method is implemented from the HttpCalloutMock interface
==================================================================================================================================**/
    global HttpResponse respond(HTTPRequest httpRequestP){
        
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_JSON);
        httpResponse.setBody(StringConstants.ACCESS_TOKEN_GRANT_BODY);
        httpResponse.setStatusCode(StringConstants.STATUS_CODE_200);
        
        httpResponse.setHeader(StringConstants.HTTP_UID, StringConstants.HTTP_MOCK_UID);
        httpResponse.setHeader(StringConstants.HTTP_CLIENT_NAME, StringConstants.HTTP_MOCK_CLIENT_NAME);
        httpResponse.setHeader(StringConstants.HTTP_ACCESS_TOKEN, StringConstants.HTTP_MOCK_ACCESS_TOKEN);
        
        return httpResponse;
        
    }
}
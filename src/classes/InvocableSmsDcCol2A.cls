/**=================================================================================================================================
* Created By: Ram
* Created Date: 24/07/2018
* Description: Extends logic for process Object (SMS)

* Change History:
* None
===================================================================================================================================**/

public class InvocableSmsDcCol2A {
    /**=================================================================================================================================
* Custom Exception class to throw an exception.

1. To be used when test methods are running to throw an exception
===================================================================================================================================**/
    public class customException extends Exception{
        
    }
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided
* Invocable Methods only take collections as parameters

1. List <Process__c> processListP this is our process list sent from the onSmsProcessInvocable Process Builder
===================================================================================================================================**/
    @InvocableMethod
    public static void buildProcessSmsRequestInvocable(List <Process__c> processListP){
        
        
        try{
            
            if(processListP != null && processListP.size() > 0){
                
                Process__c process = processListP.iterator().next();
                
                ProcessController.handleCommonSmsInvocableLogic(process, StringConstants.PROCESS_SMS_KEY_COL_2A);
                //If test classes are running an exception is thrown for code coverage for the exception block
                if (test.isRunningTest()){
                    
                    throw new customException();
                    
                }
                
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
    }
}
public class MetadataUtilities {
    
    /**==============================================================================================================================
* Builds a SELECT ALL type query for Metadata object based on parameter(s) provided

1. String metadataObjectNameP this is our query metadata object name
2. String whereClauseP this is the where clause for our select query
==================================================================================================================================**/
    public static List<sObject> getQueryResultForMetadata(String metadataObjectNameP, String whereClauseP){
        
        Map < String, Schema.SObjectField > fieldObjectMap = Schema.getGlobalDescribe().get(metadataObjectNameP).getDescribe().Fields.getMap();
        
        String queryString = StringConstants.QUERY_SELECT;
        String fieldName = StringConstants.EMPTY_STRING;
        
        for( Schema.SObjectField field : fieldObjectMap.values() ){
            
            fieldName = field.getDescribe().getName();
            queryString += fieldName + StringConstants.QUERY_DELIMITER;
            
        }
        
        queryString = queryString.subStringBeforeLast(StringConstants.QUERY_TRAILING_DELIMITER);
        queryString += whereClauseP;
        
        List < sObject > sObjectMetadataRecordsList = Database.query( queryString );
        
        return sObjectMetadataRecordsList;
        
    }
    
    /**==============================================================================================================================
* Gets application option metadata list based on parameter(s) provided

1. String applicationOptionP this is our application option record
==================================================================================================================================**/
    public static List < Metadata_Application_Option__mdt > getApplicationOptionMetadataList(String applicationOptionP){
        
        String whereClause = StringConstants.QUERY_FROM_METADATA_APPLICATION_OPTION + applicationOptionP + StringConstants.QUERY_END_DELIMITER;
        
        return getQueryResultForMetadata(StringConstants.METADATA_APPLICATION_OPTION_NAME, whereClause);
        
    }
    
    /**==============================================================================================================================
* Gets notification template item metadata list based on parameter(s) provided

1. String notificationKeyP this is our notification template item record key value
==================================================================================================================================**/
    public static List < Metadata_Notification_Template_Item__mdt > getNotificationTemplateItemMetadataList(String notificationKeyP){
        
        String whereClause = StringConstants.QUERY_FROM_METADATA_NOTIFICATION_TEMPLATE_ITEM + notificationKeyP + StringConstants.QUERY_END_DELIMITER;
        
        return getQueryResultForMetadata(StringConstants.METADATA_NOTIFICATION_TEMPLATE_ITEM_NAME, whereClause);
        
    }
    
    /**==============================================================================================================================
* Determines if object triggers can run or not based on metadata flag
==================================================================================================================================**/
    public static Boolean getExecuteTriggerFlagFromApplicationOptionMdt(){
        
        List <Metadata_Application_Option__mdt> applicationOptionMdtList = getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
        
        if(applicationOptionMdtList != Null && applicationOptionMdtList.size() == 1){
            
            return applicationOptionMdtList[0].ExecuteTriggerFlag__c;
            
        }
        
        return true;
    }
}
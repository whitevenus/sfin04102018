global with sharing class Logger {
    
    /**==============================================================================================================================
* This gets the Org wide Application Option details from Metadata
==================================================================================================================================**/
    global static Metadata_Application_Option__mdt APPLICATION_OPTION{
        
        get{
            
            if(APPLICATION_OPTION == null){
                
                List<Metadata_Application_Option__mdt> applicationOptionMetadataList = MetadataUtilities.getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
                
                if(applicationOptionMetadataList != null && applicationOptionMetadataList.size() == 1){
                    
                    APPLICATION_OPTION = applicationOptionMetadataList.get(0);
                    
                }
            }
            
            return APPLICATION_OPTION;
            
        }
        
        private set;
        
    }
    
    /**==============================================================================================================================
* This gets the endpoint from the Named Credential
==================================================================================================================================**/
    global static String LOGGER_ENDPOINT{
        
        get{
            
            if(LOGGER_ENDPOINT == null){
                
                if(GeneralUtilities.isSandBox()){
                    
                    LOGGER_ENDPOINT = StringConstants.NAMED_CREDENTIAL_LOGGLY_STAGING;
                    
                }else{
                    
                    LOGGER_ENDPOINT = StringConstants.NAMED_CREDENTIAL_LOGGLY_PRODUCTION;
                    
                }
            }
            
            return LOGGER_ENDPOINT;
            
        }
        
        private set;
        
    }
    
    /**==============================================================================================================================
* This gets the level from the APPLICATION_OPTION
==================================================================================================================================**/
    global static String LOGGER_LEVEL{
        
        get{
            
            if (LOGGER_LEVEL == null && APPLICATION_OPTION != null){
                
                LOGGER_LEVEL = APPLICATION_OPTION.Loggly_Callout_Log_Level__c;
                
            }
            
            return LOGGER_LEVEL;
            
        }
        
        private set;
        
    }
    
    /**==============================================================================================================================
* This gets the tags to be used by the logger
==================================================================================================================================**/
    global static String LOGGER_TAGS{
        
        get{
            
            if(LOGGER_TAGS == null){
                
                LOGGER_TAGS = StringConstants.DEFAULT_LOGGER_TAGS;
                
            }
            
            return LOGGER_TAGS;
            
        }
        
        set;
    }
    
    /**==============================================================================================================================
* Performs Batch Logs

* This is important for being able to use Logger.singleLog and still capture it and send it over and only use one @future call.
* If you have a known exit point (i.e a webservice call) set this to true and before exiting the method call the flush() on your Logger instance.
==================================================================================================================================**/
    global static Boolean BATCH_LOGS{
        
        get{
            
            if(BATCH_LOGS == null){
                
                BATCH_LOGS = false;
                
            }
            
            return BATCH_LOGS;
            
        }
        
        set;
    }
    
    global static Boolean THROW_TEST_EXCEPTION{
        
        get{
            
            if(THROW_TEST_EXCEPTION == null){
                
                THROW_TEST_EXCEPTION = false;
                
            }
            
            return Test.isRunningTest() ? THROW_TEST_EXCEPTION : false;
            
        }
        
        set;
    }
    
    /**==============================================================================================================================
* This class is a wrapper for our log information.

* It allows us to easily format log information internally without our clients having to know formatting scheme.
* NOTE: Loggly does not support setting actual date the event happened.
==================================================================================================================================**/
    global class Log{
        
        //The text value of the message
        public String logValue;
        
        //The time in which the log occurred.
        public DateTime logDate;
        
        //The level of the log (ERROR, WARNING, INFO, DEBUG)
        public String logLevel;
        
        public Map<String, Object> additionalValues;
        
        //Blank constructor
        public Log(){}
        
        public Log(String logValueP, DateTime logDateP){
            
            this();
            this.logValue = logValueP;
            this.logDate = logDateP;
            this.logLevel = StringConstants.DEFAULT_LEVEL;
            
        }
        
        public Log(String logValueP){
            
            this(logValueP, DateTime.now());
            
        }
        
        public Log(String logValueP, DateTime logDateP, String logLevelP){
            
            this(logValueP, logDateP);
            this.logLevel = logLevelP;
            
        }
        
        public Log(String logValueP, DateTime logDateP, String logLevelP, Map<String, Object> additionalValuesP){
            
            this(logValueP, logDateP, logLevelP);
            this.additionalValues = additionalValuesP;
            
        }
        
        public Map<String, Object> toKeyValueMap(){
            
            Map<String, Object> keyValueMap = new Map<String, Object>();
            
            keyValueMap.put(StringConstants.KEY_ORG_ID, UserInfo.getOrganizationId());
            keyValueMap.put(StringConstants.KEY_USERID, UserInfo.getUserId());
            keyValueMap.put(StringConstants.KEY_USERNAME, UserInfo.getUserName());
            keyValueMap.put(StringConstants.KEY_DATETIME, this.logDate.format(StringConstants.DATE_FORMAT));
            
            if (this.logLevel != null){
                
                keyValueMap.put(StringConstants.KEY_LEVEL, this.logLevel);
                
            }
            
            if (String.isNotBlank(this.logValue)){
                
                keyValueMap.put(StringConstants.KEY_MESSAGE, this.logValue);
                
            }
            
            if (this.additionalValues != null){
                
                keyValueMap.putAll(this.additionalValues);
                
            }
            
            return keyValueMap;
            
        }
        
        //Returns a JSON string version of the log message
        public String toJSONString(){
            
            return JSON.serialize(this.toKeyValueMap());
            
        }
    }
    
    /**==============================================================================================================================
* Class to cache several log messages to then push to Logger at one time
==================================================================================================================================**/
    global class LogCache{
        
        public List<Log> logList;
        
        //Constructor to make a new empty list
        public LogCache(){
            
            this.logList = new List<Log>();
            
        }
        
        public LogCache(Log logP){
            
            this();
            this.logList.add(logP);
            
        }
        
        //Constructor based on a list of logs
        public LogCache(List<Log> logListP){
            
            this();
            this.logList.addAll(logListP);
            
        }
        
        //Adds a single log to the list
        public void add(Log logP){
            
            this.logList.add(logP);
            
        }
        
        //Flushes all of the logs and pushes them to Logger
        public void flushLogs(){
            
            if (this.logList.isEmpty()){
                
                return;
                
            }
            
            Set<String> logMessageSet = new Set<String>();
            
            for(Log log: this.logList){
                
                logMessageSet.add(log.toJSONString());
                
            }
            
            serviceCallout(logMessageSet, LOGGER_TAGS);
            
            this.logList.clear();
            
        }
    }
    
    /**==============================================================================================================================
* An instance variable of our log cache
==================================================================================================================================**/
    global static LogCache cache;
    
    /**==============================================================================================================================
* Constructor for use with batching logs
==================================================================================================================================**/
    global Logger(){
        
        cache = new LogCache();
        
    }
    
    global void add(String logValueP, DateTime logDateP){
        
        cache.add(new Log(logValueP, logDateP));
        
    }
    
    global void add(String logValueP, DateTime logDateP, String logLevelP){
        
        cache.add(new Log(logValueP, logDateP, logLevelP));
        
    }
    
    global void add(String logValueP, DateTime logDateP, String logLevelP, Map<String, Object> additionalValuesP){
        
        cache.add(new Log(logValueP, logDateP, logLevelP, additionalValuesP));
        
    }
    
    global void flush(){
        
        cache.flushLogs();
        
    }
    
    /**==============================================================================================================================
* Overloaded methods which invoke service callout based on parameter(s) provided
* Sends a single log to Logger
* This can be overridden by setting BATCH_LOGS to true
==================================================================================================================================**/
    global static void singleLog(String logValueP, DateTime logDateP, String logLevelP, Map<String, Object> additionalValuesP, LogCache logCacheP){
        
        Log log = new Log(logValueP, logDateP, logLevelP, additionalValuesP);
        
        //If we are batching logs we want to override single log.
        //All calls should then be made through a class instance of Logger
        if (!BATCH_LOGS){
            
            serviceCallout(new Set<String>{log.toJSONString()}, LOGGER_TAGS);
            
        }else if(logCacheP != null){
            
            logCacheP.add(log);
            
        }else if(Test.isRunningTest()){
            
            StringConstants.TEST_NOCACHE = true;
            
        }
    }
    
    global static void singleLog(String logValueP, DateTime logDateP, String logLevelP, LogCache logCacheP){
        
        singleLog(logValueP, logDateP, logLevelP, null, logCacheP);
        
    }
    
    global static void singleLog(String logValueP, DateTime logDateP, LogCache logCacheP){
        
        singleLog(logValueP, logDateP, null, null, logCacheP);
        
    }
    
    global static void singleLog(String logValueP){
        
        singleLog(logValueP, DateTime.now());
        
    }
    
    global static void singleLog(String logValueP, DateTime logDateP){
        
        singleLog(logValueP, logDateP, cache);
        
    }
    
    global static void singleLog(String logValueP, DateTime logDateP, String logLevelP){
        
        singleLog(logValueP, logDateP, logLevelP, cache);
        
    }
    
    global static void singleLog(String logValueP, DateTime logDateP, String logLevelP, Map<String, Object> additionalValuesP){
        
        singleLog(logValueP, logDateP, logLevelP, additionalValuesP, cache);
        
    }
    
    global static void singleLog(Exception exP){
        
        singleLog(exP, DateTime.now(), StringConstants.EXCEPTION_LEVEL);
        
    }
    
    global static void singleLog(Exception ex, DateTime logDateP, String logLevelP){
        
        Map<String, Object> additionalValuesMap = new Map<String, Object>();
        
        additionalValuesMap.put(StringConstants.KEY_EXCEPTION_TYPE_NAME, ex.getTypeName());
        additionalValuesMap.put(StringConstants.KEY_EXCEPTION_LINENUMBER, ex.getLineNumber());
        additionalValuesMap.put(StringConstants.KEY_EXCEPTION_STACK_TRACE, ex.getStackTraceString());
        singleLog(ex.getMessage(), logDateP, logLevelP, additionalValuesMap);
        
    }
    
    /**==============================================================================================================================
* Overloaded methods which invoke common logic based on parameter(s) provided
==================================================================================================================================**/
    global static void logOutboundIntegration(HttpRequest httpRequestP){
        
        logOutboundIntegration(httpRequestP, null, StringConstants.DEFAULT_LEVEL, null);
        
    }
    
    global static void logOutboundIntegration(HttpRequest httpRequestP, Exception exP){
        
        logOutboundIntegration(httpRequestP, null, StringConstants.EXCEPTION_LEVEL, exP);
        
    }
    
    global static void logOutboundIntegration(HttpRequest httpRequestP, String logLevelP){
        
        logOutboundIntegration(httpRequestP, null, logLevelP, null);
        
    }
    
    global static void logOutboundIntegration(HttpResponse httpResponseP){
        
        logOutboundIntegration(null, httpResponseP, StringConstants.DEFAULT_LEVEL, null);
        
    }
    
    global static void logOutboundIntegration(HttpResponse httpResponseP, String logLevelP){
        
        logOutboundIntegration(null, httpResponseP, logLevelP, null);
        
    }
    
    global static void logOutboundIntegration(HttpRequest httpRequestP, HttpResponse httpResponseP){
        
        logOutboundIntegration(httpRequestP, httpResponseP, StringConstants.DEFAULT_LEVEL, null);
        
    }
    
    global static void logOutboundIntegration(HttpRequest httpRequestP, HttpResponse httpResponseP, String logLevelP){
        
        logOutboundIntegration(httpRequestP, httpResponseP, logLevelP, null);
        
    }
    
    /**==============================================================================================================================
* Method performs common logic based on parameter(s) provided

1. HttpRequest httpRequestP this is our integration request
2. HttpResponse httpResponseP this is our integration response
3. String logLevelP this is our required logging level
4. Exception exP this is our exception record
==================================================================================================================================**/
    global static void logOutboundIntegration(HttpRequest httpRequestP, HttpResponse httpResponseP, String logLevelP, Exception exP){
        
        Map<String, Object> additionalValuesMap = new Map<String, Object>();
        Map<String, String> additionalValuesRequestMap = new Map<String, String>();
        Map<String, String> additionalValuesResponseMap = new Map<String, String>();
        Map<String, String> additionalValuesExceptionMap = new Map<String, String>();
        
        String calloutName;
        LOGGER_TAGS = StringConstants.INTEGRATION_OUTBOUND_TAGS + StringConstants.TAG_DELIMITER + logLevelP;
        
        if (httpRequestP != null){
            
            additionalValuesRequestMap.put(StringConstants.KEY_INTEGRATION_HTTPREQUEST, httpRequestP.toString());
            
            calloutName = httpRequestP.getHeader(StringConstants.KEY_INTEGRATION_CALLOUTNAME);
            
            if (String.isNotBlank(calloutName)){
                
                additionalValuesMap.put(StringConstants.KEY_INTEGRATION_CALLOUTNAME, calloutName);
                LOGGER_TAGS += StringConstants.TAG_DELIMITER + calloutName;
                
            }
            
            if ((StringConstants.DEBUG_LEVELS.contains(LOGGER_LEVEL) || StringConstants.DEBUG_LEVELS.contains(logLevelP))
                && String.isNotBlank(httpRequestP.getBody())){
                    
                    additionalValuesRequestMap.put(StringConstants.KEY_INTEGRATION_BODY, httpRequestP.getBody());
                    
                }
            
            additionalValuesMap.put(StringConstants.KEY_INTEGRATION_REQUEST, additionalValuesRequestMap);
            
        }
        
        if (httpResponseP != null){
            
            additionalValuesResponseMap.put(StringConstants.KEY_INTEGRATION_HTTPRESPONSE, httpResponseP.toString());
            
            if ((StringConstants.DEBUG_LEVELS.contains(LOGGER_LEVEL) || StringConstants.DEBUG_LEVELS.contains(logLevelP))
                && String.isNotBlank(httpResponseP.getBody())){
                    
                    additionalValuesResponseMap.put(StringConstants.KEY_INTEGRATION_BODY, httpResponseP.getBody());
                    
                    for(String key: httpResponseP.getHeaderKeys()){
                        
                        additionalValuesResponseMap.put(key, httpResponseP.getHeader(key));
                        
                    }
                }
            
            additionalValuesMap.put(StringConstants.KEY_INTEGRATION_RESPONSE, additionalValuesResponseMap);
        }
        
        if (exP != null){
            
            additionalValuesExceptionMap.put(StringConstants.KEY_EXCEPTION_TYPE_NAME, exP.getTypeName());
            additionalValuesExceptionMap.put(StringConstants.KEY_EXCEPTION_MESSAGE, exP.getMessage());
            additionalValuesExceptionMap.put(StringConstants.KEY_EXCEPTION_LINENUMBER, String.valueOf(exP.getLineNumber()));
            additionalValuesExceptionMap.put(StringConstants.KEY_EXCEPTION_STACK_TRACE, exP.getStackTraceString());
            
            additionalValuesMap.put(StringConstants.KEY_EXCEPTION, additionalValuesExceptionMap);
        }
        
        singleLog(StringConstants.EMPTY_STRING, DateTime.now(), logLevelP, additionalValuesMap);
        
    }
    
    /**==============================================================================================================================
* Performs outbound integration callout to Loggly based on parameter(s) provided

1. Set<String> logSetP
2. String loggerTagsP
==================================================================================================================================**/
    global static void serviceCallout(Set<String> logSetP, String loggerTagsP){
        
        List<String> logList = new List<String>();
        HttpResponse httpResponse = new HttpResponse();
        
        logList.addAll(logSetP);
        String httpRequestMessage = String.join(logList, StringConstants.NEWLINE_DELIMITER_STRING);
        
        try{
            
            HttpRequest httpRequest = new HttpRequest();
            
            String endpoint = LOGGER_ENDPOINT + StringConstants.FORWARD_SLASH_STRING + loggerTagsP + StringConstants.FORWARD_SLASH_STRING;
            
            httpRequest.setEndpoint(endpoint);
            httpRequest.setMethod(StringConstants.HTTP_REQUEST_METHOD_POST);
            httpRequest.setHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_JSON);
            httpRequest.setBody(httpRequestMessage);
            
            Http http = new Http();
            
            if(Test.isRunningTest()){
                
                Test.setMock( HttpCalloutMock.class, new MockHttpLoggerCalloutResponse() );
                
            }
            
            httpResponse = http.send(httpRequest);
            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
    }
}
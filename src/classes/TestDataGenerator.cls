@isTest
public class TestDataGenerator{
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Account object

1. Id recordTypeIdP this is the test Account's record type
==================================================================================================================================**/
    public static Account setAccountFields(Id recordTypeIdP){
        
        Account testAccount = new Account();
        testAccount.RecordTypeId = recordTypeIdP;
        testAccount.Name = StringConstants.TEST_DATA_ACCOUNT_NAME;
        
        return testAccount;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Consultant object
==================================================================================================================================**/
    public static Consultant__c setConsultantFields(){
        
        Consultant__c testConsultant = new Consultant__c();
        
        testConsultant.Name = StringConstants.TEST_DATA_CONSULTANT_NAME;
        testConsultant.Email__c = StringConstants.TEST_DATA_CONSULTANT_EMAIL;
        
        return testConsultant;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Contact object

1. Id AccountIdP this is the test contact's parent account
2. Id ConsultantIdP this is the test contact's related Consultant
==================================================================================================================================**/
    public static Contact setContactFields(Id AccountIdP, Id ConsultantIdP, Id partnerIdP, Id contactRecordTypeIdP){
        
        Contact testContact = new Contact();
        
        testContact.FirstName = StringConstants.TEST_DATA_CONTACT_FIRST_NAME;
        testContact.LastName = StringConstants.TEST_DATA_CONTACT_LAST_NAME;
        testContact.Email = StringConstants.TEST_DATA_CONTACT_EMAIL;
        testContact.Source__c = StringConstants.TEST_DATA_CONTACT_SOURCE;
        testContact.ID_Number__c = StringConstants.TEST_DATA_CONTACT_ID_NUMBER;
        testContact.Marital_Status__c = StringConstants.TEST_DATA_CONTACT_MARITIAL_STATUS;
        testContact.AccountId = AccountIdP;
        testContact.Consultant__c = ConsultantIdP;
        testContact.MobilePhone = StringConstants.TEST_DATA_MOBILE_PHONE;
        testContact.RecordTypeId = contactRecordTypeIdP;
        testContact.Partner_Contact__c = partnerIdP;
        
        return testContact;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Contact object

1. Id AccountIdP this is the test contact's parent account
2. Id ConsultantIdP this is the test contact's related Consultant
==================================================================================================================================**/
    public static Contact setContactFields(Id AccountIdP, Id ConsultantIdP, Id contactRecordTypeIdP){
        
        Contact testContact = new Contact();
        
        testContact.FirstName = StringConstants.TEST_DATA_CONTACT_FIRST_NAME;
        testContact.LastName = StringConstants.TEST_DATA_CONTACT_LAST_NAME;
        testContact.Email = StringConstants.TEST_DATA_CONTACT_EMAIL;
        testContact.Source__c = StringConstants.TEST_DATA_CONTACT_SOURCE;
        testContact.ID_Number__c = StringConstants.TEST_DATA_CONTACT_ID_NUMBER;
        testContact.Marital_Status__c = StringConstants.TEST_DATA_CONTACT_MARITIAL_STATUS;
        testContact.AccountId = AccountIdP;
        testContact.Consultant__c = ConsultantIdP;
        testContact.RecordTypeId = contactRecordTypeIdP;
        
        return testContact;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Contact object

1. Id AccountIdP this is the test contact's parent account
2. Id ConsultantIdP this is the test contact's related Consultant
==================================================================================================================================**/
    public static Contact setContactPartnerFields(Id AccountIdP, Id ConsultantIdP, Id contactRecordTypeIdP){
        
        Contact testContactPartner = new Contact();
        
        testContactPartner.FirstName = StringConstants.TEST_DATA_CONTACT_PARTNER_FIRST_NAME;
        testContactPartner.LastName = StringConstants.TEST_DATA_CONTACT_PARTNER_LAST_NAME;
        testContactPartner.Email = StringConstants.TEST_DATA_CONTACT_PARTNER_EMAIL;
        testContactPartner.Source__c = StringConstants.TEST_DATA_CONTACT_SOURCE;
        testContactPartner.ID_Number__c = StringConstants.TEST_DATA_CONTACT_PARTNER_ID_NUMBER;
        testContactPartner.Marital_Status__c = StringConstants.TEST_DATA_CONTACT_PARTNER_MARITIAL_STATUS;
        testContactPartner.AccountId = AccountIdP;
        testContactPartner.Consultant__c = ConsultantIdP;
        testContactPartner.RecordTypeId = contactRecordTypeIdP;
        
        return testContactPartner;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Case object

1. Id recordTypeIdP this is the test case's record type
2. Id ContactP this is the test case's parent contact
==================================================================================================================================**/
    public static Case setCaseFields(Id recordTypeIdP, Id ContactP){
        
        Case testCase = new Case();
        
        testCase.RecordTypeId = recordTypeIdP;
        testCase.Status = StringConstants.TEST_DATA_CASE_STATUS;
        testCase.Origin = StringConstants.TEST_DATA_CASE_ORIGIN;
        testCase.ContactId = ContactP;
        
        testCase.Latest_Payslips__c = False;
        testCase.Loan_Agreement__c = False;
        testCase.ID__c = False;
        testCase.EAO_Attached__c = False;
        
        testCase.Signed_Form_16__c = False;
        testCase.NCT_Power_Of_Attorney__c = False;
        testCase.Payroll_Consent_Order__c = False;
        testCase.Debit_Order_Mandate__c = False;
        
        
        testCase.Debit_Order_Mandate__c = False;
        testCase.Provisional_Proposal__c = False;
        testCase.Certificate_Of_Balance__c = False;
        
        testCase.Power_Of_Attorney_Signed__c = False;
        testCase.ID__c = False;
        testCase.Signed_Form_16__c = False;
        testCase.Debt_Repayment_Proposal__c = False;
        
        testCase.Form_TI_138__c = False;
        testCase.Debt_Restructuring_Proposal__c = False;
        testCase.Proposal_Schedule_Summary__c = False;
        testCase.Revised_Budget__c = False;
        
        testCase.Draft_Consent_Order__c = False;
        //testCase.Court_Date__c = False;
        testCase.Debit_Order_Cancellation_Form__c = False;
        testCase.Hyphen_Mandate__c = False;
        
        testCase.Signed_Founding_Affidavit__c = False;
        testCase.Signed_Confirmatory_Affidavit__c = False;
        testCase.Draft_Consent_Order__c = False;
        //testCase.First_Payment_Date__c = null;
        
        //testCase.First_Payment_Amount__c  = null;
        //testCase.Second_Payment_Amount__c = null;
        //testCase.Second_Payment_Date__c = null;
        
        Id userId = UserInfo.getUserId();
        
        testCase.OwnerId = userId;
        testCase.Case_Consultant_Name__c = userId;
        
        return testCase;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Case object

1. Id recordTypeIdP this is the test case's record type
2. Id ContactP this is the test case's parent contact
==================================================================================================================================**/
    /*public static Case setCaseFieldsForNegativeTest(Id recordTypeIdP, Id ContactP){

Case testCase = new Case();

testCase.RecordTypeId = recordTypeIdP;
testCase.Status = StringConstants.TEST_DATA_CASE_STATUS;
testCase.Origin = StringConstants.TEST_DATA_CASE_ORIGIN;
testCase.ContactId = ContactP;

testCase.Latest_Payslips__c = False;
testCase.Loan_Agreement__c = False;
testCase.ID__c = False;
testCase.EAO_Attached__c = False;

testCase.Signed_Form_16__c = False;
testCase.NCT_Power_Of_Attorney__c = False;
testCase.Payroll_Consent_Order__c = False;
testCase.Debit_Order_Mandate__c = False;


testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;

testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;

testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;

testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;
testCase.Loan_Agreement__c = False;

Id userId = UserInfo.getUserId();

testCase.OwnerId = userId;
testCase.Case_Consultant_Name__c = userId;

return testCase;

}*/
    
    /**==============================================================================================================================
* Gets Unique Record Id for an Active System Administrator User in the Current Org
==================================================================================================================================**/
    public static Id getActiveUser(){
        
        List <User> userList = [SELECT Id
                                FROM User
                                WHERE Profile.Name = : StringConstants.SYSTEM_ADMINISTRATOR_ROLE
                                AND isActive = True
                                LIMIT 1];
        
        if(userList != Null && userList.size() > 0){
            
            return userList[0].Id;
            
        }else{
            
            return null;
            
        }
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Process object

1. Id recordTypeIdP this is the test process' record type
2. Id CaseIdP this is the test process' parent case
==================================================================================================================================**/
    public static Process__c setProcessFields(Id recordTypeIdP, Id CaseIdP){
        
        Process__c testProcess = new Process__c();
        
        testProcess.RecordTypeId = recordTypeIdP;
        testProcess.Status__c = StringConstants.TEST_DATA_PROCESS_STATUS;
        testProcess.Attorney_Name__c = StringConstants.TEST_DATA_PROCESS_ATTORNEY_NAME;
        testProcess.Case__c = CaseIdP;
        
        return testProcess;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Process object

1. Id recordTypeIdP this is the test process' record type
2. Id CaseIdP this is the test process' parent case
3. Id caseOwnerId this is the test process' assigned onsultant
==================================================================================================================================**/
    public static Process__c setProcessCaseFields(Id recordTypeIdP, Id CaseIdP, Id caseOwnerId){
        
        Process__c testProcess = new Process__c();
        
        testProcess.RecordTypeId = recordTypeIdP;
        testProcess.Status__c = StringConstants.TEST_DATA_PROCESS_STATUS;
        testProcess.Attorney_Name__c = StringConstants.TEST_DATA_PROCESS_ATTORNEY_NAME;
        testProcess.Case__c = CaseIdP;
        
        testProcess.Process_Consultant__c = caseOwnerId;
        
        /*
        testProcess.Power_Of_Attorney_Signed__c = False;
        testProcess.ID__c = False;
        testProcess.Signed_Form_16__c = False;
        
        testProcess.Debt_Repayment_Proposal__c = False;
        testProcess.Form_TI_138__c = False;
        testProcess.Debt_Restructuring_Proposal__c = False;
        testProcess.Proposal_Schedule_Summary__c = False;
        
        testProcess.Revised_Budget__c = False;
        testProcess.Draft_Consent_Order__c = False;
        testProcess.Court_Date__c = False;
        testProcess.Debit_Order_Cancellation_Form__c = False;
        
        testProcess.Hyphen_Mandate__c = False;
        testProcess.Signed_Founding_Affidavit__c = False;
        testProcess.Signed_Confirmatory_Affidavit__c = False;
        testProcess.Draft_Consent_Order__c = False;
        
        testProcess.First_Payment_Date__c = null;
        testProcess.First_Payment_Amount__c  = null;
        testProcess.Second_Payment_Amount__c = null;
        testProcess.Second_Payment_Date__c = null;
        */
        
        
        return testProcess;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Task object

1. Id caseIdP this is the test task's parent case
2. Id ownerIdP this is the test task's owner Id
==================================================================================================================================**/
    public static Task setCaseTaskFields(Id caseIdP, Id ownerIdP){
        
        Task testCaseTask = new Task();
        
        testCaseTask.Status = StringConstants.TEST_DATA_CASE_STATUS;
        testCaseTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_CASE;
        testCaseTask.OwnerId = ownerIdP;
        testCaseTask.WhatId = caseIdP;
        
        return testCaseTask;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Task object

1. Id ProcessIdP this is the test task's parent process
2. Id ownerIdP this is the test task's owner Id
==================================================================================================================================**/
    public static Task setProcessTaskFields(Id ProcessIdP, Id ownerIdP){
        
        Task testProcessTask = new Task();
        
        testProcessTask.Status = StringConstants.TEST_DATA_CASE_STATUS;
        testProcessTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_PROCESS;
        testProcessTask.Parent_Record_Next_Status__c = StringConstants.TEST_DATA_CASE_STATUS;
        testProcessTask.OwnerId = ownerIdP;
        testProcessTask.WhatId = ProcessIdP;
        testProcessTask.Static_ID__c = StringConstants.TEST_DATA_NCR_PROCESS_TASK_STATIC_ID;
        testProcessTask.Order__c = StringConstants.TEST_DATA_NCR_PROCESS_TASK_ORDER;
        
        return testProcessTask;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Task object

1. Id contactIdP this is the test task's parent contact
==================================================================================================================================**/
    public static Task setContactTaskFields(Id contactIdP){
        
        Task testContactTask = new Task();
        
        testContactTask.Status = StringConstants.TEST_DATA_CASE_STATUS;
        testContactTask.Related_Object__c = StringConstants.TASK_RELATED_OBJECT_CONTACT;
        testContactTask.WhoId = contactIdP;
        
        return testContactTask;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Financial Assessment object

1. Id ContactP this is the test financial assessment's parent contact
==================================================================================================================================**/
    public static Financial_Assessment__c setFinancialAssessmentFields(Id ContactP){
        
        Financial_Assessment__c testFinancialAssessment = new Financial_Assessment__c();
        
        testFinancialAssessment.Contact__c = ContactP;
        testFinancialAssessment.Is_Active__c = True;
        
        return testFinancialAssessment;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Liability object

1. Id recordTypeIdP this is the test liability's record type
2. Id FinancialAssessmentP this is the test liability's parent financial assessment
==================================================================================================================================**/
    public static Liability__c setLiabilityHouseholdExpenseFields(Id recordTypeIdP, Id FinancialAssessmentP){
        
        Liability__c testLiabilityHouseholdExpense = new Liability__c();
        
        testLiabilityHouseholdExpense.RecordTypeId = recordTypeIdP;
        testLiabilityHouseholdExpense.Type__c = StringConstants.TEST_DATA_LIABILITY_HOUSEHOLD_EXPENSE_TYPE;
        testLiabilityHouseholdExpense.Frequency__c = StringConstants.TEST_DATA_LIABILITY_FREQUENCY_MONTHLY;
        testLiabilityHouseholdExpense.Recurring_Amount__c = StringConstants.TEST_DATA_LIABILITY_RECURRING_AMOUNT;
        testLiabilityHouseholdExpense.Financial_Assessment__c = FinancialAssessmentP;
        
        return testLiabilityHouseholdExpense;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Liability object

1. Id recordTypeIdP this is the test liability's record type
2. Id FinancialAssessmentP this is the test liability's parent financial assessment
==================================================================================================================================**/
    public static Liability__c setLiabilityCreditorLoanFields(Id recordTypeIdP, Id FinancialAssessmentP){
        
        Liability__c testLiabilityCreditorLoan = new Liability__c();
        
        testLiabilityCreditorLoan.RecordTypeId = recordTypeIdP;
        testLiabilityCreditorLoan.Type__c = StringConstants.TEST_DATA_LIABILITY_CREDITOR_LOAN_TYPE;
        testLiabilityCreditorLoan.Frequency__c = StringConstants.TEST_DATA_LIABILITY_FREQUENCY;
        testLiabilityCreditorLoan.Recurring_Amount__c = StringConstants.TEST_DATA_LIABILITY_RECURRING_AMOUNT;
        testLiabilityCreditorLoan.Financial_Assessment__c = FinancialAssessmentP;
        
        return testLiabilityCreditorLoan;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Liability object

1. Id recordTypeIdP this is the test liability's record type
2. Id FinancialAssessmentP this is the test liability's parent financial assessment
3. Id incomeIdP this is the test liability's parent income
==================================================================================================================================**/
    public static Liability__c setLiabilityPayslipDeduction(Id recordTypeIdP, Id FinancialAssessmentP, Id incomeIdP){
        
        Liability__c testLiabilityPayslipDeduction = new Liability__c();
        
        testLiabilityPayslipDeduction.RecordTypeId = recordTypeIdP;
        testLiabilityPayslipDeduction.Type__c = StringConstants.TEST_DATA_PAYSLIP_DEDUCTION_TYPE_PENSION_FUND;
        testLiabilityPayslipDeduction.Frequency__c = StringConstants.TEST_DATA_LIABILITY_FREQUENCY_MONTHLY;
        testLiabilityPayslipDeduction.Recurring_Amount__c = StringConstants.TEST_DATA_LIABILITY_RECURRING_AMOUNT;
        testLiabilityPayslipDeduction.Financial_Assessment__c = FinancialAssessmentP;
        testLiabilityPayslipDeduction.Income__c = incomeIdP;
        testLiabilityPayslipDeduction.Deduction_Category__c = StringConstants.TEST_DATA_PAYSLIP_DEDUCTION_DEDUCTION_CATEGORY_EMPLOYER_DEDUCTION;
        
        return testLiabilityPayslipDeduction;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Income object
1. Id FinancialAssessmentP this is the test Income's parent financial assessment
==================================================================================================================================**/
    public static Income__c setIncomeFields(Id FinancialAssessmentP){
        
        Income__c testIncome = new Income__c();
        
        testIncome.Type__c = StringConstants.TEST_DATA_INCOME_SALARY_TYPE;
        testIncome.Gross_Amount__c = StringConstants.TEST_DATA_INCOME_GROSS_AMOUNT;
        testIncome.Beneficiary__c = StringConstants.TEST_DATA_INCOME_CLIENT_BENEFICIARY;
        testIncome.Financial_Assessment__c = FinancialAssessmentP;
        
        return testIncome;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Case object

1. Id caseIdP this is the test case's Id
2. Id liabilityIdP this is the test liability's Id
===================================================================================================================================**/
    public static Case_Liability__c setCaseLiabilityFields(Id caseIdP, Id liabilityIdP){
        
        Case_Liability__c testCaseLiability = new Case_Liability__c();
        
        testCaseLiability.Case__c = caseIdP;
        testCaseLiability.Liability__c = liabilityIdP;
        
        return testCaseLiability;
        
    }
    
    /**==============================================================================================================================
* Sets record fields and returns a populated instance of the Attachment object
==================================================================================================================================**/
    public static Attachment setAttachmentFields(){
        
        Attachment attachment = new Attachment();
        
        return attachment;
        
    }
    
    public static Entitlement setEntitlementFields(Id accountId){
        
        Entitlement entitlement = new Entitlement();
        entitlement.AccountId = accountId;
        entitlement.Name = StringConstants.EMAIL_SUBJECT_TEXT;
        return entitlement;
        
    }
    
    public static Account createAccount(){
        Account acc = new Account();
        acc.Simplicity_RID_Number__c = 20;
        acc.Name = 'This is an example note.';
        
        return acc;
    }
    
    public static Document_Link__c setDocumentLinkFields(Contact contact){
        
        Document_Link__c docLink = new Document_Link__c();
        docLink.Contact__c = contact.Id;
        docLink.Type__c = StringConstants.DOC_TYPE_FAL_RECEIVED;
        docLink.Name = StringConstants.DOC_TYPE_FAL_RECEIVED;
        docLink.Document_URL__c = 'www.example.com';
        return docLink;
        
    }
    
    
}
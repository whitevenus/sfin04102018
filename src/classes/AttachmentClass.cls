/**=================================================================================================================================
* Created By: Ram
* Created Date: 02/07/2018
* Description: Uses handler to get document URL from a given case

* Change History:
* None
===================================================================================================================================**/
public class AttachmentClass{
    
    /**=================================================================================================================================
* Gets document URL from document Links object to be sent with email based on parameter(s) provided
* Invocable Methods only take collections as parameters

1. List <Case> caseListP this is our Case list sent from the onSmsCaseInvocable Process Builder.
===================================================================================================================================**/
    public String templateInquestion {get; set;}
    public String receipientId {get; set;}
    public List <Document_Link__c> documentAttachmentlinks {get; set;}
    Public List <String> Attachments = new List <String>();
    public List <Template_Attachment_Option__mdt> attachmentMetadata = new List <Template_Attachment_Option__mdt> ();
    Set <id> caseLiabilityIdSet = new Set<id>();  
    Public List <Account> creditProviderList {get;set;}
    Public ID relatedCaseID ;
    
    public ID getrelatedCaseID(){
        
        return relatedCaseID;
    }
    
    public void setrelatedCaseID(ID s){
        relatedCaseID = s;
        AttachmentClass();
    }
    
    
    
    
    Public void AttachmentClass(){
        System.debug('attachment class constructor ');
       //buildEmailTemplateAttachmentLinks(receipientId,templateInquestion);
       accountRelatedRecs(relatedCaseID);
        
    }
    
    
    public List<Account> accountRelatedRecs(Id caseIdP){
        
        try{
            
            List <Case_Liability__c> caseLiabilityList = [SELECT id,Case__c,Liability__c 
                                                          FROM 
                                                          Case_Liability__c 
                                                          WHERE Case__c =:caseIdP];
            
            System.debug('caseLiability List Debug *****' + caseLiabilityList);
            For(Case_Liability__c cl : caseLiabilityList ){
                
                caseLiabilityIdSet.add(cl.Liability__c);
                
            }
            
            List<Liability__c> liabilityList = [SELECT id,Credit_Provider__c 
                                                FROM Liability__c
                                                WHERE id IN: caseLiabilityIdSet];
            
            Set<id> accountIdSet = new Set<id>();
            
            For(Liability__c liability : liabilityList ){
                
                accountIdSet.add(liability.Credit_Provider__c);
                
            }
            
            
            
            creditProviderList =    [SELECT id,Name,Type,AccountNumber,AnnualRevenue
                                     
                                     FROM Account
                                     WHERE id IN : accountIdSet];
            
            System.debug('Account List Debug *****' + creditProviderList);
            
        }
        
        
        
        
        Catch(Exception e){
            
            GeneralUtilities.customLogger('ATTACHMENT CLASS EXCEPTION : ' + e.getMessage() + 'STACKTRACE!!'+ e.getStackTraceString());
        }
        Return creditProviderList;
    }
    
    
    
    public List<Document_Link__c> buildEmailTemplateAttachmentLinks(String contactIdP, String templateP){
        try{
            
            String metadataquery = '[SELECT Attachment_Name__c FROM Template_Attachment_Option__mdt WHERE Template_Name__c = '+ templateP +'];';
            attachmentMetadata = Database.query(metadataquery); 
            
            
            if(attachmentMetadata.size() > 0 && attachmentMetadata != Null){
                
                for (Template_Attachment_Option__mdt Attachment : attachmentMetadata ){
                    
                    
                    Attachments.addAll(Attachment.Attachment_Name__c.split(','));
                    
                }
                
            }
            
            documentAttachmentlinks = [SELECT Document_URL__c,Type__c
                                       FROM Document_Link__c
                                       WHERE Contact__c =: contactIdP
                                       AND Type__c	IN: Attachments
                                       ORDER BY CreatedDate DESC
                                       LIMIT 1];
            
            system.debug('Links debug'+ documentAttachmentlinks);
            
        }
        Catch(Exception e){
            
            GeneralUtilities.customLogger('ATTACHMENT CLASS EXCEPTION : ' + e.getMessage() + 'STACKTRACE!!'+ e.getStackTraceString() + 'Cause!!' + e.getCause());
            
        }
        return documentAttachmentlinks;
    }  
    
    
    
}
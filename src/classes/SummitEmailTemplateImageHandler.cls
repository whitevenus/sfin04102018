public class SummitEmailTemplateImageHandler {
    
    public static String fileNameP { get; set; }
    
    /**==============================================================================================================================
*Gets Image URL from Metadata Email Template Image URL based on filename provided as parameter from Component
* fileNameP must be passed as public variable because components do not support passing variables in signature
==================================================================================================================================**/
    public static String getimageUrl(){
        
        List <Metadata_Notification_Template_Item__mdt> emailTemplateImageUrlMdtList = MetadataUtilities.getNotificationTemplateItemMetadataList(fileNameP);
        
        if(emailTemplateImageUrlMdtList != Null && emailTemplateImageUrlMdtList.Size() ==1){
            
            return emailTemplateImageUrlMdtList[0].Description__c;
            
        }else{
            
            return null;
            
        }
    }
}
/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 02/07/2018
* Description: Provides additional logic to the Contact Standard Controller

* Change History:
* None
===================================================================================================================================**/
global with sharing class ContactController{
    
    Public List <Contact> contactDataList { get; set; }
    Public List <Financial_Assessment__c> financialAssessmentLiabilitiesList { get; set; }
    Public List<Liability__c> householdExpenseList { get; set; }
    Public List<Liability__c> creditorLoanList { get; set; }
    Public Decimal SurplusShortFall {get; set;}
    Public Contact contact;
    Private Attachment document;
    
    /**=================================================================================================================================
* Class constructor
===================================================================================================================================**/
    public ContactController(ApexPages.StandardController controller){
        
        this.contact = (Contact)controller.getRecord();
        
    }
    
    /**=================================================================================================================================
* Initializes our attachment object
===================================================================================================================================**/
    public Attachment getdocument(){
        
        document = new Attachment();
        
        return document;
    }
    
    /**=================================================================================================================================
* Handles document upload to Summit Fin Service
===================================================================================================================================**/
    public PageReference documentUploadHandler(){
        
        try{
            
            if(document != Null){
                
                String documentType = ApexPages.currentPage().getParameters().get(StringConstants.DOCUMENT_UPLOAD_PAGE_DOC_TYPE);
                
                List <Contact> contactList = [SELECT ID_Number__c, Passport_Number__c
                                              FROM Contact
                                              WHERE Id =: contact.Id
                                              LIMIT 1];
                
                if(contactList != Null && contactList.Size() == 1){
                    
                    String contactIdDocNumber;
                    
                    Contact contact = contactList[0];
                    
                    if(String.isNotBlank(contact.ID_Number__c)){
                        
                        contactIdDocNumber = contact.ID_Number__c;
                        
                    }else{
                        
                        contactIdDocNumber = contact.Passport_Number__c;
                        
                    }
                    
                    //Send Document to Summit Fin Service
                    IntegrationHandler.sendDocumentToSummitFinService(document, contact.Id, documentType, contactIdDocNumber);
                    
                }
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
       
        return null;
    }
    
    /**=================================================================================================================================
* Generates Snapshot of Contact Information in pdf format
===================================================================================================================================**/
    public PageReference Snapshot(){
        
        try{
            
            PdfGeneratorHandler pdfGeneratorHandler = new PdfGeneratorHandler();
            
            //BUILD A MAP FROM A (LIABILITY)SOBJECT LIST FOR LIABILITY
            Map<String, List<SObject>> recordTypeMap =  pdfGeneratorHandler.buildSobjectRecordTypeMap(contact.Id);
            System.debug('recordTypeMap: ' + recordTypeMap.size());
            
            contactDataList = recordTypeMap.get('contactDataList');
            
            financialAssessmentLiabilitiesList = recordTypeMap.get('financialAssessmentLiabilitiesList');
            
            //Verify that financialAssessmentLiabilitiesList is not null and contains an assessment
            if(financialAssessmentLiabilitiesList != Null && financialAssessmentLiabilitiesList.size() == 1){
                
                Financial_Assessment__c assessment = financialAssessmentLiabilitiesList[0];
                
                SurplusShortFall = (assessment.Total_Income__c - (assessment.Total_Monthly_Household_Expenses__c + assessment.Total_Monthly_Debt_Commitments__c));
            }
            
            //CREATE A LIST FOR LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE
            householdExpenseList = recordTypeMap.get(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE);
            
            //CREATE A LIST FOR LIABILITY_RECORD_TYPE_CREDITOR_LOAN
            creditorLoanList = recordTypeMap.get(StringConstants.LIABILITY_RECORD_TYPE_CREDITOR_LOAN);
            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
        
        return null;
    }
}
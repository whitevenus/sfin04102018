/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 22/06/2018
* Description: Provides test methods for General Utilities class

* Change History:
* None
===================================================================================================================================**/
@isTest
public class GeneralUtilitiesTest{
    
    static testMethod void test_logException(){
        
        CalloutException ex =  new CalloutException();
        GeneralUtilities.logException(ex);
        GeneralUtilities.logAndThrowException(ex);
        
    }
    
    static testMethod void test_isSandbox(){
        
        GeneralUtilities.isSandBox();
        
    }
    
    static testMethod void sendMail(){
        
        List<String> bccAddressListP = new List<String> {StringConstants.EMPTY_STRING};
            List<String> toAddressListP = new List<String> {StringConstants.EMPTY_STRING};
                List<String> ccAddressListP = new List<String> {StringConstants.EMPTY_STRING};
                    
                    GeneralUtilities.sendMail(toAddressListP, bccAddressListP, ccAddressListP,
                                              StringConstants.EMPTY_STRING, StringConstants.EMPTY_STRING);
        
        GeneralUtilities.sendMail(toAddressListP, bccAddressListP, ccAddressListP,
                                  StringConstants.EMAIL_SUBJECT_TEXT, StringConstants.EMAIL_BODY_TEXT);
        
        
        toAddressListP = null;  
        
        GeneralUtilities.sendMail(toAddressListP, bccAddressListP, ccAddressListP,
                                  StringConstants.EMAIL_SUBJECT_TEXT, StringConstants.EMAIL_BODY_TEXT);
    }
    
    static testMethod void test_customLogger(){
        
        List<String> messageList = new List<String> {StringConstants.EMPTY_STRING}; 
            
            GeneralUtilities.customLogger(messageList[0]);
        GeneralUtilities.customLogger(messageList);
        
    }
    
    static testMethod void test_getRecordTypeId(){
        
        
        GeneralUtilities.getRecordTypeId(StringConstants.CASE_RECORD_TYPE_PRESCRIPTION);
        GeneralUtilities.getRecordTypeId(StringConstants.EMPTY_STRING);
        
    }
    
    static testMethod void test_convertListToMapWithIdKey(){
        
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_CREDIT_PROVIDER);
        Account accts = TestDataGenerator.setAccountFields(recordTypeId);
        
        List<Account> acct = new List<Account> {accts};
            
            insert acct; 
        
        GeneralUtilities.convertListToMapWithIdKey(acct, 'RecordTypeId');
        
    }
    
    static testMethod void test_convertListToMapWithIdKeyConditional(){
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_CREDIT_PROVIDER);
        Account accts = new Account();
        
        List<Account> acct = new List<Account> {};
            
            GeneralUtilities.convertListToMapWithIdKey(acct, 'RecordTypeId');
        
    }
    
    static testMethod void test_convertListToMapWithStringKey() {
        
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_CREDIT_PROVIDER);
        Account accts = TestDataGenerator.setAccountFields(recordTypeId);
        
        List<Account> acct = new List<Account> {accts};
            

        
        GeneralUtilities.convertListToMapWithStringKey(acct, 'Name');
        
    }
    
    static testMethod void test_convertListToMapWithStringKeyForLookup(){
        
       /*
        //Prepare Contact Creation Data
        Account myAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT myAccount;
        
        Consultant__c myConsultant = TestDataGenerator.setConsultantFields();
        INSERT myConsultant;
        
        Contact myContact = TestDataGenerator.setContactFields(myAccount.Id, myConsultant.Id);
        INSERT myContact;
        
        Financial_Assessment__c myFinancialAssesment = TestDataGenerator.setFinancialAssessmentFields(myContact.Id);
        INSERT myFinancialAssesment;
        
        Id recordTypeId = GeneralUtilities.getRecordTypeId(StringConstants.LIABILITY_RECORD_TYPE_HOUSEHOLD_EXPENSE);
        
        
        Liability__c myLiability = TestDataGenerator.setLiabilityHouseholdExpenseFields(recordTypeId, myFinancialAssesment.Id);
        List<Liability__c> liabilityList = new List<Liability__c> {myLiability};

        Schema.DescribeFieldResult fieldResult = Liability__c.RecordTypeId.getDescribe();
        

        GeneralUtilities.convertListToMapWithStringKeyForLookup(liabilityList, fieldResult , 'Type__c');
*/
    }
    
    static testMethod void test_getParentRecordObjectApiName(){
        
        GeneralUtilities.getParentRecordObjectApiName('Name');
    } 
    
    static testMethod void test_getParentRecordObjectApiNameConditional(){
        
        GeneralUtilities.getParentRecordObjectApiName('Some_Non_Existing_String');
    }
    
    static testMethod void test_preventDeletion(){
        
        
        List<Document_Link__c> documentLinkList = [SELECT Document_URL__c
                                                   FROM Document_Link__c
                                                   ORDER BY CreatedDate DESC
                                                   LIMIT 1];
        
        delete documentLinkList;
    }
}
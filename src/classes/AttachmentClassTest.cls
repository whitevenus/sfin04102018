@isTest
public class AttachmentClassTest {
    
    static testMethod void test_buildEmailTemplateAttachmentLinks(){
        
        String templateInquestion = 'COL 1A – Setting up Stop Order';
        String rectypeid = '0120N000000F1xmQAC';
        Account currentAccount = TestDataGenerator.setAccountFields(GeneralUtilities.getRecordTypeId(StringConstants.ACCOUNT_RECORD_TYPE_EMPLOYER));
        INSERT currentAccount;
        Consultant__c currentConsultant = TestDataGenerator.setConsultantFields();
        INSERT currentConsultant;
        Contact Peter =  TestDataGenerator.setContactFields(currentAccount.id,currentConsultant.id,rectypeid);

        INSERT Peter;
        Document_Link__c DocLink =  TestDataGenerator.setDocumentLinkFields(Peter);
        INSERT DocLink; 
        AttachmentClass Ac = new AttachmentClass();
        Ac.buildEmailTemplateAttachmentLinks(Peter.id, DocLink.type__c);
    }
    
}
/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 03/07/2018
* Description: Represents Response Schema for All REST Api Endpoints

* Change History:
* None
===================================================================================================================================**/
public class IntegrationBeanRESTApiResponse {
    
    Public responses[] responses;
    
    public class responses {
        
        public String status; //Response Status
        public Integer statusCode; //Response Status Code
        public String statusMessage; //Response Message
        
    } 
}
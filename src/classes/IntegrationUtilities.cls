/**=================================================================================================================================
* Created By: Eduardo Salia
* Created Date: 22/06/2018
* Description: Provides integration specific utility methods

* Change History:
* None
===================================================================================================================================**/
public class IntegrationUtilities {
    
    /**==============================================================================================================================
* Performs outbound integration callouts based on parameter(s) provided

1. HttpRequest httpRequestP this is our integration request
==================================================================================================================================**/
    public static HttpResponse serviceCallout(HttpRequest httpRequestP){
        
        HttpResponse httpResponse;
        
        try{
            
            Http http = new Http();
            httpResponse = http.send(httpRequestP);
            
            if(httpResponse.getStatusCode() >= StringConstants.STATUS_CODE_200 && httpResponse.getStatusCode() < StringConstants.STATUS_CODE_300){
                
                Logger.logOutboundIntegration(httpRequestP, httpResponse, StringConstants.DEFAULT_LEVEL);
                
            }else{
                
                Logger.logOutboundIntegration(httpRequestP, httpResponse, StringConstants.EXCEPTION_LEVEL);
                
            }
            
            GeneralUtilities.customLogger(StringConstants.INTEGRATION_UTILITIES_DEBUG_MESSAGE + httpResponse.getStatusCode());
            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            Logger.logOutboundIntegration(httpRequestP, ex);
            
        }
        
        return httpResponse;
        
    }
    
    /**==============================================================================================================================
* Generates access credentials for Summit Integration user to be used in outbound callouts
==================================================================================================================================**/
    public static Map<String, String> getSummitAccessCredentials(){
        
        HttpRequest accessTokenGrantRequest = IntegrationMapFields.mapAccessTokenGrantRequestFields();
        
        if(test.isRunningTest()){
            
            Test.setMock(HttpCalloutMock.class, new MockHttpAccessTokenGrantResponse());
            
        }
        
        //Get Summit Fin Application Access Token
        HttpResponse accessTokenGrantResponse = serviceCallout(accessTokenGrantRequest);
        
        //Assign Response Headers
        String applicationUid = accessTokenGrantResponse.getHeader(StringConstants.HTTP_UID);
        String clientName = accessTokenGrantResponse.getHeader(StringConstants.HTTP_CLIENT_NAME);
        String accessToken = accessTokenGrantResponse.getHeader(StringConstants.HTTP_ACCESS_TOKEN);
        
        //Check if access token, uid and client name returned on response headers
        if(String.isNotBlank(applicationUid) && String.isNotBlank(clientName) && String.isNotBlank(accessToken)){
            
            Map<String, String> summitAccessCredentialsMap = new Map<String, String>();
            
            summitAccessCredentialsMap.put(StringConstants.HTTP_UID, applicationUid);
            summitAccessCredentialsMap.put(StringConstants.HTTP_CLIENT_NAME, clientName);
            summitAccessCredentialsMap.put(StringConstants.HTTP_ACCESS_TOKEN, accessToken);
            
            if(summitAccessCredentialsMap != Null && summitAccessCredentialsMap.isEmpty() == False){
                
                return summitAccessCredentialsMap;
                
            }else{
                
                return null;
                
            }
        }else{
            
            GeneralUtilities.customLogger(StringConstants.ERROR_MESSAGE_UNABLE_TO_GET_ACCESS_TOKEN);
            
        }
        
        return null;
    }
}
@isTest
global class MockHttpLoggerCalloutResponse implements HttpCalloutMock {
    
    /**==============================================================================================================================
* This function will return a response when MockHttpLoggerCalloutResponse is invoked

1. HTTPRequest httpRequestP parameter is needed because the respond method is implemented from the HttpCalloutMock interface
==================================================================================================================================**/
    global HttpResponse respond(HTTPRequest httpRequestP){
        
        
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setHeader(StringConstants.HTTP_CONTENT_TYPE, StringConstants.HTTP_APPLICATION_JSON);
        httpResponse.setBody(StringConstants.LOGGER_CALLOUT_RESPONSE_BODY);
        httpResponse.setStatusCode(StringConstants.STATUS_CODE_200);
        
        return httpResponse;
        
    }
}
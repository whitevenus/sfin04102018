global with sharing class LiabilityController{
    
    Public Liability__c liability;
    Private Attachment document;
    
    /**=================================================================================================================================
* Class constructor
===================================================================================================================================**/
    public LiabilityController(ApexPages.StandardController controller){
        
        this.liability = (Liability__c)controller.getRecord();
        
    }
    
    /**=================================================================================================================================
* Initializes our attachment object
===================================================================================================================================**/
    public Attachment getdocument(){
        
        document = new Attachment();
        
        return document;
    }
    
    /**=================================================================================================================================
* Handles document upload to Summit Fin Service
===================================================================================================================================**/
    public PageReference documentUploadHandler(){
        
        try{
            
            if(document != Null){
                
                String documentType = ApexPages.currentPage().getParameters().get(StringConstants.DOCUMENT_UPLOAD_PAGE_DOC_TYPE);
                
                List <Liability__c> liabilityList = [SELECT Financial_Assessment__c, Date_Certificate_of_Balance_Received__c,
                                                     Date_Final_Acceptance_Letter_Received__c, Date_Provisional_Acceptance_Received__c
                                                     FROM Liability__c
                                                     WHERE Id =: liability.Id
                                                     LIMIT 1];
                
                if(liabilityList != Null && liabilityList.Size() == 1){
                    
                    Liability__c liability = liabilityList[0];
                    
                    List <Contact> contactList = [SELECT ID_Number__c, Passport_Number__c
                                                  FROM Contact
                                                  WHERE Active_Financial_Assessment__c =: liability.Financial_Assessment__c
                                                  LIMIT 1];
                    
                    if(contactList != Null && contactList.Size() == 1){
                        
                        String contactIdDocNumber;
                        
                        Contact contact = contactList[0];
                        
                        if(String.isNotBlank(contact.ID_Number__c)){
                            
                            contactIdDocNumber = contact.ID_Number__c;
                            
                        }else{
                            
                            contactIdDocNumber = contact.Passport_Number__c;
                            
                        }
                        
                        if(documentType == StringConstants.DOC_TYPE_COB_RECEIVED){
                            
                            liability.Date_Certificate_of_Balance_Received__c = date.today();
                            
                        }else if(documentType == StringConstants.DOC_TYPE_PAL_RECEIVED){
                            
                            liability.Date_Provisional_Acceptance_Received__c = date.today();
                            
                        }else if(documentType == StringConstants.DOC_TYPE_FAL_RECEIVED){
                            
                            liability.Date_Final_Acceptance_Letter_Received__c = date.today();
                            
                        }
                        
                        UPDATE liability;
                        
                        //Send Document to Summit Fin Service
                        IntegrationHandler.sendDocumentToSummitFinService(document, contact.Id, documentType, contactIdDocNumber);
                        
                    }
                }
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
        
        return null;
    }
}
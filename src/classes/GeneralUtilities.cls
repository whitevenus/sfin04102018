/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 22/06/2018
* Description: Provides general use utility methods

* Change History:
* None
===================================================================================================================================**/
public class GeneralUtilities {
    
    /**==============================================================================================================================
* Determines what type of org environment we are currently logged into.
==================================================================================================================================**/
    public Static Boolean isSandBox(){
        
        Boolean isSandBox;
        Organization organization = [SELECT IsSandbox FROM Organization LIMIT 1];
        
        if( organization.IsSandbox ){
            
            isSandBox = True;
            
        }else{
            
            isSandBox = False;
            
        }
        
        return isSandBox;
    }
    
    /**==============================================================================================================================
* Builds email recipients list, body and subject based on parameter(s) provided

1. List <String> toAddressListP this is the list of To Address for the email
2. List <String> bccAddressListP this is the list of BCC Address for the email
3. List <String> ccAddressListP this is the list of CC Address for the email
4. String subjectP this is our email subject
5. String bodyP this is our email body
==================================================================================================================================**/
    public static void sendMail(List <String> toAddressListP, List <String> bccAddressListP, List <String> ccAddressListP, String subjectP, String bodyP){
        
        //Instance of the messaging class responsible for the sending of the mail
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        
        //Check if email has primary recipients
        if(toAddressListP != Null && toAddressListP.size() > 0){
            
            emailMessage.setToAddresses(toAddressListP);
            
            //Check if email has secondary CC recipients
            if(ccAddressListP != Null && ccAddressListP.size() > 0){
                
                emailMessage.setBccAddresses(ccAddressListP);
                
            }
            
            //Check if email has secondary BCC recipients
            if(bccAddressListP != Null && bccAddressListP.size() > 0){
                
                emailMessage.setBccAddresses(bccAddressListP);
                
            }
            
            //Set our opt out policy
            emailMessage.optOutPolicy = StringConstants.SEND_EMAIL_OPT_OUT_POLICY;
            
            //Set email subject
            if(String.isNotBlank(subjectP)){
                
                emailMessage.setsubject(subjectP);
                
            }
            
            //Set email body
            if(String.isNotBlank(bodyP)){
                
                emailMessage.setplainTextBody(bodyP);
                
            }
            
            try{
                
                Messaging.SendEmailResult[] sendEmailResult = Messaging.sendEmail( new Messaging.SingleEmailMessage[] {emailMessage});
                
            }catch(System.EmailException ex){
                
                logException(ex);
                
            }
            
        }else{
            
            customLogger(StringConstants.ERROR_MESSAGE_NO_RECIPIENT_PROVIDED_FOR_SEND_EMAIL_UTILITY);
            
        }
    }
    
    /**==============================================================================================================================
* Generates a log of exception with details about error message, line number and exception type
* Throws exception at the UI level

1. Exception exP this is the exception body
==================================================================================================================================**/
    public static void logAndThrowException(Exception exP){
        
        System.debug('***** GeneralUtilites.logAndThrowException *****');
        
        if(exP != null){
            
            System.debug('Exception Message: ' + exP.getMessage());
            System.debug('Exception Line Number: ' + exP.getLineNumber());
            System.debug('Exception Stack Trace: ' + exP.getStackTraceString());
            
            //throw new CustomException(exP.getMessage());
            
        }else{
            
            System.debug('No Exception Body Received');
            
        }
    }
    
    /**==============================================================================================================================
* Generates a log of exception with details about error message, line number and exception type

1. Exception exP this is the exception body
==================================================================================================================================**/
    public static void logException(Exception exP){
        
        System.debug('***** GeneralUtilites.logException *****');
        
        if(exP != null){
            
            System.debug('Exception Message: ' + exP.getMessage());
            System.debug('Exception Line Number: ' + exP.getLineNumber());
            System.debug('Exception Stack Trace: ' + exP.getStackTraceString());
            
        }else{
            
            System.debug('No Exception Body Received');
            
        }
    }
    
    /**==============================================================================================================================
* Generates a print log based on parameter(s) provided

1. String messageP this is the value that will be printed
==================================================================================================================================**/
    public static void customLogger(String messageP){
        
        System.debug('***** GeneralUtilites.customLogger *****');
        System.debug('Custom Message: ' + messageP);
        
    }
    
    /**==============================================================================================================================
* Generates a print log based on parameter(s) provided

1. List <String> messageListP this is the value list that will be printed
==================================================================================================================================**/
    public static void customLogger(List <String> messageListP){
        
        System.debug('***** GeneralUtilites.customLogger *****');
        System.debug('Custom Message: ' + messageListP);
        
    }
    
    /**==============================================================================================================================
* Gets record type Id based on parameter(s) provided

1. String recordTypeNameP this is the developer name of the record type
==================================================================================================================================**/
    public static Id getRecordTypeId(String recordTypeNameP){
        
        List <RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE DeveloperName =: recordTypeNameP LIMIT 1];
        
        if(recordTypeList != Null && recordTypeList.size() > 0){
            
            return recordTypeList[0].Id;
            
        }else{
            
            return NULL;
            
        }
    }
    
    /**==============================================================================================================================
* Prevents Database Operation Commit based on parameter(s) provided
* Prompts user that record cannot be created, changed or deleted

1. List<SObject> sObjectListP this is our list of sObject records
==================================================================================================================================**/
    public static void preventCommit(List<SObject> sObjectListP){
        
        for(SObject record : sObjectListP){
            
            record.addError(StringConstants.ERROR_MESSAGE_RECORD_CHANGE);

        }
    }
    
    /**==============================================================================================================================
* Creates a Map < Id, List<SObject> from a List based on parameter(s) provided

1. List<SObject> sObjectListP this is our list of sObject records
2. String objectFieldNameP this is our sObject field name which will become the map key
==================================================================================================================================**/
    public static Map < Id, List<SObject> > convertListToMapWithIdKey( List<SObject> sObjectListP, String objectFieldNameP ){
        
        Map< Id, List<SObject> > sObjectMap = new Map< Id, List<SObject> >();
        
        if(sObjectListP != Null){
            
            for(SObject sObjectRecord : sObjectListP){
                
                Id mapKeyId = (Id)sObjectRecord.get(objectFieldNameP);
                
                if(!sObjectMap.containsKey(mapKeyId)){
                    
                    List<SObject> sObjectList = new List<SObject>();
                    
                    sObjectList.add(sObjectRecord);
                    sObjectMap.put(mapKeyId, sObjectList);
                    
                }else{
                    
                    List<SObject> sObjectList = sObjectMap.get(mapKeyId);
                    sObjectList.add(sObjectRecord);
                }
            }
        }
        
        return sObjectMap;
        
    }
    
    /**==============================================================================================================================
* Creates a Map < Id, List<SObject> from a List based on parameter(s) provided

1. List<SObject> sObjectListP this is our list of sObject records
2. String objectFieldNameP this is our sObject field name which will become the map key
==================================================================================================================================**/
    public static Map < String, List<SObject> > convertListToMapWithStringKey( List<SObject> sObjectListP, String objectFieldNameP ){
        
        Map< String, List<SObject> > sObjectMap = new Map< String, List<SObject> >();
        
        if(sObjectListP != Null){
            
            for(SObject sObjectRecord : sObjectListP){
                
                String mapKeyString = (String)sObjectRecord.get(objectFieldNameP);
                
                if(!sObjectMap.containsKey(mapKeyString)){
                    
                    List<SObject> sObjectList = new List<SObject>();
                    
                    sObjectList.add(sObjectRecord);
                    sObjectMap.put(mapKeyString, sObjectList);
                    
                }else{
                    
                    List<SObject> sObjectList = sObjectMap.get(mapKeyString);
                    sObjectList.add(sObjectRecord);
                }
            }
        }
        
        return sObjectMap;
        
    }
    
    /**==============================================================================================================================
* Creates a Map < String, List<SObject> from a List based on parameter(s) provided

1. List<SObject> sObjectListP this is our list of sObject records
2. Schema.DescribeFieldResult fieldResultP this is our field result record
3. String objectFieldNameP this is our sObject field name which will become the map key
==================================================================================================================================**/
    public static Map < String, List<SObject> > convertListToMapWithStringKeyForLookup( List<SObject> sObjectListP, Schema.DescribeFieldResult fieldResultP, String objectFieldNameP ){
        
        Map< String, List<SObject> > sObjectMap = new Map< String, List<SObject> >();
        
        if(sObjectListP != Null){
            
            for(SObject sObjectRecord : sObjectListP){
                
                Schema.SObjectfield field = fieldResultP.getSobjectField();
                SObject lookupRecord = sObjectRecord.getSObject(field);
                String mapKeyString = (String)lookupRecord.get(objectFieldNameP);
                
                if(!sObjectMap.containsKey(mapKeyString)){
                    
                    List<SObject> sObjectList = new List<SObject>();
                    
                    sObjectList.add(sObjectRecord);
                    sObjectMap.put(mapKeyString, sObjectList);
                    
                }else{
                    
                    List<SObject> sObjectList = sObjectMap.get(mapKeyString);
                    sObjectList.add(sObjectRecord);
                }
            }
        }
        
        return sObjectMap;
        
    }
    
    /**=================================================================================================================================
* Determines what object called this class

1. Id parentRecordId this is our parent record Id eg(Contact Id, CaseId, ProcessId)
===================================================================================================================================**/
    public static String getParentRecordObjectApiName(String parentRecordIdP){
        
        String objectApiName;
        
        //Get prefix from parent record ID
        String parentRecordIdPrefix = String.valueOf(parentRecordIdP).substring(0,3);
        
        //Get schema information
        Map <String, Schema.SObjectType> schemaDescribe =  Schema.getGlobalDescribe();
        
        //Loop through all the sObject types returned by Schema
        for(Schema.SObjectType schemaType : schemaDescribe.values()){
            
            Schema.DescribeSObjectResult describeResult = schemaType.getDescribe();
            String describeResultKeyPrefix = describeResult.getKeyPrefix();
            
            //Check if the prefix matches with requested parent record ID prefix
            if(describeResultKeyPrefix !=null && describeResultKeyPrefix.equals(parentRecordIdPrefix)){
                
                objectApiName = describeResult.getName();
                break;
                
            }
        }
        
        //Debug will be needed in Production
        GeneralUtilities.customLogger(StringConstants.DEBUG_PARENT_RECORD_OBJECT_NAME + objectApiName);
        
        return objectApiName;
        
    }
}
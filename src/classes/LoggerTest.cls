@isTest
public class LoggerTest {
    
    static TestMethod void settings_test() {
        
        Test.startTest();
        
        Metadata_Application_Option__mdt application_option = Logger.APPLICATION_OPTION;
        
        Test.stopTest();
        
    }
    
    static TestMethod void endpoint_test() {
/*
        String endpoint = 'http://logs-01.loggly.com/';
        
        Test.startTest();
        
        System.assert(Logger.LOGGER_ENDPOINT.startsWith(endpoint), 'Did not get the right endpoint back');
        
        Test.stopTest();*/
    }
    
    static TestMethod void level_test() {
        /*
        String settingName = 'Staging';
        String level_val = 'DEBUG';

        Test.startTest();
        
        System.assertEquals(level_val, Logger.LOGGER_LEVEL, 'Did not get the right level back');
        
        Test.stopTest();*/
    }
    
    static TestMethod void logger_tags_test() {
        String settingName = 'Staging';
        String tag = 'http';

        Test.startTest();
        
        System.assertEquals(Logger.LOGGER_TAGS, tag, 'log tags should be included');
        
        Test.stopTest();
    }
    
    
    static TestMethod void batch_logs_true_test() {
        Logger.BATCH_LOGS = true;
        
        Test.startTest();
        
        System.assert(Logger.BATCH_LOGS, 'We should be batching logs');
        
        Test.stopTest();
    }
    
    static TestMethod void batch_logs_false_test() {
        Logger.BATCH_LOGS = null;
        
        Test.startTest();
        
        System.assert(!Logger.BATCH_LOGS, 'We should not be batching logs');
        
        Test.stopTest();
    }
    static testMethod void logCache_constructor_empty_test() {
        Test.startTest();
        
        Logger.LogCache result = new Logger.LogCache();
        
        Test.stopTest();
        
    }
    
    static testMethod void logCache_constructor_log_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        Logger.Log testLog = new Logger.Log(testValue, testDate, testLevel);
        
        Test.startTest();
        
        Logger.LogCache result = new Logger.LogCache(testLog);
        
        Test.stopTest();
        
    }
    
    static testMethod void logCache_constructor_list_test() {
        String testValue1 = '_unittest_value_1';
        Logger.Log testLog1 = new Logger.Log(testValue1);
        
        String testValue2 = '_unittest_value_2';
        Logger.Log testLog2 = new Logger.Log(testValue2);
        
        List<Logger.Log> testLogs = new List<Logger.Log>{testLog1, testLog2};
            
            Test.startTest();
        
        Logger.LogCache result = new Logger.LogCache(testLogs);
        
        Test.stopTest();
        
        Boolean gotLog1 = false;
        Boolean gotLog2 = false;
        
        for (Logger.Log l: result.logList) {
            
            if (l.logValue == testValue1) {
                
                gotLog1 = true;
                
            } else if (l.logValue == testValue2) {
                
                gotLog2 = true;
                
            }
        }
        
    }
    
    static testMethod void logCache_add_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        Logger.Log testLog = new Logger.Log(testValue, testDate, testLevel);
        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        
        testCache.add(testLog);
        
        Test.stopTest();
        
    }
    
    static testMethod void logCache_flushLogs_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        String settingName = 'Staging';

        Logger.Log testLog = new Logger.Log(testValue, testDate, testLevel);
        Logger.LogCache testCache = new Logger.LogCache(testLog);
        
        Test.startTest();
        
        // Don't have a good way to really test this outside of checking to make
        // sure it doesn't throw exceptions and that it clears out the log list
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        testCache.flushLogs();
        
        Test.stopTest();
    }
    
    static testMethod void logCache_flushLogs_multi_test() {
        String testValue1 = '_unittest_value_: 001';
        DateTime testDate1 = DateTime.now();
        
        String testValue2 = '_unittest_value_: 002';
        DateTime testDate2 = DateTime.now();
        
        String testLevel = 'ERROR';
        String settingName = 'Staging';
        String hostname_val = '_unitest_hostname_';
        
        Logger.Log testLog1 = new Logger.Log(testValue1, testDate1, testLevel);
        Logger.Log testLog2 = new Logger.Log(testValue2, testDate2, testLevel);
        Logger.LogCache testCache = new Logger.LogCache(new List<Logger.Log>{testLog1, testLog2});
        
        Test.startTest();
        
        // Don't have a good way to really test this outside of checking to make
        //  sure it doesn't throw exceptions and that it clears out the log list
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        testCache.flushLogs();
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_constructor_empty_test() {
        Test.startTest();
        
        Logger testLogger = new Logger();
        
        Test.stopTest();
        
        System.assertNotEquals(null, Logger.cache, 'Inital cache should not be null');
    }
    
    static testMethod void Logger_add_double_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        Logger testLogger = new Logger();
        
        Test.startTest();
        
        testLogger.add(testValue, testDate);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_add_triple_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        Logger testLogger = new Logger();
        
        Test.startTest();
        
        testLogger.add(testValue, testDate, testLevel);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_add_quad_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        String testKey = '_unittest_key_';
        String testData = '_unittest_data_';
        
        Map<String, String> testAdditionalValues = new Map<String, String>{ testKey => testData };
            
            Logger testLogger = new Logger();
        
        Test.startTest();
        
        testLogger.add(testValue, testDate, testLevel, testAdditionalValues);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_flush_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger testLogger = new Logger();
        testLogger.add(testValue, testDate);
        
        Test.startTest();
        
        testLogger.flush();
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_flush_empty_test() {
        
        String settingName = 'Staging';

        Logger testLogger = new Logger();
        
        Test.startTest();
        
        testLogger.flush();
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quint_noBatch_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        String testKey = '_unittest_key_';
        String testData = '_unittest_data_';
        
        Map<String, String> testAdditionalValues = new Map<String, String>{ testKey => testData };

        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testAdditionalValues, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quint_noCache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        String testKey = '_unittest_key_';
        String testData = '_unittest_data_';
        
        Map<String, String> testAdditionalValues = new Map<String, String>{ testKey => testData };

        Logger.BATCH_LOGS = true;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testAdditionalValues);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quint_cache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        String testKey = '_unittest_key_';
        String testData = '_unittest_data_';
        
        Map<String, String> testAdditionalValues = new Map<String, String>{ testKey => testData };

        Logger.BATCH_LOGS = true;
        
        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testAdditionalValues, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quad_noBatch_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quad_noCache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger.LogCache testCache = null;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_quad_cache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_level_noBatch_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_level_noCache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_level_cache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = '_unittest_name_';

        Logger.BATCH_LOGS = true;
        
        Logger testLogger = new Logger();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_cache_noBatch_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_cache_noCache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger.LogCache testCache = null;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_triple_cache_cache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger.LogCache testCache = new Logger.LogCache();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testCache);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_double_noBatch_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_double_noCache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_double_cache_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger testLogger = new Logger();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_exception_test() {
        String testValue = '_unittest_value_';
        DateTime testDate = DateTime.now();
        String testLevel = 'ERROR';
        
        String settingName = 'Staging';

        Logger.THROW_TEST_EXCEPTION = true;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue, testDate, testLevel);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_single_noBatch_test() {
        String testValue = '_unittest_value_';
        
        String settingName = 'Staging';
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_single_noCache_test() {
        String testValue = '_unittest_value_';
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue);
        
        Test.stopTest();
        
    }
    
    static testMethod void Logger_singleLog_single_cache_test() {
        String testValue = '_unittest_value_';
        
        String settingName = 'Staging';

        Logger.BATCH_LOGS = true;
        
        Logger testLogger = new Logger();
        
        Test.startTest();
        // Test.setMock(HttpCalloutMock.class, new CandidateSourcingCalloutMock());
        Logger.singleLog(testValue);
        
        Test.stopTest();
        
    }
    
    static testMethod void test_logOutboundIntegration(){
        /*
        String logLevel = 'DEBUG';
        String testValue = '_unittest_value_';
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        request.setHeader('calloutName', 'requestCalloutName');
        request.setBody('requestBody');
        response.setHeader('calloutName', 'responseCalloutName');
        response.setBody('responseBody');
        
        Test.startTest();
        
        Logger.logOutboundIntegration(request);
        Logger.logOutboundIntegration(request, logLevel);
        Logger.logOutboundIntegration(response);
        Logger.logOutboundIntegration(response, logLevel);
        Logger.logOutboundIntegration(request, response);
        Logger.logOutboundIntegration(request, response, logLevel);
        
        Test.stopTest();
        */
    }
}
/**=================================================================================================================================
* Created By: Eduardo Salia, Danie Booysen and Benjamin Adebowale
* Created Date: 22/06/2018
* Description: Handles display of activities on the custom activity timeline, validates tasks before they can be completed

* Change History:
* Updated by Eduardo Salia (25/06/2018) Added parent object validation on constructor
===================================================================================================================================**/
global class ActivityHandler {
    
    Public String errorMessage {get; set;}
    Public Boolean canDisplayOpenTasks {get; set;}
    Public Boolean canDisplayClosedTasks {get; set;}
    Public Boolean canDisplayContactTasks {get; set;}
    Public List <Task> openTaskList {get; set;}
    Public List <Task> closedTaskList {get; set;}
    
    /**=================================================================================================================================
* Class constructor
===================================================================================================================================**/
    public ActivityHandler(){
        
        String parentRecordId = ApexPages.currentPage().getParameters().get(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_ID);
        String parentRecordObjectName = GeneralUtilities.getParentRecordObjectApiName(parentRecordId);
        
        If(String.IsNotBlank(parentRecordObjectName)){
            
            if(parentRecordObjectName == StringConstants.OBJECT_NAME_CONTACT){
                
                populateTaskListsUsingWhoIdId(parentRecordId);
                
            }else{
                
                populateTaskListsUsingWhatId(parentRecordId);
                
            }
        }
    }
    
    /**=================================================================================================================================
* Populates Open and Closed Task Lists which are displayed on the Activity Timeline based on parameter(s) provided

1. Id whatIdP this is our task parent record Id (e.g Case Id, Process Id)
===================================================================================================================================**/
    public PageReference populateTaskListsUsingWhatId(Id whatIdP){
        
        try{
            
            openTaskList = [SELECT Status, Static_ID__c, Related_Object__c, WhatId, ActivityDate, Subject
                            FROM Task
                WHERE Status != :StringConstants.TASK_STATUS_COMPLETED
                            AND Status != :StringConstants.TASK_STATUS_CANCELLED
                            AND WhatId =: whatIdP
                            ORDER BY Order__c ASC];
            
            closedTaskList = [SELECT Status, Static_ID__c, Related_Object__c, WhatId, WhoId, Completed_Date__c, Subject
                              FROM Task
                              WHERE (Status =: StringConstants.TASK_STATUS_COMPLETED
                                     OR Status =: StringConstants.TASK_STATUS_CANCELLED)
                              AND WhatId =: whatIdP
                              ORDER BY Order__c ASC];
            
            if(openTaskList != null && openTaskList.size() > 0){
                
                canDisplayOpenTasks = True;
                
            }else{
                
                canDisplayOpenTasks = False;
                
            }
            
            if(closedTaskList != null && closedTaskList.size()> 0){
                
                canDisplayClosedTasks = True;
                
            }else{
                
                canDisplayClosedTasks = False;
                
            }
            
            canDisplayContactTasks = False;
            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
        
        return null;
    }
    
    /**=================================================================================================================================
* Populates Open and Closed Task Lists which are displayed on the Activity Timeline based on parameter(s) provided

1. Id whoIdP this is our task parent record Id (e.g Contact Id)
===================================================================================================================================**/
    public PageReference populateTaskListsUsingWhoIdId(Id whoIdP){
        
        try{
            
            closedTaskList = [SELECT Status, Static_ID__c, Related_Object__c, WhatId, WhoId, Completed_Date__c, Subject, CreatedDate
                              FROM Task
                              WHERE (Status =: StringConstants.TASK_STATUS_COMPLETED
                                     OR Status =: StringConstants.TASK_STATUS_CANCELLED)
                              AND WhoId =: whoIdP
                              ORDER BY CreatedDate DESC];
            
            if(closedTaskList != null && closedTaskList.size()> 0){
                
                canDisplayContactTasks = True;
                
            }else{
                
                canDisplayContactTasks = False;
                
            }
            
            canDisplayOpenTasks = False;
            canDisplayClosedTasks = False;
            
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
        
        return null;
    }
    
    /**==================================================================================================================================
* Handles completing task for a parent record, it is called by the relevent complete button on the Activity Timeline Component
==========================================================================================================================================**/
    public void completeTask(){
        
        String taskId = Apexpages.currentPage().getParameters().get(StringConstants.ACTIVITY_HANDLER_GET_PARAMETER_TASK_ID);
        
        try {
            
            List<Task> currentTaskList = [SELECT Status, Static_ID__c, Related_Object__c, WhatId, ActivityDate
                                          FROM Task
                                          WHERE Id =: taskId
                                          LIMIT 1];
            
            if(currentTaskList != null && currentTaskList.size() == 1){
                
                Task task = currentTaskList[0];
                String taskErrorMessage = validateTask(task.Static_ID__c, task.Related_Object__c, task.WhatId, task.ActivityDate);
                
                if(String.isBlank(taskErrorMessage)){
                    
                    task.Status = StringConstants.TASK_STATUS_COMPLETED;
                    UPDATE task;
                    
                }else{
                    
                    errorMessage = taskErrorMessage;
                    
                }
            }
            
        }catch (Exception e) {
            
            GeneralUtilities.logAndThrowException(e);
            
        }
    }
    
    /**==============================================================================================================================
* Validates if task can be completed based on parameter(s) provided

1. String taskStaticIdP this is our task static id
2. String taskRelatedObjectNameP this is our task related object name
3. Id taskWhatIdP this is our task parent record Id (e.g Case Id, Process Id)
4. Date taskDueDateP this is our task due date

* Due Date must be String to cater for javascript date formatting issue
==================================================================================================================================**/
    @RemoteAction
    global static String validateTask(String taskStaticIdP, String taskRelatedObjectNameP, Id taskWhatIdP, String taskDueDateP){
        
        String taskErrorMessage = validateTask(taskStaticIdP, taskRelatedObjectNameP, taskWhatIdP, date.parse(taskDueDateP));
        
        return taskErrorMessage;
        
    }
    
    /**==============================================================================================================================
* Validates if task can be completed based on parameter(s) provided

1. String taskStaticIdP this is our task static id
2. String taskRelatedObjectNameP this is our task related object name
3. Id taskWhatIdP this is our task parent record Id (e.g Case Id, Process Id)
4. Date taskDueDateP this is our task due date
==================================================================================================================================**/
    global static String validateTask(String taskStaticIdP, String taskRelatedObjectNameP, Id taskWhatIdP, Date taskDueDateP){
        
        String taskErrorMessage = StringConstants.EMPTY_STRING;
        
        try{
            
            if(taskRelatedObjectNameP == StringConstants.TASK_RELATED_OBJECT_CASE){
                
                List <Case> caseList = getCaseInformation(taskWhatIdP);
                
                if(caseList != Null){
                    
                    Case currentCase = caseList[0];
                    
                    if(UserInfo.getUserId() != currentCase.Case_Consultant_Name__c){
                        
                        taskErrorMessage = StringConstants.ERROR_MESSAGE_TAKE_OWNERSHIP_OF_CASE_TO_COMPLETE_TASK;
                        
                    }
                    
                    if(taskStaticIdP == StringConstants.TASK_TEMP_VERIFY_CASE_DOCUMENTS){
                        
                        if(currentCase.RecordType.DeveloperName == StringConstants.CASE_RECORD_TYPE_AUDIT_LOAN_AUDIT
                           && currentCase.Loan_Agreement__c == False){
                               
                               taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_LOAN_AGREEMENTS;
                               
                           }
                        
                        if(currentCase.RecordType.DeveloperName == StringConstants.CASE_RECORD_TYPE_AUDIT_RECKLESS_LENDING
                           && currentCase.Carstens_Gericke_Mandate__c == False){
                               
                               taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_CG_MANDATE;
                               
                           }
                        
                        if(currentCase.RecordType.DeveloperName == StringConstants.CASE_RECORD_TYPE_JUDGEMENT_EAO_SOFT_COLLECTION
                           && currentCase.EAO_Attached__c == False){
                               
                               taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_EAO;
                               
                           }
                        
                        if(currentCase.Power_Of_Attorney_Signed__c == False){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_POA;
                            
                        }
                        
                        if(currentCase.Latest_Payslips__c == False){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_PAYSLIP;
                            
                        }
                        
                        if(currentCase.ID__c == False){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_ID_DOC;
                            
                        }
                    }
                    
                    if(taskStaticIdP == StringConstants.TASK_TEMP_CERTIFICATE_OF_BALANCE_RECEIVED
                       && taskDueDateP > System.today()){
                           
                           List <Case_Liability__c> caseLiabilityList = [SELECT Id
                                                                         FROM Case_Liability__c
                                                                         WHERE Case__c =: taskWhatIdP
                                                                         AND Liability__r.Date_Certificate_of_Balance_Received__c != Null
                                                                         LIMIT 1];
                           
                           if((currentCase.Certificate_Of_Balance__c == False)
                              || (caseLiabilityList == Null || caseLiabilityList.size() == 0)){
                                  
                                  taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_COB_RECEIVED_TO_COMPLETE_TASK;
                                  
                              }
                       }
                    
                    if(taskStaticIdP == StringConstants.TASK_TEMP_PROVISIONAL_ACCEPTANCE_LETTER_RECEIVED
                       && taskDueDateP > System.today()){
                           
                           List <Case_Liability__c> caseLiabilityList = [SELECT Id
                                                                         FROM Case_Liability__c
                                                                         WHERE Case__c =: taskWhatIdP
                                                                         AND Liability__r.Date_Provisional_Acceptance_Received__c = Null
                                                                         LIMIT 1];
                           
                           if((currentCase.Provisional_Proposal__c == False)
                              || (caseLiabilityList == Null || caseLiabilityList.size() == 0)){
                                  
                                  taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_PROVISIONAL_ACCEPTANCE_LETTER_TO_COMPLETE_TASK;
                                  
                              }
                       }
                    
                    if(taskStaticIdP == StringConstants.TASK_TEMP_FINAL_ACCEPTANCE_LETTER_RECEIVED
                       && taskDueDateP > System.today()){
                           
                           List <Case_Liability__c> caseLiabilityList = [SELECT Id
                                                                         FROM Case_Liability__c
                                                                         WHERE Case__c =: taskWhatIdP
                                                                         AND Liability__r.Date_Final_Acceptance_Letter_Received__c = Null
                                                                         LIMIT 1];
                           
                           if((currentCase.Provisional_Proposal__c == False)
                              || (caseLiabilityList == Null || caseLiabilityList.size() == 0)){
                                  
                                  taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_FINAL_ACCEPTANCE_LETTER_TO_COMPLETE_TASK;
                                  
                              }
                       }
                    
                    if(taskStaticIdP == StringConstants.TASK_TEMP_REQUEST_CLIENT_MANDATE){
                        
                        if(currentCase.NCT_Power_Of_Attorney__c == False){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_NCT_POA;
                            
                        }
                        
                        if(currentCase.Payroll_Consent_Order__c == False && currentCase.Debit_Order_Mandate__c == False){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_PAYROLL_CONSENT_ORDER;
                            
                        }
                    }
                }//end of caseList If Block
            }//end of Task Related Object Case If Block
            
            if(taskRelatedObjectNameP == StringConstants.TASK_RELATED_OBJECT_PROCESS){
                
                List <Process__c> processList = getProcessInformation(taskWhatIdP);
                
                if(processList != Null){
                    
                    Process__c process = processList[0];
                    
                    List <Case> caseList = getCaseInformation(process.Case__c);
                    
                    if(caseList != Null){
                        
                        Case parentCase = caseList[0];
                        
                        if(UserInfo.getUserId() != process.Process_Consultant__c){
                            
                            taskErrorMessage = StringConstants.ERROR_MESSAGE_TAKE_OWNERSHIP_OF_PROCESS_TO_COMPLETE_TASK;
                            
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_VERIFY_PROCESS_DOCUMENTS
                           || taskStaticIdP == StringConstants.TASK_TEMP_VERIFY_COURT_DOCUMENTS){
                               
                               if(parentCase.ID__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_ID_DOC;
                                   
                               }
                               
                               if(parentCase.Power_Of_Attorney_Signed__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_POA;
                                   
                               }
                               
                               if(parentCase.Signed_Form_16__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_FORM_16;
                                   
                               }
                               
                               if(parentCase.Debt_Repayment_Proposal__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_DEBT_REPAYMENT_PROPOSAL;
                                   
                               }
                               
                               if(parentCase.Form_TI_138__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_FORM_138;
                                   
                               }
                               
                               if(parentCase.Debt_Restructuring_Proposal__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_DEBT_RESTRUCTURING_PROPOSAL;
                                   
                               }
                               
                               if(parentCase.Proposal_Schedule_Summary__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_PROPOSAL_SCHEDULE_SUMMARY;
                                   
                               }
                               
                               if(parentCase.Revised_Budget__c == False){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_RB;
                                   
                               }
                               
                               if(taskStaticIdP == StringConstants.TASK_TEMP_VERIFY_PROCESS_DOCUMENTS
                                  && parentCase.Draft_Consent_Order__c == False){
                                      
                                      taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_DRAFT_CONSENT_ORDER;
                                      
                                  }
                               

                           }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_PROOF_OF_PAYMENT_RECEIVED){
                            
                            if(parentCase.NCT_Proof_Of_Payment__c == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_NCT_PROOF_OF_PAYMENT;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_GET_TRIBUNAL_COURT_DATE
                           || taskStaticIdP == StringConstants.TASK_TEMP_COURT_DATE_ISSUED){
                               
                               if(process.Court_Date__c == Null){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_COURT_DATE_REQUIRED;
                                   
                               }
                               
                               if(String.isBlank(process.Court_Case_Number__c)){
                                   
                                   taskErrorMessage = StringConstants.ERROR_MESSAGE_COURT_CASE_NUMBER_REQUIRED;
                                   
                               }
                           }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_APPOINT_CORRESPONDING_ATTORNEY
                           && process.Corresponding_Attorney__c == Null){
                               
                               taskErrorMessage = StringConstants.ERROR_MESSAGE_CORRESPONDING_ATTORNEY_REQUIRED;
                               
                           }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_CANCELLATION_OF_DEBIT_ORDER_SIGNED){
                            
                            if(parentCase.Debit_Order_Cancellation_Form__c == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_DEBIT_ORDER_CANCELLATION_FORM;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_FIRST_FEES_PAID){
                            
                            if(process.First_Payment_Amount__c == Null){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_FIRST_PAYMENT;
                                
                            }
                            
                            if(process.First_Payment_Date__c == Null){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_FIRST_DATE;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_SECOND_FEES_PAID){
                            
                            if(process.Second_Payment_Amount__c == Null){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_SECOND_PAYMENT;
                                
                            }
                            
                            if(process.Second_Payment_Date__c == Null){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_ADD_SECOND_DATE;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_CLIENT_STOP_ORDER_ACTIVATED){
                            
                            List <Metadata_Application_Option__mdt> applicationOptionMdtList = MetadataUtilities.getApplicationOptionMetadataList(StringConstants.APPLICATION_OPTION_DEFAULT);
                            
                            if(applicationOptionMdtList != Null && applicationOptionMdtList.size() == 1){
                                
                                List <String> selectedLendersList = applicationOptionMdtList[0].Client_Stop_Order_Selected_Creditors__c.split(StringConstants.SEMI_COLON_STRING);
                                
                                List <Case_Liability__c> caseLiabilityList = [SELECT Id
                                                                              FROM Case_Liability__c
                                                                              WHERE Case__c =: parentCase.Id
                                                                              AND Liability__r.Credit_Provider__r.Name IN : selectedLendersList
                                                                              LIMIT 1];
                                
                                if(caseLiabilityList == Null || caseLiabilityList.size() == 0){
                                    
                                    if(parentCase.Hyphen_Mandate__c == False){
                                        
                                        taskErrorMessage = StringConstants.ERROR_MESSAGE_HYPHEN_MANDATE;
                                        
                                    }
                                    
                                }else{
                                    
                                    if(parentCase.Debit_Order_Cancellation_Form__c == False){
                                        
                                        taskErrorMessage = StringConstants.ERROR_MESSAGE_DEBIT_ORDER_CANCELLATION_FORM;
                                        
                                    }
                                }
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_VERIFY_COLLECTIONS_DOCUMENTS){
                            
                            if(parentCase.Debit_Order_Cancellation_Form__c == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_DEBIT_ORDER_CANCELLATION_FORM;
                                
                            }
                            
                            if(parentCase.Hyphen_Mandate__c == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_HYPHEN_MANDATE;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_SEND_FOUNDING_CONFIRMATORY_AFFIDAVIT){
                            
                            if(parentCase.Signed_Founding_Affidavit__c  == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_FOUNDING_AFFIDAVIT;
                                
                            }
                            
                            if(parentCase.Signed_Confirmatory_Affidavit__c  == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_CONFIRMATORY_AFFIDAVIT;
                                
                            }
                        }
                        
                        if(taskStaticIdP == StringConstants.TASK_TEMP_NCT_ORDER_RECEIVED){
                            
                            if(parentCase.Draft_Consent_Order__c == False){
                                
                                taskErrorMessage = StringConstants.ERROR_MESSAGE_UPLOAD_DRAFT_CONSENT_ORDER;
                                
                            }
                        }
                    }//end of caseList If Block
                }//end of processList If Block
                
            }//end of Task Related Object Process If Block
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }//end of Try Catch
        
        return taskErrorMessage;
        
    }
    
    /**=================================================================================================================================
* Queries Case object and returns a populated instance of the Case object based on parameter(s) provided

1. Id caseIdP this is our case Id
===================================================================================================================================**/
    public static List <Case> getCaseInformation(Id caseIdP){
        
        List <Case> caseList = [SELECT Id, Case_Consultant_Name__c, Power_Of_Attorney_Signed__c,
                                Latest_Payslips__c, Loan_Agreement__c, ID__c, Carstens_Gericke_Mandate__c,
                                EAO_Attached__c, ContactId, Signed_Form_16__c, Passport__c, Credit_Report__c,
                                NCT_Power_Of_Attorney__c, Payroll_Consent_Order__c, Debit_Order_Mandate__c,
                                Certificate_Of_Balance__c, Final_Proposal__c, Provisional_Proposal__c, Form_TI_138__c,
                                Draft_Consent_Order__c, Marriage_Certificate__c, Debt_Repayment_Proposal__c,
                                Proposal_Schedule_Summary__c, Debt_Restructuring_Proposal__c, Revised_Budget__c,
                                Provisional_Acceptance_Letter__c, Final_Acceptance_Letter__c, NCT_Proof_Of_Payment__c,
                                Signed_Founding_Affidavit__c , Signed_Confirmatory_Affidavit__c, RecordType.DeveloperName
                                FROM Case
                                WHERE Id =: caseIdP
                                LIMIT 1];
        
        if(caseList != Null && caseList.size() == 1){
            
            return caseList;
            
        }
        
        return null;
        
    }
    
    /**=================================================================================================================================
* Queries Process object and returns a populated instance of the Process object based on parameter(s) provided

1. Id processIdP this is our process Id
===================================================================================================================================**/
    public static List <Process__c> getProcessInformation(Id processIdP){
        
        List <Process__c> processList = [SELECT Id, Case__c, Process_Consultant__c, Court_Location__c,
                                         Court_Case_Number__c, Court_Date__c, Corresponding_Attorney__c,
                                         First_Payment_Date__c, First_Payment_Amount__c, Second_Payment_Date__c,
                                         Second_Payment_Amount__c
                                         FROM Process__c
                                         WHERE Id =: processIdP
                                         LIMIT 1];
        
        if(processList != Null && processList.size() == 1){
            
            return processList;
            
        }
        
        return null;
        
    }
}
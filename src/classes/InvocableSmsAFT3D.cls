/**=================================================================================================================================
* Created By: Bianca
* Created Date: 21/08/2018
* Description: Extends logic for Case Object (SMS)

* Change History:
* None
===================================================================================================================================**/
public class InvocableSmsAFT3D {
    /**=================================================================================================================================
* Custom Exception class to throw an exception.

1. To be used when test methods are running to throw an exception
===================================================================================================================================**/
    public class customException extends Exception{
        
    }
    
    /**=================================================================================================================================
* Builds SMS message details and invokes integration method to send callout based on parameter(s) provided
* Invocable Methods only take collections as parameters

1. List <Case> caseListP this is our Case list sent from the onSmsCaseInvocable Process Builder
===================================================================================================================================**/
    @InvocableMethod
    public static void buildCaseSmsRequestInvocable(List <Case> caseListP){
        
        try{
            
            if(caseListP != null && caseListP.size() > 0){
                
                //List will always have 1 index element
                Case currentCase = caseListP.iterator().next();
                
                CaseController.handleCommonSmsInvocableLogic(currentCase, StringConstants.PROCESS_SMS_KEY_AFT_3D);
                //If test classes are running an exception is thrown for code coverage for the exception block
                if (test.isRunningTest()){
                    
                    throw new customException();
                    
                }
                
            }
        }catch(Exception ex){
            
            GeneralUtilities.logException(ex);
            
        }
    }
}
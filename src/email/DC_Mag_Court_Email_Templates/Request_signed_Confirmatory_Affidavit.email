<messaging:emailTemplate subject="I need your help – Confirmatory Affidavit" recipientType="User" relatedToType="Process__c">

<messaging:htmlEmailBody >

<head>

    <style>
    
        .Icons{
            display: flex;
        }
        
        body {
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color:rgb(0,32,96);
            font-size: 0.8em;
            display: block;
            margin-left: 5%;
            margin-right: 5%;
          }
          
          .boldParagraph{
          font-weight: bold;
          }
        
    </style>
    
</head>

<body>

    <div class="header">
         <c:summitEmailTemplateImageComponent fileName="DC_Mag_Court_Header_Step_1" />
    </div>

    <div class="body">

        <p class="email_text">Hi {!relatedTo.Case__r.Contact.Name}</p>
        
        <p class="email_text">I’m almost ready to submit your debt counselling court application to the {!relatedTo.Court_Location__c} Magistrate’s Court, but I need your help.</p>
        
        <p class="email_text">This will require a bit of work from your side, but it’s vital to help me finalise your court application.</p>
        
        <p class="email_text"><b>What I need from you –</b> Please will you:</p>
        
        <ol>
            <li>Read through the attached <b>Founding Affidavit</b>, as well as the <b>Annexures</b> (Supporting documents)</li>
            <li>Read through the attached <b>Confirmatory Affidavit</b> written on your behalf and let me know if there’s anything you’d like me to change</li>
            <li><b>Sign</b> the Confirmatory Affidavit and have it <b>commissioned</b> by your nearest <b>Commissioner of Oaths</b> (try a local Attorney or Police Station)</li>
                <ul>
                    <li>Each page must be initialed by both you and the Commissioner</li>
                    <li>The last page must be signed by you and signed and stamped by the Commissioner</li>
                </ul>
            <li>Please drop off the original Confirmatory Affidavit with our Correspondent Attorneys at: <b>{!relatedTo.Attorney_Email__c}</b></li>
        </ol>
        
        <p class="email_text">I’ll collect your <b>signed and commissioned Confirmatory Affidavit</b> from the Correspondent Attorneys and proceed with your court application.</p>
        
        <p class="email_text">Please let me know if you have any questions.</p>
        
        <p class="email_text">Kind Regards,</p>
        
        <p class="email_text">{!relatedTo.Case__r.Case_Consultant_Name__r.Name}</p>
        <p class="email_text">{!relatedTo.Case__r.Case_Consultant_Name__r.UserRole.Name}</p>
        <p class="email_text">Summit Financial Partners</p>
        <p class="email_text">Tel: {!recipient.Phone}</p>
        <p class="email_text">Email: {!recipient.Email}</p>
        <p class="email_text">Case Number: {!relatedTo.Case__r.CaseNumber}</p>
        <apex:outputLink value="www.summitfin.co.za" id="summitfin"> www.summitfin.co.za </apex:outputLink>
        <br/>
        
    </div>
     <c:emailComponent templateName="DC Mag 2A: Send Confirmatory Affidavit to client" contactId="{!recipient.id}"> </c:emailComponent>
    <div class="Icons">
        <div class="Facebook">
            <a href="https://www.facebook.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Facebook_Icon" /> </a>
        </div>
        
        <div class="Twitter">
            <a href="https://twitter.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Twitter_Icon" /> </a>
        </div>
        
        <div class="Blog">
            <a href="http://blog.6cents.co.za/"> <c:summitEmailTemplateImageComponent fileName="Blog_Icon" /> </a>
        </div>
        
    </div>
    
    <div class="footer">
        <c:summitEmailTemplateImageComponent fileName="Consumer_Champions_Footer" />
    </div>

</body>

</messaging:htmlEmailBody>
    <messaging:attachment renderAs="PDF" filename="foundingaffidavit.pdf">
        <c:FoundingAffidavitComponent ></c:FoundingAffidavitComponent>
    </messaging:attachment>
    
    <messaging:attachment renderAs="PDF" filename="confirmatoryaffidavit.pdf">
        <c:ConfirmatoryAffidavitComponent ></c:ConfirmatoryAffidavitComponent>
    </messaging:attachment>
</messaging:emailTemplate>
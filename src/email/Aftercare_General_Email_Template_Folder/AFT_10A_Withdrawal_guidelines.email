<messaging:emailTemplate subject="Withdrawing from debt counselling" recipientType="User" relatedToType="Case">

<messaging:htmlEmailBody >

<head>

    <style>
    
        .Icons{
            display: flex;
        }
        
        body{
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color: rgb(22, 20, 22);
            font-size: small;
            padding: 0px; 
            margin: 0px;
        }
          
        .boldParagraph{
            font-weight: bold;
        }
        
        .docsNeed{
            width: 50%;
        }
        
        .green_header {
            color: #32cd32;
        }
        
    </style>
    
</head>

<body>

    <div class="header">
         <c:summitEmailTemplateImageComponent fileName="Consumer_Champions_Footer" />
    </div>

    <div class="body">

        <p class="email_text">Hi {!relatedTo.Contact.Name}</p>
        
        <p class="email_text">You asked for information about withdrawing from debt counselling.</p>
        
        <p class="green_header">When can you withdraw?</p>
        <p class="email_text">According to the National Credit Regulator (NCR) guidelines, a consumer can only withdraw from debt counselling if:</p>
        <ul>
            <li>Their unsecured debt (i.e. excluding home loans) is paid off, or</li>
            <li>Their debt review court order is rescinded by a court, or</li>
            <li>They get a court order confirming that they are no longer over-indebted</li>
        </ul>
        
        <p class="green_header">What that means for you</p>
        <p class="email_text">If you haven’t yet paid off your unsecured accounts, you would therefore need to appoint an attorney, 
        at your own cost, to apply for a court order confirming that you’re no longer over-indebted or rescinding your original debt review court order.</p>
        
        <p class="email_text">Once you have the above court order, you must send it to me so I can notify your credit providers and update your credit report. 
        Your accounts and credit report would then be updated within 20 to 30 working days.</p>
        
        <p class="email_text">I hope that answers all your questions. Please let me know if there’s anything else you’d like to know.</p>
        
        <p class="email_text">Kind Regards,</p>
        
        <p class="email_text">{!relatedTo.Case_Consultant_Name__r.Name}</p>
        <p class="email_text">{!relatedTo.Case_Consultant_Name__r.UserRole.Name}</p>
        <p class="email_text">Summit Financial Partners</p>
        <p class="email_text">Tel: {!recipient.Phone}</p>
        <p class="email_text">Email: {!recipient.Email}</p>
        <p class="email_text">Case Number: {!relatedTo.CaseNumber}</p>
        <apex:outputLink value="www.summitfin.co.za" id="summitfin"> www.summitfin.co.za </apex:outputLink>
        <br/>
        
    </div>
    
    <div class="Icons">
        <div class="Facebook">
            <a href="https://www.facebook.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Facebook_Icon" /> </a>
        </div>
        
        <div class="Twitter">
            <a href="https://twitter.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Twitter_Icon" /> </a>
        </div>
        
        <div class="Blog">
            <a href="http://blog.6cents.co.za/"> <c:summitEmailTemplateImageComponent fileName="Blog_Icon" /> </a>
        </div>
        
    </div>

</body>

</messaging:htmlEmailBody>

</messaging:emailTemplate>
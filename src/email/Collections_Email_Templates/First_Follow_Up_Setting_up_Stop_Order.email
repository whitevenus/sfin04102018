<messaging:emailTemplate subject="Remember your stop order (debt counselling)" recipientType="User" relatedToType="Process__c">

<messaging:htmlEmailBody >

<head>

    <style>
    
        .Icons{
            display: flex;
        }
        
        body {
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color:rgb(0,32,96);
            font-size: 0.8em;
            display: block;
            margin-left: 5%;
            margin-right: 5%;
          }
          
          .boldParagraph{
          font-weight: bold;
          }
        
    </style>
    
</head>

<body>

    <div class="header">
         <c:summitEmailTemplateImageComponent fileName="Email_Banner_First_Follow_Up_Header" />
    </div>

    <div class="body">

        <p class="email_text">Hi {!relatedTo.Case__r.Contact.Name}</p>
        
        <p class="email_text">Please remember to set a stop order for your monthly debt counselling payments.</p>
        
        <p class="email_text">If you’ve already set up your stop order, please email confirmation to 
        <apex:outputLink value="payments@summitfin.co.za" id="summitfinPayments1"> payments@summitfin.co.za </apex:outputLink> or fax to 086 673 9987.</p>
        
        <p class="email_text">If you have <b>not</b> set up a stop order, please activate it urgently using the details below:</p>
        
        <p class="boldParagraph">How to set up your monthly payment</p>
        <p class="email_text">Please set up a <b>stop order*</b> to Summit’s accredited Payment Distribution Agent (PDA) to ensure your credit providers receive punctual payment each month:</p>
        
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Account:</td>
            </tr>
            <tr>
                <td>Amount: R{!relatedTo.First_Payment_Amount__c} (reviewed annually)</td>
                <td>&nbsp;</td>
                <td>Hyphen Technology (Pty) Ltd</td>
            </tr>
            <tr>
                <td>First payment due: {!relatedTo.First_Payment_Date__c}</td>
                <td>&nbsp;</td>
                <td>First National Bank</td>
            </tr>
            <tr>
                <td>Recurs on: Your pay day</td>
                <td>&nbsp;</td>
                <td>Account #: 62196206413</td>
            </tr>
            <tr>
                <td>Duration: Five years (reviewed annually)</td>
                <td>&nbsp;</td>
                <td>Branch code: 255005</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Ref: {!relatedTo.Case__r.Contact.ID_Number__c}</td>
            </tr>
        </table>
        
        <p class="email_text">Once your stop order has been set up, please email confirmation to 
        <apex:outputLink value="payments@summitfin.co.za" id="summitfinPayments2"> payments@summitfin.co.za </apex:outputLink> or fax to 086 673 9987.</p>
        
        <p class="email_text"><b>*Payments from ABSA accounts:</b> Hyphen’s account is already linked with ABSA’s system. Please set up your stop order using the following: </p>
        <ul>
            <li>Account and Reference number: your ID </li>
            <li>Business Code: 0142239</li>
        </ul>
        
        <p class="email_text"><b>*Payments from Standard Bank accounts:</b> Please do <b>not</b> activate a stop order. Contact us to complete a Hyphen Debit Order Mandate.</p>
        
        <p class="email_text">Feel free to contact me if you have any questions.</p>
        
        <p class="email_text">Kind Regards,</p>
        
        <p class="email_text">{!relatedTo.Case__r.Case_Consultant_Name__r.Name}</p>
        <p class="email_text">{!relatedTo.Case__r.Case_Consultant_Name__r.UserRole.Name}</p>
        <p class="email_text">Summit Financial Partners</p>
        <p class="email_text">Tel: {!recipient.Phone}</p>
        <p class="email_text">Email: {!recipient.Email}</p>
        <p class="email_text">Case Number: {!relatedTo.Case__r.CaseNumber}</p>
        <apex:outputLink value="www.summitfin.co.za" id="summitfin"> www.summitfin.co.za </apex:outputLink>
        <br/>
        
    </div>
    
    <div class="Icons">
        <div class="Facebook">
            <a href="https://www.facebook.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Facebook_Icon" /> </a>
        </div>
        
        <div class="Twitter">
            <a href="https://twitter.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Twitter_Icon" /> </a>
        </div>
        
        <div class="Blog">
            <a href="http://blog.6cents.co.za/"> <c:summitEmailTemplateImageComponent fileName="Blog_Icon" /> </a>
        </div>
        
    </div>
    
    <div class="footer">
        <c:summitEmailTemplateImageComponent fileName="DC_Ambassador_3_Nonesi_Footer" />
    </div>

</body>

</messaging:htmlEmailBody>

</messaging:emailTemplate>
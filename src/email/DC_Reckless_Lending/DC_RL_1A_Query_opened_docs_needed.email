<messaging:emailTemplate subject="Let’s check your {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} {!relatedTo.Related_Liability__r.Type__c} for reckless lending" recipientType="User" relatedToType="Case">
<messaging:HtmlEmailBody >
    
  <head>
      <style>
       body {
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color:rgb(0,32,96);
            font-size: 0.8em;
            display: block;
            margin-left: 5%;
            margin-right: 5%;
          }

        .header {
            margin: auto;
        }

        .header img {
            margin: auto;
            display: block;
        }

        .footer {
            margin: auto;
            bottom: 0;
        }

        .footer img {
            margin: auto;
            display: block;
        }

        .table-img {
            width: 100%;
        }

       
        .date {
            float: right;
        }

        .body {
            /* background-color: red; */
            border: 1px;
            width: 75%;
            margin: 3% auto;
        }

        table td {
            border-left: 1px solid darkgray;
            border-right: 1px solid darkgray;
        }

        table td:first-child {
            border-left: none;
        }

        table td:last-child {
            border-right: none;
        }

        .cg_table_data {
            width: 50%;
            padding-left: 20px;
        }

        .cg_table {
            font-weight: 100;
            font-size: 0.9em;
            color: gray;
            border: none;
            height: 10%;
            border-collapse: collapse;

        }

        .bold-letter {
            color: #9ACD32;
        }

        .email_text {
            text-align: left;
        }
        .light_text{
            color: #a9a9a9;
        }
        hr {
            border-color: #9ACD32;
        }

        .cg_table_data_1 {
            background-color: #9ACD32;
            color: black;
        }

        .green_header {
            color: #32cd32;
        }
        
          .Icons{
            display: flex;
        }
       
    </style>
 </head>
 <body>  
    
    <c:summitEmailTemplateImageComponent fileName="Loan_Audit_Process_Step_1_Collect_doc"> </c:summitEmailTemplateImageComponent>    
    <p class="email_text">
       Hi {!relatedTo.Contact.Name},
    </p>
    <p class="email_text">
       I’d like to investigate your {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} {!relatedTo.Related_Liability__r.Type__c} for reckless lending..
    </p>
    
    <p class="email_text">
        If successful, this could lead to savings which would help you pay off your remaining accounts faster under debt counselling. 
    </p>
    
    <p class="email_text">
        To speed up the process – please send me a copy of the following documents, if you have them:
    </p>
    
    <ul>
        <li>Full credit agreement and pre-agreement quotations</li>
        <li>Proof of income at the time the loan was granted (pay slips or bank statements)</li>
        <li>Credit Report at the time the loan was granted</li>
        <li>{!relatedTo.Related_Liability__r.Credit_Provider__r.Name} account statement</li>
    </ul>
    
    <p class="email_text">
        <b>Don’t worry</b> – if you don’t have copies of these documents, I’ll request them from {credit provider}. 
        However, please note that it can take <b>up to a month</b> for {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} to send us all your documents.
    </p>
    
    <p class="email_text">
        In the meantime, please let me know if you have any questions.
    </p>
    
    <c:summitEmailTemplateImageComponent fileName="What_we_need_from_you"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Loan_agreement"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Pre_agreement_loan_quotati"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Credit_report"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Bank_statement"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Pay_slip"> </c:summitEmailTemplateImageComponent>
    <c:summitEmailTemplateImageComponent fileName="Doc_request_Creditor_statement"> </c:summitEmailTemplateImageComponent>
    
    <p class="email_text">
        Kind Regards,
    </p>
    
    <p class="email_text">
        {!relatedTo.Case_Consultant_Name__r.Name}<br/>
        {!relatedTo.Case_Consultant_Name__r.UserRole}<br/>
        Summit Financial Partners<br/>
        Tel: {!recipient.Phone}<br/>
        Email: {!recipient.Email}<br/>
        Case Number: {!relatedTo.CaseNumber}<br/>
        <a href="http://www.summitfin.co.za"> www.summitfin.co.za </a>
    </p>
    
     <div class="Icons">
        <div class="Facebook">
            <a href="https://www.facebook.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Facebook_Icon" /> </a>
        </div>
        
        <div class="Twitter">
            <a href="https://twitter.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Twitter_Icon" /> </a>
        </div>
        
        <div class="Blog">
            <a href="http://blog.6cents.co.za/"> <c:summitEmailTemplateImageComponent fileName="Blog_Icon" /> </a>
        </div>
        
    </div>
    
    <div class="footer">
        <c:summitEmailTemplateImageComponent fileName="Reckless_Lending_ambassador_1" />
    </div>
    
 </body>          
 
</messaging:HtmlEmailBody>
</messaging:emailTemplate>
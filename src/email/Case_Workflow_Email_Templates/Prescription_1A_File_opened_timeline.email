<messaging:emailTemplate subject=": Let&#39;s check if your {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} account has prescribed" recipientType="User" relatedToType="Case">
<messaging:htmlEmailBody >
<html>

<head>
    <style>
         body {
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color:rgb(0,32,96);
            font-size: 0.8em;
            display: block;
            margin-left: 5%;
            margin-right: 5%;
          }

        .header {
            margin: auto;
        }

        .header img {
            margin: auto;
            display: block;
        }

        .footer {
            margin: auto;
            bottom: 0;
        }

        .footer img {
            margin: auto;
            display: block;
        }

        .table-img {
            width: 100%;
        }

       
        .date {
            float: right;
        }


        table td {
            border-left: 1px solid darkgray;
            border-right: 1px solid darkgray;
        }

        table td:first-child {
            border-left: none;
        }

        table td:last-child {
            border-right: none;
        }

        .cg_table_data {
            width: 50%;
            padding-left: 20px;
        }

        .cg_table {
            font-weight: 100;
            font-size: 0.9em;
            color: gray;
            border: none;
            height: 10%;
            border-collapse: collapse;

        }

        .bold-letter {
            color: #9ACD32;
        }

        .email_text {
            text-align: left;
        }
        .light_text{
            color: #a9a9a9;
        }
        hr {
            border-color: #9ACD32;
        }

        .cg_table_data_1 {
            background-color: #9ACD32;
            color: black;
        }

        .green_header {
            color: #32cd32;
        }
        
          .Icons{
            display: flex;
        }
        
          .textBig{
               font-size:12px;
               font-weight: bold;
           
          }
    </style>
   
</head>
 <body>
        <div class="header">
            <c:summitEmailTemplateImageComponent fileName="Loan_Audit_Process_Step_1_Collect_doc" />
        </div>

        <div class="body">

            <p class="email_text">Hi {!relatedTo.Contact.Name}</p>
            
            <p class="email_text">You asked Summit to check if your {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} {!relatedTo.Related_Liability__r.Type__c} has prescribed. 
                   I’m excited to be handling your file and make sure that your debt is affordable and fair.
            </p>
            
            <p class="email_text"><b>First step</b>  – I’ll request your loan documents from {!relatedTo.Related_Liability__r.Credit_Provider__r.Name} to confirm if your {!relatedTo.Related_Liability__r.Type__c} 
                               has prescribed. It can take up to a month to receive all your documents, but I’ll keep you updated.</p>


            <p class="email_text">In the meantime, please let me know if you have any questions.</p>
                 
            <span class="textBig">What is prescription?</span><br/>
            
                A debt is prescribed when a credit provider has not claimed payment on a debt,
                or issued Summons against you, for three years (30 years for home loans and court orders),
                and you have not acknowledged the debt or made any payment for the same time.              
            
            <p class="email_text">When a debt has prescribed, it becomes unlawful for a credit provider 
                or debt collector to demand payment. That’s where Summit comes in – we’ll confirm if 
                your debt has prescribed and challenge your credit providers where necessary to ensure
                you don’t pay any debts you don’t need to.         
            </p>
            
            <div class="header">
            <c:summitEmailTemplateImageComponent fileName="Prescription infographic" />
        </div>
            
            <p class="email_text">Case Number:{!relatedTo.CaseNumber}</p>
            
            <p class="email_text">Kind Regards,</p>
            
                {!relatedTo.Case_Consultant_Name__r.Name}<br/>
                {!relatedTo.Case_Consultant_Name__r.UserRole.Name}<br/>
                Summit Financial Partners<br/>
                Tel: {!recipient.Phone}<br/>
                Email: {!recipient.Email}<br/>
            
            <apex:outputLink value="www.summitfin.co.za" id="summitfin"> www.summitfin.co.za </apex:outputLink>
        </div>
        
        <div class="Icons">
            <div class="Facebook">
                <a href="https://www.facebook.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Facebook_Icon" /> </a>
            </div>
            
            <div class="Twitter">
                <a href="https://twitter.com/Summitfin"> <c:summitEmailTemplateImageComponent fileName="Twitter_Icon" /> </a>
            </div>
            
            <div class="Blog">
                <a href="http://blog.6cents.co.za/"> <c:summitEmailTemplateImageComponent fileName="Blog_Icon" /> </a>
            </div>
        
        </div>
    </body>
    
</html>
</messaging:htmlEmailBody>

</messaging:emailTemplate>
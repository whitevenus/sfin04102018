<html>
<!-- 
    Subject: {Record Type} unsuccessful – {Contact__c} - ID: {ID__c}
    Objects: Case | Legal Process
    Record Types: NCR | Court | ADR | OMBUD | Reckless Lending 
    Status: Closed or Rejected
    Internal Status- Reckless Lending: Referred for Solution
    Internal Status- Legal Process: Referred back to audit
    Sent to contact and contact consultant
-->

<head>
    <style>
        body {
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            color: rgb(22, 20, 22);
            font-size: small;
            padding: 0px;
            margin: auto;
            width: 65%;
        }

        .header {
            margin: auto;
        }

        .header img {
            margin: auto;
            display: block;
        }

        .footer {
            margin: auto;
            bottom: 0;
        }

        .footer img {
            margin: auto;
            display: block;
        }

        .watermark {
            display: block;
            margin-left: auto;
            margin-right: auto;
            /* width: 65%; */
            background-size: 40%;
            background-image: url("https://c.cs89.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680E000000ACbQ&operationContext=CHATTER&contentId=05T0E000001QFLr");
            background-repeat: no-repeat;
            background-position: right top;
        }

        .date {
            float: right;
        }

        .body {
            /* background-color: red; */
            border: 1px;
            width: 75%;
            margin: 3% auto;
        }

        .email_text {
            text-align: left;
        }

        .green_header {
            color: rgb(50, 205, 50);
        }
    </style>
</head>

<body>
    <div class="watermark">
        <div class="header">
            <img src="https://c.cs89.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Png&versionId=0680E000000ACbR&operationContext=CHATTER&contentId=05T0E000001QFLw"
            />
        </div>
        <div class="body">
            <p class="date">
                {!TODAY()}
            </p><br>
            <p class="email_text">
                <b>Our ref: [CG reference – placed in required field]</b>
            </p>
            <p class="email_text">
                <b>ATTENTION: {!Case.Contact}</b>
                <br> {!Case.Contact_Consultant__c}
            </p>
            <p class="email_text">Dear {!Case.Contact}:</p>

            <p class="email_text">
                <b>RE: {!Case.Contact} // {!Case.Creditor__c}</b>
            </p>
            <p class="email_text">
                <b>MAGISTRATE’S COURT</b>
            </p>
            <p class="email_text">
                <b>RECKLESS LENDING APPLICATION</b>
            </p>
            <br>
            <p class="email_text">We refer to the abovementioned matter.</p>
            <br>
         
            <p class="email_text">
                We are happy to inform you that we have drafted the founding affidavit for your reckless lending application. Please find
                the founding affidavit attached hereto. We request that you read through the founding affidavit, familiarise
                yourself with the content thereof and make sure that you are happy with it.
            </p>

            <p class="email_text">
                We request that you then proceed to print the founding affidavit and go to a commissioner of oaths. A commissioner of oaths
                can be found at your nearest police station or at any attorney’s office.
            </p>

            <p class="email_text">
                We request that you please initial each and every page of the founding affidavit as well as the annexures attached to the
                founding affidavit. This must be done in front of the commissioner of oaths. Please note that you have to
                sign on the last page of the founding affidavit on the line above your name. Then the commissioner of oaths
                must also initial each and every page of the founding affidavit as well as the annexures and sign on the
                line above the name “commissioner of oaths”. If you have any problems during this time please do not hesitate
                to contact us and we will also speak to the commissioner of oaths if necessary.
            </p>

            <p class="email_text">
                Once the above mentioned is done, please scan in the signed founding affidavit and send to the writer hereof. We will then
                confirm via email that the founding affidavit is correctly signed and commissioned and request that you provide
                us with a place and time when we can arrange to have the original document collected from you.
            </p>
            <p class="email_text">We will arrange for the courier to come and pick up the original signed founding affidavit. When the courier
                has picked up the signed affidavit please send us an email to confirm that the signed founding affidavit
                is now with the courier. </p>

            <p class="email_text">We will then make contact again when we have received a court date.</p>

            <p class="email_text">If you have any questions during this process you are welcome to contact us via email ({!01I0E0000001qwD.00N0E000001B8zR}) or
                telephone (087 357 0699).
            </p>
            <p class="email_text">We look forward to helping you in this matter.</p>
            <p class="email_text">Kind Regards,
                <br> <b>CARSTENS GERICKE ATTORNEYS</b>
            </p>
        </div>
    </div>
    <div class="footer">
        <img src="https://c.cs89.content.force.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId=0680E000000ACbG&operationContext=CHATTER&contentId=05T0E000001QFLc"
        />
    </div>

</body>

</html>
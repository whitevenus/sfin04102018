<apex:component access="global" controller="PdfGeneratorHandler">
    <apex:attribute access="global" name="relatedCase" description="Related Case" type="Case" />
    <apex:variable id="currentCase" value="{!relatedCase.Id}" var="currentCaseId"/>
    
    <head>
        
        <apex:stylesheet value="{!$Resource.summit_17_1_17_2_stylesheet}"/>
        
    </head>
    
    <body>
        
        <div class="header">
            <center><apex:image url="{!$Resource.SummitEmailHeader1}" id="HeaderImage"></apex:image></center>
        </div>
        
        <div class="content">
            
            <div class="reference-date" style="width:100%;  text-align:right;">
                <p><b>Reference: Query</b> - {!relatedCase.CaseNumber}</p>
                <p>
                    <b>Date:</b> <apex:outputText value=" {0,date,yyyy'/'MM'/'dd}"><apex:param value="{!Today()}"/></apex:outputText>  
                </p>                            
            </div>
            
           <div class="debtObligationsTable">
                <table>
                    <thead>
                        <tr>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Account Type</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Account Number</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Information</th>
                        </tr>
                    </thead>
                    <tbody >
                        <apex:repeat value="{!liabilityDataList}" var="liability">
                            <tr class="slds-hint-parent">
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Credit_Provider__r.Name}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Type__c}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Loan_Reference_Number__c}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Credit_Provider__r.Account_Email__c}</td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            
            <div class="form_section" style="font-weight:bold; text-align:center;">
                <p><b>FORM 17.1</b></p>
                <p></p>
                <p>
                    <b>NOTIFICATION TO ALL CREDIT PROVIDERS AND REGISTERED CREDIT 
                        BUREAUS IN TERMS OF SECTION 86(4)(b)(i)(ii) OF THE NATIONAL CREDIT 
                        ACT 34 OF 2005</b>
                </p>
                
                
                <p></p>
                <p>
                    <apex:outputText value="{!relatedCase.Contact.Name}"></apex:outputText> - ID#:<apex:outputText value="{!relatedCase.Contact.ID_Number__c}"></apex:outputText>       
                </p>
                <p style="{!IF(relatedCase.Contact.Partner_Contact__c != '', 'display:block', 'display:none')}">
                    <apex:outputText value="{!relatedCase.Contact.Partner_Contact__r.Name}"></apex:outputText> - ID#:<apex:outputText value="{!relatedCase.Contact.Partner_Contact__r.ID_Number__c}"></apex:outputText>       
                </p>
                <p></p>
            </div>
            <p>
                Kindly take note that the above consumer has applied for Debt Review in terms of section 86 
                of the National Credit Act, and has been found to be prima facie over-indebted.
            </p>
            
            <p>
                All credit bureaus have been advised, via the NCR debthelp website, to list the 
                abovementioned consumer as having applied for debt review.
            </p>
            
            <p>
                Please send the following information by no later than 17h00 on 17.05.2018
            </p>
            <p>
                All information to be sent by email to <b><u>cob@summitfin.co.za</u></b> or by fax to: <B>086 673 5902</B>
            </p>
            
            <ol class="ordered_list_a">
                <li>Your Certificate of Balance, including the following:
                    <ol>
                        <li>Account Type</li>   
                        <li>Opening date / Original term of debt (for all secured loans and personal loans)</li>  
                        <li>Full outstanding balance (including arrears)</li>   
                        <li>Full monthly instalment (including all insurance and other monthly charges)</li>   
                        <li>Specification of insurance or other charges, if applicable</li>   
                        <li>Current interest rate</li>   
                    </ol>
                </li> 
                <li>Bank details for the account into which monthly payments should be made by the PDA, 
                    once the payment plan has been finalised</li>   
                <li>Consent to service of the Consumers Court Application by email / fax</li> 
                <li>Full statements of all the consumers accounts</li>   
                <li>If the account has been handed to legal, full Bill of Costs for attorneys or collectors, as 
                    well as all legal letters and documents.
                </li>            
            </ol>  
            
            <p>
                The above is in terms of Section 86(5) of the National Credit Act.
            </p>
            
            <p>
                Please also ensure that the following is done:
            </p>
            
            <ol>
                <li>All the accounts must be amended to indicate that the consumers are under Debt Review</li>   
                <li>
                    All Debit Orders for these accounts must be cancelled, so that the consumer can proceed 
                    with interim payments
                </li>  
                <li>
                    All departments and, if applicable, all collectors or attorneys, must be notified that the 
                    consumer is under Debt Review, and that all legal action and collection proceedings must be 
                    stopped
                </li>   
            </ol>
            
            <p>
                If we have not received your COB after 10 working days, we will continue with the balances 
                as provided by the consumer.
            </p>
            
            <p>
                Values will be inferred where sufficient information is not provided.
            </p>
            
            <div class="static_footer" style="page-break-inside:avoid;">
                <p>Kind Regards,</p>
                <apex:image url="{!$Resource.summit_clark_signature}" id="ClarkSignatureImage" style="width: 20%; height: 20%;"></apex:image>
                <p></p>
                <p>NCRDC44</p>
                <p></p>
                <p>Debt Relief Team</p>
                <p><b>Email:</b> proposals@summitfin.co.za</p>
                <p><b>Fax:</b>086 673 5565</p>
                <p><b>Tel:</b>087 806 1032</p>
            </div>
        </div>
    </body>
</apex:component>
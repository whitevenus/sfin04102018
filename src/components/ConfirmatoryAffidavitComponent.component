<apex:component access="global" controller="PdfGeneratorHandler">
    
    <apex:attribute access="global" name="relatedProcess" description="Related Process" type="Process__c" />
    
    <head>
        
        <style>
        </style>
        
    </head>
    
    <body>
        
        <P ALIGN="CENTER">
            <apex:outputText value="IN THE MAGISTRATES COURT FOR THE DISTRICT OF {!relatedProcess.Court_Location__c}"/> <br/>
            <apex:outputText value="HELD AT {!relatedProcess.Court_Location__c}"/>
        </P>
        <br/>
        <br/>
        <P ALIGN="RIGHT">
            <apex:outputText value="Case Number: {!relatedProcess.Court_Case_Number__c}"/>
        </P>
        <br/>
        In the matter between:<br/>
        <br/>
        
        <table width="100%">
            <tr >
                <td><apex:outputText value="CLARK GARDNER"/></td>
                <td align="RIGHT">APPLICANT <br/><br/></td>
            </tr>
            <tr>
                <td align="LEFT"> AND <br/><br/></td>
            </tr>
            <tr>
                <td align = "LEFT"> <apex:outputText value="{!UPPER(relatedProcess.Case__r.Contact.Name)}"/></td>
                <td align = "RIGHT">RESPONDENT</td>
            </tr>
            <tr>
                <td align = "LEFT"> 
                    <apex:outputText value="ID: {!relatedProcess.Case__r.Contact.ID_Number__c}"/><br/><br/></td>
            </tr>
        </table>
        
        AND<br/>
        <br/>
        
        <table width="100%">
            <apex:repeat value="{!liabilityRespondentSet}" var="creditProvider">
                <tr class="slds-hint-parent">
                    <td class="slds-size--1-of-4" data-label="CREDITOR NAME" >{!creditProvider}</td>
                    <td align="RIGHT">RESPONDENT <br/><br/></td>
                </tr>
            </apex:repeat>
        </table>
        
        <hr/>
        <P ALIGN="CENTER">
            <apex:outputText value="CONFIRMATORY AFFIDAVIT IN SUPPORT OF APPLICATION FOR DEBT REVIEW IN TERMS OF SECTION 86 OF THE NATIONAL CREDIT ACT 34 of 2005 "/>
        </P>
        <hr/>
        
        <p>I, the undersigned,</p>
        
        <p ALIGN="CENTER"><b>{!relatedProcess.Case__r.Contact.Name}</b></p>
        
        <p>Hereby declare under oath:</p>
        
        <ol>
            <li style="padding-top:2%;padding-bottom:2%">
                I am the First Respondent in the matter, a major {!relatedProcess.Case__r.Contact.Gender__c} with
                identity number {!relatedProcess.Case__r.Contact.ID_Number__c}, currently employed at {!relatedProcess.Case__r.Contact.Account.Name}, residing at
                ({!relatedProcess.Case__r.Contact.MailingStreet}, {!relatedProcess.Case__r.Contact.MailingCity}, {!relatedProcess.Case__r.Contact.MailingState}, {!relatedProcess.Case__r.Contact.MailingPostalCode})
                and {!relatedProcess.Case__r.Contact.Marital_Status__c}. I confirm that I am a consumer as defined in the National Credit Act, 34 of 2005 ("the Act").<br/>
                <span style="background-color: #f9fa12;">(insert number of dependants, if the client has any).</span>
            </li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I have read the Founding Affidavit of <b>CLARK GARDNER</b> and confirm the contents thereof insofar as it relates to me.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I confirm that the reasons for over-indebtedness of my estate was not <span style="font-style: italic;">mala fide</span>, as it was never my intention to be a party to a 
                credit agreement of which I could not honour my obligations.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I further confirm that I am aware of the stipulation of Section 88 of the Act, in that I may not incur any further charges under a credit facility 
                or enter into any further credit agreements, other than a consolidation agreement with any credit provider, until all my obligations under the credit agreements as re-arranged, 
                are fulfilled and have been complied with.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">
                I confirm that I have agreed to the following fees being charged:<br/><br/>
                
                <table class="debtReArrangementTable" style="page-break-inside: avoid;border-collapse: collapse;" cellpadding="5" width="100%" border="1" frame="border">
                    <tbody>
                        <tr>
                            <td class="debtReArrangementTable_td" width="50%">Debt counselling fee / Restructuring fee (once-off)</td>
                            <td class="debtReArrangementTable_td" width="50%"><span style="background-color: #f9fa12;">R</span></td>
                        </tr>
                        <tr>
                            <td class="debtReArrangementTable_td" width="50%">Legal fee (once-off)</td>
                            <td class="debtReArrangementTable_td" width="50%"><span style="background-color: #f9fa12;">R</span></td>
                        </tr>
                        
                        <tr>
                            <td class="debtReArrangementTable_td" width="50%">Hyphen PDA fee (monthly)</td>
                            <td class="debtReArrangementTable_td" width="50%"><span style="background-color: #f9fa12;">R</span></td>
                        </tr>
                        
                        <tr>
                            <td class="debtReArrangementTable_td" width="50%">Aftercare fee (monthly)</td>
                            <td class="debtReArrangementTable_td" width="50%"><span style="background-color: #f9fa12;">R</span></td>
                        </tr>
                        
                    </tbody>
                </table>
            </li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I hereby declare that no legal action had been instituted against me with regard to any of the aforementioned credit agreements, 
                as contemplated in Section 130 read with Section 129 of the Act.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I hereby declare that I have no executable movable or immovable property or assets 
                (other than the necessary beds, bedding, clothes, other furniture and household equipment needed by me and my dependants) not forming part of or associated with any credit agreement 
                in the debt review, that can be sold to pay my debts.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I confirm further that I have been making regular payments of the restructured amounts, according to the proposal of my Debt Counsellor.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I confirm that I am committed to the debt review process and undertake to comply fully with the debt restructuring.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I further undertake to advise my debt counsellor of any change in income and/or circumstances that may affect my ability to repay my debt 
                and consent to periodic credit bureau checks to verify my commitment to debt review.</li>
            
            <li style="padding-top:2%;padding-bottom:2%; margin-bottom: 20px;">I accordingly request the above Honourable Court for an order as recommended by the Debt Counsellor, to provide debt-restructuring, in order to 
                enable me to satisfy my obligations under all my credit agreements in a responsible manner.</li>
        </ol>
        
        <br/>
        
        <p ALIGN="RIGHT">_____________________________</p>
        <p ALIGN="RIGHT"><b>{!relatedProcess.Case__r.Contact.Name}</b></p>
        
        <br/>
        
        <p style="line-height: 150%;">
            Thus signed and sworn before me at ______________________________ on this _________day of _______________________________,
            the Deponent having sworn that the contents of this affidavit are true and acknowledged that {!heSheGenderPronoun} knows and understands the contents of the this affidavit, that {!heSheGenderPronoun} 
            has no objection to taking the prescribed oath, and that {!heSheGenderPronoun} considers the prescribed oath to be binding on {!hisHerGenderPronoun} conscience .
        </p>
        
        <br/>
        
        <p>_____________________________</p>
        <p><b>COMMISSIONER OF OATHS</b></p>
        <p>FULL NAMES:</p>
        <p>DESIGNATION:</p>
        <p>BUSINESS ADDRESS:</p>
        <p>EX OFFICIO:</p>
        
    </body>
    
</apex:component>
<apex:component access="global" controller="PdfGeneratorHandler">
    
    <apex:attribute access="global" name="relatedCase" description="Related Case" type="Case" />
    
    <head>
        
        <apex:stylesheet value="{!$Resource.summit_17_1_17_2_stylesheet}"/>
        
    </head>
    
    <body>
        
        <div class="header">
            <center><apex:image url="{!$Resource.SummitEmailHeader1}" id="HeaderImage"></apex:image></center>
        </div>
        
        <div class="content">
            
            <div class="reference-date" style="width:100%;  text-align:right;">
                <p><b>Reference: Query</b> - {!relatedCase.CaseNumber}</p>
                <p>
                    <b>Date:</b> <apex:outputText value=" {0,date,yyyy'/'MM'/'dd}"><apex:param value="{!Today()}"/></apex:outputText>  
                </p>                            
            </div>
            
            <div class="debtObligationsTable">
                <table>
                    <thead>
                        <tr>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Account Type</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Account Number</th>
                            <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Information</th>
                        </tr>
                    </thead>
                    <tbody >
                        <apex:repeat value="{!liabilityDataList}" var="liability">
                            <tr class="slds-hint-parent">
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Credit_Provider__r.Name}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Type__c}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Loan_Reference_Number__c}</td>
                                <td class="slds-size--1-of-4" data-label="SUB STATUS">{!liability.Credit_Provider__r.Account_Email__c}</td>
                            </tr>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            
            <div class="form_section" style="font-weight:bold; text-align:center;">
                <p>FORM 17.2:</p>
                <p>DCRS Provisional Proposal</p>
                <p></p>
                <p>
                    <apex:outputText value="{!relatedCase.Contact.Name}"></apex:outputText> - ID#:<apex:outputText value="{!relatedCase.Contact.ID_Number__c}"></apex:outputText>       
                </p>
                <p style="{!IF(relatedCase.Contact.Partner_Contact__c != '', 'display:block', 'display:none')}">
                    <apex:outputText value="{!relatedCase.Contact.Partner_Contact__r.Name}"></apex:outputText> - ID#:<apex:outputText value="{!relatedCase.Contact.Partner_Contact__r.ID_Number__c}"></apex:outputText>       
                </p>
                <p></p>
            </div>
            
            <p>
                The above consumers have successfully applied for debt review and their debt obligations are
				being restructured.
            </p>
            <p>The consumer solves within the restructuring rules.</p>
            
            <p>
                Please find attached a copy of the consumers Budget, Payment Proposal Summary and monthly payment schedule.
            </p>
            <p>
                Please send your consent to this DCRS payment plan, within <b>5 business days</b>, by email to
                <b><u>proposals@summitfin.co.za</u></b> or by <b>fax</b> to <b>086 673 5565</b>.
            </p>
            <p>
                Please note that if we did not receive your COB (certificate of balance) within the time
                provided, we have continued with the balance provided to us by the consumer, in accordance
                with regulation 24(4) of the National Credit Act Regulations.
            </p>
            <p>Please note that if the annual interest rate is reflected as 0%, this is either because:</p>
            <ol class="ordered_list_a">
                <li>The annual interest rate was reflected as 0% on the COB provided, or</li> 
                <li>No annual interest rate was provided on the COB, or</li>   
                <li>The annual interest rate reflected on the COB exceeded the rate allowed by the NCA, and was written off.</li>   
            </ol>  
            <div class="static_footer" style="page-break-inside:avoid;">
                <p>Kind Regards,</p>
                <apex:image url="{!$Resource.summit_clark_signature}" id="ClarkSignatureImage" style="width: 20%; height: 20%;"></apex:image>
                <p></p>
                <p>NCRDC44</p>
                <p></p>
                <p>Debt Relief Team</p>
                <p><b>Email:</b> proposals@summitfin.co.za</p>
                <p><b>Fax:</b>086 673 5565</p>
                <p><b>Tel:</b>087 806 1032</p>
            </div>
            
        </div>
        
    </body>
</apex:component>
<apex:component access="global" controller="PdfGeneratorHandler">
    
    <apex:attribute access="global" name="relatedProcess" description="Related Process" type="Process__c" />
    
    <head>
        
        <apex:stylesheet value="{!$Resource.summitNctApplicationStyles}"/>
        
    </head>
    
    <body>
        
        <!-- NCT Single Consent Order Start -->
        
        <div class="singleConsentOrder" style="{!IF(relatedProcess.Case__r.Contact.Partner_Contact__c == '', 'display:block', 'display:none')}">
            
            <center><p><b>IN THE NATIONAL CONSUMER TRIBUNAL HELD IN CENTURION</b></p></center>
            
            <p style="text-align: right;"><b>Case no: NCT-</b> {!relatedProcess.Court_Case_Number__c}</p>
            
            <p>In the matter between:</p>
            
            <p style="text-align:right"><b>Applicant</b></p>
            <p><b>CLARK GARDNER</b></p>
            
            <p><b>NCR REGISTRATION NUMBER: NCRDC44</b></p>
            
            <p><b>And</b></p>
            
            <p><b>{!relatedProcess.Case__r.Contact.Name}</b></p>
            <p><b>{!relatedProcess.Case__r.Contact.ID_Number__c}</b></p>
            
            <p><b>And</b></p>
            
            <table>
                <thead>
                    <tr>
                        <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                    </tr>
                </thead>
                <tbody >
                    <apex:repeat value="{!liabilityDataList}" var="liability">
                        <tr class="slds-hint-parent">
                            <td class="slds-size--1-of-4" data-label="CREDITOR NAME" >{!liability.Credit_Provider__r.NAME}</td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
            
            <hr/>
            <center><p><b>CONSENT ORDER</b></p></center>
            <hr/>
            
            <p>
                The Tribunal, having read the papers filed of record and being satisfied that the Consumer
                and the Credit Providers are parties to a debt re-arrangement facilitated by a Debt
                Counsellor under section 86(7)(b) read with section 138(1) of The National Credit Act, 34
                of 2005 (the Act), hereby makes the following order:
            </p>
            
            <p>
                1.  The payment structure between the consumer and the credit providers is confirmed as an order of the Tribunal in terms of section 150 of the Act:
            </p>
            
            <table class="respondentCP_Table" style="page-break-inside: avoid;border-collapse: collapse;" cellpadding="5" border="1" frame="border">
                <thead>
                    <tr>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Reference Number</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Annual Interest Rate</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">New Monthly Installment</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Outstanding Balance</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Estimated Repayment Period in Months</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Start Commencement Date Of Payment (if applicable)</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Monthly / Fees/Insurance / Policy Premium (if applicable)</th>
                    </tr>
                </thead>
                <tbody >
                    <apex:repeat value="{!liabilityDataList}" var="liability">
                        <tr class="slds-hint-parent">
                            <td class="nctConsentOrderCP_td" data-label="CANDIDATE NAME" >{!liability.Credit_Provider__r.NAME}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Loan_Reference_Number__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Interest_Rate__c}%</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">R{!liability.Recurring_Amount__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">R{!liability.Loan_Capital_Outstanding__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Term_in_months__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Effective_Start_Date__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">???</td>
                        </tr>
                    </apex:repeat>
                    
                </tbody>
            </table>
            
            <p>
                2.  The parties' attention is drawn to sections 71(1),71(5)(a) and 152 of the Act which states as follows:
            </p>
            
            <p><b>Section 71(1):</b></p>
            
            <p>
                A consumer, whose debts have been re-arranged in terms of part D of this chapter,
                may apply to a debt counsellor at any time for a clearance certificate
            </p>
            
            <p><b>Section 71(5):</b></p>
            
            <p>
                Upon receiving a copy of a clearance certificate, a credit bureau, or the national
                credit register must expunge from it's records the fact that the consumer was
                subject to the relevant debt re-arrangement order or agreement
            </p>
            
            <p><b>Section 152:</b></p>
            
            <p>
                Any decision, judgement or order of the Tribunal may be served, executed and
                enforced as if it were an order of the High Court.
            </p>
            
            <p>
                3.  The parties are warned that failure to comply with the order of the Tribunal
                constitutes an offence in terms of section 160(1) of the Act
            </p>
            
            <p>
                Dated at ______________________ on this _____day of _______________________2018
<apex:image url="{!$Resource.summit_clark_signature}" id="ClarkSignatureImage1" style="width: 20%; height: 20%;"></apex:image>
                <p></p>
            
            <p><b>Presiding Member</b></p>
            <p><b>National Consumer Tribunal</b></p>
            
        <p>FULL NAMES:</p>
        <p>DESIGNATION:</p>
        <p>BUSINESS ADDRESS:</p>
        <p>AREA:</p>
        </p>
            
        </div>        
        <!-- NCT Single Consent Order End -->
        
        
        <!-- NCT Joint Consent Order Start -->
        
        <div class="jointConsentOrder" style="{!IF(relatedProcess.Case__r.Contact.Partner_Contact__c != '', 'display:block', 'display:none')}">
            
            <center><p><b>IN THE NATIONAL CONSUMER TRIBUNAL HELD IN CENTURION</b></p></center>
            
            <p style="text-align: right;"><b>Case no: NCT-</b> {!relatedProcess.Court_Case_Number__c}</p>
            
            <p>In the matter between:</p>
            
            <p style="text-align:right"><b>Applicant</b></p>
            <p><b>CLARK GARDNER</b></p>
            
            <p><b>NCR REGISTRATION NUMBER: NCRDC44</b></p>
            
            <p><b>And</b></p>
            
            <p><b>Main Applicant Name: {!relatedProcess.Case__r.Contact.Name}</b></p>
            <p><b>Main Applicant ID Number: {!relatedProcess.Case__r.Contact.ID_Number__c}</b></p>
            
            <p><b>Applicant Partner Name: {!relatedProcess.Case__r.Contact.Partner_Contact__r.Name}</b></p>
            <p><b>Applicant Partner ID Number: {!relatedProcess.Case__r.Contact.Partner_Contact__r.ID_Number__c}</b></p>
            
            <p><b>And</b></p>
            
            <table>
                <thead>
                    <tr>
                        <th class="slds-text-heading--label slds-size--1-of-4" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                    </tr>
                </thead>
                <tbody >
                    <apex:repeat value="{!liabilityDataList}" var="liability">
                        <tr class="slds-hint-parent">
                            <td class="slds-size--1-of-4" data-label="CREDITOR NAME" >{!liability.Credit_Provider__r.NAME}</td>
                        </tr>
                    </apex:repeat>
                </tbody>
            </table>
            
            <hr/>
            <center><p><b>CONSENT ORDER</b></p></center>
            <hr/>
            
            <p>
                The Tribunal, having read the papers filed of record and being satisfied that the
                Consumers and the Credit Providers are parties to a debt re-arrangement facilitated by a
                Debt Counsellor under section 86(7)(b) read with section 138(1) of The National Credit
                Act, 34 of 2005 (the Act), hereby makes the following order:
            </p>
            
            <p>
                1.  The payment structure between the consumers and the credit providers is confirmed as an order of the Tribunal in terms of section 150 of the Act:
            </p>
            
            <table class="respondentCP_Table" style="page-break-inside: avoid;border-collapse: collapse;" cellpadding="5" border="1" frame="border">
                <thead>
                    <tr>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Credit Provider</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Reference Number</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Annual Interest Rate</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">New Monthly Installment</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Outstanding Balance</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Estimated Repayment Period in Months</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Start Commencement Date Of Payment (if applicable)</th>
                        <th class="nctConsentOrderCP_th" scope="col"  style = "font-weight:bold;">Monthly / Fees/Insurance / Policy Premium (if applicable)</th>
                    </tr>
                </thead>
                <tbody >
                    <apex:repeat value="{!liabilityDataList}" var="liability">
                        <tr class="slds-hint-parent">
                            <td class="nctConsentOrderCP_td" data-label="CANDIDATE NAME" >{!liability.Credit_Provider__r.NAME}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Loan_Reference_Number__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Interest_Rate__c}%</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">R{!liability.Recurring_Amount__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">R{!liability.Loan_Capital_Outstanding__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Term_in_months__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">{!liability.Effective_Start_Date__c}</td>
                            <td class="nctConsentOrderCP_td" data-label="SUB STATUS">???</td>
                        </tr>
                    </apex:repeat>
                    
                </tbody>
            </table>
            
            <p>
                2.  The parties' attention is drawn to sections 71(1),71(5)(a) and 152 of the Act which states as follows:
            </p>
            
            <p><b>Section 71(1):</b></p>
            
            <p>
                A consumer, whose debts have been re-arranged in terms of part D of this chapter,
                may apply to a debt counsellor at any time for a clearance certificate
            </p>
            
            <p><b>Section 71(5):</b></p>
            
            <p>
                Upon receiving a copy of a clearance certificate, a credit bureau, or the national
                credit register must expunge from it's records the fact that the consumer was
                subject to the relevant debt re-arrangement order or agreement
            </p>
            
            <p><b>Section 152:</b></p>
            
            <p>
                Any decision, judgement or order of the Tribunal may be served, executed and
                enforced as if it were an order of the High Court.
            </p>
            
            <p>
                3.  The parties are warned that failure to comply with the order of the Tribunal
                constitutes an offence in terms of section 160(1) of the Act
            </p>
            
            <p>
                Dated at ______________________ on this _____day of _______________________2018
            </p>
            <apex:image url="{!$Resource.summit_clark_signature}" id="ClarkSignatureImage" style="width: 20%; height: 20%;"></apex:image>
                <p></p>
            
            <p><b>Presiding Member</b></p>
            <p><b>National Consumer Tribunal</b></p>
            
        <p>FULL NAMES:</p>
        <p>DESIGNATION:</p>
        <p>BUSINESS ADDRESS:</p>
        <p>AREA:</p>
            
        </div>
        
        <!-- NCT Joint Consent Order End -->
        
    </body>
    
</apex:component>
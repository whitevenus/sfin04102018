<apex:component access="global" controller="PdfGeneratorHandler">
    
    <apex:attribute access="global" name="relatedCase" description="Related Process" type="Case" />
    
    <head>
        
        <apex:stylesheet value="{!$Resource.summitForm17WithdrawalStyles}"/>
        
    </head>
    
    <body>
        
        <div class="Content">
            
            <div class="headerSection">
                
                <right><apex:image id="headerImage" style="width: 100%; height: 80%;" url="{!$Resource.SummitHeader1}"></apex:image></right>
                
                <p></p>
                
                <p class="paragraphRightHeader">Form 17.W (Withdrawal from Debt Review)</p>
                
            </div>
            
            <div>
                
                <table cellpadding="5">
                    <tr>
                        <th colspan="2" class="rowHeader">
                            Debt Counsellor's Name: Clark Gardner<br/>
                            Registered Debt Counsellor. Registration Number NCRDC44
                        </th>
                    </tr>
                    <tr>
                        <td rowspan="{!liabilityDataListSize}" class="tdBold">To:</td>
                        <td><b>Credit Department(s)</b></td>
                    </tr>
                    <apex:repeat value="{!liabilityDataList}" var="liability" >
                        <tr>
                            <td>
                                Name: {!liability.Credit_Provider__r.NAME}<br/>
                                Address:
                            </td>
                        </tr>
                    </apex:repeat>
                    <tr>
                        <td class="tdBold">From:</td>
                        <td>
                            Debt Counsellor Name: Clark Gardner<br/>
                            Registration number: NCRDC44<br/>
                            Address of Debt Counsellor: Block K Central Park 16th Road,<br/>
                            Midrand, Gauteng 1685<br/>
                            Telephone: 087 806 1011<br/>
                            E-Mail: queries@summitfin.co.za
                        </td>
                    </tr>
                    <tr>
                        <td class="tdBold">Date:</td>
                        <td><apex:outputText value="{0,date,yyyy'/'MM'/'dd}"><apex:param value="{!TODAY()}"/></apex:outputText></td>
                    </tr>
                    <tr>
                        <td class="tdBold">Date Form 16 signed:</td>
                        <td><apex:outputText value="{0,date,yyyy'/'MM'/'dd}"><apex:param value="{!relatedCase.Date_Form_16_Signed__c}"/></apex:outputText></td>
                    </tr>
                    <tr>
                        <th colspan="2" class="rowHeader">Registered Debt Counsellor. Registration Number NCRDC44</th>
                    </tr>
                    <tr>
                        <td class="tdBold">Name and Surname of Consumer(s):</td>
                        <td class="tdBold">ID Number(s):</td>
                    </tr>
                    <tr>
                        <td>{!relatedCase.Contact.Name}</td>
                        <td>{!relatedCase.Contact.ID_Number__c}</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdBold">Account Name(s):</td>
                        <td class="tdBold">Account Number(s):</td>
                    </tr>
                    <apex:repeat value="{!liabilityDataList}" var="liability" >
                        <tr>
                            <td>{!liability.Credit_Provider__r.NAME}</td>
                            <td>{!liability.Loan_Reference_Number__c}</td>
                        </tr>
                    </apex:repeat>
                </table>
                
                <p><b>This notice serves to notify you that:</b></p>
                
                    <p style="{!IF(relatedCase.Internal_Status__c ='Client Withdrew Prior 17.2', 'display:block', 'display:none')}">The consumer has withdrawn from the debt review process prior to issuance of Form 17.2 and the credit
                        bureaus have been updated accordingly via the NCR Debt Help System.</p>
                    <p style="{!IF(relatedCase.Internal_Status__c ='DC Suspended for Non Payment', 'display:block', 'display:none')}">The debt counsellor has suspended provision of service due to non-cooperation by the consumer. The debt
                        counsellor remains the debt counsellor on record.</p>
                    <p style="{!IF(relatedCase.Internal_Status__c ='Court Order to Rescind DC', 'display:block', 'display:none')}">The consumer has obtained a court order to rescind the debt review order. Credit bureaus have been updated
                        via the NCR Debt Help System.</p>
                    <p style="{!IF(relatedCase.Internal_Status__c ='Client No Longer Over Indebted', 'display:block', 'display:none')}">The consumer has obtained a court order declaring the consumer no longer over indebted. Credit bureaus
                        have been updated via the NCR Debt Help System.</p>
                
                <p>Signed at <b>Midrand</b> on this day <b><apex:outputText value="{0,date,dd' of 'MMMM' 'yyyy}"><apex:param value="{!TODAY()}"/></apex:outputText></b></p>
                
                <apex:image id="signature" style="width: 20%; height: 30%;" url="{!$Resource.summit_clark_signature}"></apex:image>
                
                <p>Debt Counsellor Signature</p>
                
            </div>
            
        </div>
        
    </body>
    
</apex:component>
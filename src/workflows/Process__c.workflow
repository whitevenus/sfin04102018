<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Collections_Cancellation_Of_Debit_Order_Signed</fullName>
        <description>Account Collections Cancellation Of Debit Order Signed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Account_Col_Cancellation_DebitOrder</template>
    </alerts>
    <alerts>
        <fullName>Account_Collections_Notification</fullName>
        <description>Account Collections Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Account_Collections_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Collections_Queue_Notification</fullName>
        <description>Account Collections Queue Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Account_Collections_Process_Created</template>
    </alerts>
    <alerts>
        <fullName>Account_Collections_Test_1</fullName>
        <ccEmails>wayde.fagan@tetrad.co.za</ccEmails>
        <ccEmails>danie.booysen@tetrad.co.za</ccEmails>
        <description>Account Collections Test 1</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Account_Collections_Test_1</template>
    </alerts>
    <alerts>
        <fullName>Attorney_Notification_Email_NCR_Ingnored_Rejected</fullName>
        <description>Attorney Notification Email NCR Ingnored/Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/NCR_0A_NCR_Ignored_Rejected</template>
    </alerts>
    <alerts>
        <fullName>COL_1A_Setting_up_Stop_Order</fullName>
        <description>COL 1A - Setting up Stop Order</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>implementation.user@summitfin.co.za</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Setting_up_Stop_Order</template>
    </alerts>
    <alerts>
        <fullName>COL_1B_First_Follow_up_Stop_Order</fullName>
        <description>COL 1B - First Follow up - Stop Order</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/First_Follow_Up_Setting_up_Stop_Order</template>
    </alerts>
    <alerts>
        <fullName>COL_1C_Second_Follow_up_Stop_Order</fullName>
        <description>COL 1C - Second Follow up - Stop Order</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Second_Follow_Up_Setting_up_Stop_Order</template>
    </alerts>
    <alerts>
        <fullName>COL_2A_Request_Debit_Order_Cancellation_Form</fullName>
        <description>COL 2A - Request Debit Order Cancellation Form</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Request_Debit_Order_Cancellation_Form</template>
    </alerts>
    <alerts>
        <fullName>COL_2B_First_Follow_up_Debit_Order_Cancellation_Form</fullName>
        <description>COL 2B - First Follow up - Debit Order Cancellation Form</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Fir_Fol_Up_Debit_Order_Cancellation_Form</template>
    </alerts>
    <alerts>
        <fullName>COL_2C_Second_Follow_up_Debit_Order_Cancellation_Form</fullName>
        <description>COL 2C - Second Follow up - Debit Order Cancellation Form</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Sec_fol_up_Debit_Order_Cancellation_Form</template>
    </alerts>
    <alerts>
        <fullName>COL_3A_First_Payment_Reminder</fullName>
        <description>COL 3A - First Payment Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/First_Payment_Reminder</template>
    </alerts>
    <alerts>
        <fullName>COL_3B_Thank_you_First_Payment_received</fullName>
        <description>COL 3B - Thank you - First Payment received</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Thank_you_First_Payment_received</template>
    </alerts>
    <alerts>
        <fullName>COL_3C_Second_Payment_Reminder</fullName>
        <description>COL 3C - Second Payment Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Second_Payment_Reminder</template>
    </alerts>
    <alerts>
        <fullName>COL_3D_Thank_you_Second_Payment_received</fullName>
        <description>COL 3D - Thank you - Second Payment received</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Thank_you_Second_Payment_received</template>
    </alerts>
    <alerts>
        <fullName>COL_4A_Missed_Payment_Restructuring_Fee</fullName>
        <description>COL 4A - Missed Payment - Restructuring Fee</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Missed_Payment_Restructuring_Fee</template>
    </alerts>
    <alerts>
        <fullName>COL_4B_Missed_Payment_Court_Fee</fullName>
        <description>COL 4B - Missed Payment - Court Fee</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Missed_Payment_Court_Fee</template>
    </alerts>
    <alerts>
        <fullName>COL_4C_Final_Warning_Missed_Fees</fullName>
        <description>COL 4C - Final Warning - Missed Fees</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Final_Warning_Missed_Fees</template>
    </alerts>
    <alerts>
        <fullName>COL_4D_First_Warning_CP_Default</fullName>
        <description>COL 4D - First Warning - CP Default</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/First_Warning_CP_Default</template>
    </alerts>
    <alerts>
        <fullName>COL_4E_Second_Warning_CP_Default</fullName>
        <description>COL 4E - Second Warning - CP Default</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Second_Warning_CP_Default</template>
    </alerts>
    <alerts>
        <fullName>COL_4F_Confirm_Payment_Arrangement</fullName>
        <description>COL 4F - Confirm Payment Arrangement</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Confirm_payment_arrangement</template>
    </alerts>
    <alerts>
        <fullName>COL_5A_Notice_of_Intention_to_Suspend</fullName>
        <description>COL 5A - Notice of Intention to Suspend</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Notice_of_Intention_to_Suspend</template>
    </alerts>
    <alerts>
        <fullName>COL_5B_Confirmation_of_Suspension</fullName>
        <description>COL 5B - Confirmation of Suspension</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Collections_Email_Templates/Confirmation_of_Suspension</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_Mag_1A_Serve_Application_on_CPs</fullName>
        <description>CP DC Mag 1A - Serve Application on CPs</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/CP_Serve_Application_on_CPs</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_Mag_1B_Send_Notice_of_Set_Down_to_CPs</fullName>
        <description>CP DC Mag 1B - Send Notice of Set Down to CPs</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/CP_Send_Notice_of_Set_Down_to_CPs</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_Mag_2A_Serve_Replying_Affidavit_on_Opposing_CP</fullName>
        <description>CP DC Mag 2A - Serve Replying Affidavit on Opposing CP</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/CP_Replying_Affidavit_to_Opposing_CP</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_Mag_3A_Send_Court_Order_to_CPs</fullName>
        <description>CP DC Mag 3A - Send Court Order to CPs</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/CP_Send_Court_Order_to_CPs</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_NCT_1A_Process</fullName>
        <description>CP DC NCT 1A - Process</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/CP_DC_NCT_1A_Notify_CPs_of_Application</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_NCT_2A_Process</fullName>
        <description>CP DC NCT 2A - Process</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/CP_DC_NCT_2A_Notify_CPs_Client_Withdrawa</template>
    </alerts>
    <alerts>
        <fullName>CP_DC_NCT_2B_Process</fullName>
        <description>CP DC NCT 2B - Process</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/CP_DC_NCT_2B_Notify_NCT_Client_Withdraws</template>
    </alerts>
    <alerts>
        <fullName>Cancellation_Debit_Order_Not_Signed_Email_Alert</fullName>
        <description>Cancellation Debit Order Not Signed Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Cancellation_Debit_Order_Not_Signed</template>
    </alerts>
    <alerts>
        <fullName>Client_Does_Not_Respond_to_Stop_Order_Details_Request_Email_Alert</fullName>
        <description>Client Does Not Respond to Stop Order Details Request Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Collections_No_Response_Stop_Order</template>
    </alerts>
    <alerts>
        <fullName>Client_Stop_Order_Not_Activated_Hyphen_Mandate_Not_Received_Email_Alert</fullName>
        <description>Client Stop Order Not Activated / Hyphen Mandate Not Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Client_Stop_Order_Not_Activated</template>
    </alerts>
    <alerts>
        <fullName>Collection_Documents_Not_Signed_Email_Alert</fullName>
        <description>Collection Documents Not Signed Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Collection_Documents_Not_Signed</template>
    </alerts>
    <alerts>
        <fullName>Consent_Order_Not_Received_Email</fullName>
        <description>Consent Order Not Received Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Consent_Order_Not_Received</template>
    </alerts>
    <alerts>
        <fullName>Court_first_follow_up_email</fullName>
        <description>Court first follow up email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG6</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_2A_Request_signed_Confirmatory_Affidavit_from_client_Alert</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC Mag 2A - Request signed Confirmatory Affidavit from client Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Request_signed_Confirmatory_Affidavit</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_2B_First_follow_up_Confirmatory_Affidavit</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC Mag 2B - First follow up - Confirmatory Affidavit</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/First_follow_up_Confirmatory_Affidavit</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_2C_Second_follow_up_Confirmatory_Affidavit</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC Mag 2C - Second follow up - Confirmatory Affidavit</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Second_follow_up_Confirmatory_Affidavit</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_2D_Suspending_File_No_Confirmatory_Affidavit</fullName>
        <description>DC Mag 2D - Suspending File - No Confirmatory Affidavit</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Suspending_File_No_Confirmatory</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_3A_Inform_client_of_Mag_Court_date</fullName>
        <description>DC Mag 3A  - Inform client of Mag Court date</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Inform_client_of_Mag_Court_date</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_3B_Inform_client_of_Opposing_CP</fullName>
        <description>DC Mag 3B - Inform client of Opposing CP</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Inform_client_of_Opposing_CP</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_4A_Court_Outcome_Postponed</fullName>
        <description>DC Mag 4A - Court Outcome - Postponed</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Court_Outcome_Postponed</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_4B_Court_Outcome_Successful_no_changes</fullName>
        <description>DC Mag 4B - Court Outcome - Successful (no changes)</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Court_Outcome_Successful_No_Changes</template>
    </alerts>
    <alerts>
        <fullName>DC_Mag_4C_Court_Outcome_Successful_with_changes</fullName>
        <description>DC Mag 4C - Court Outcome - Successful (with changes)</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Court_Outcome_Successful_With_Changes</template>
    </alerts>
    <alerts>
        <fullName>DC_NCT_1A_Process</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC NCT 1A – Process</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/DC_NCT_1A_Case_referred_to_NCT</template>
    </alerts>
    <alerts>
        <fullName>DC_NCT_2A_Process</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC NCT 2A – Process</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/DC_NCT_2A_Inform_client_of_NCT_date</template>
    </alerts>
    <alerts>
        <fullName>DC_NCT_3A_Process</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC NCT 3A – Process</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/DC_NCT_3A_NCT_Outcome_Postponed</template>
    </alerts>
    <alerts>
        <fullName>DC_NCT_3B_Process</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC NCT 3B – Process</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Tribunal_Templates/DC_NCT_3B_NCT_Outcome_Successful</template>
    </alerts>
    <alerts>
        <fullName>Final_Warning_No_Payment_Received_Email_Alert</fullName>
        <description>Final Warning No Payment Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Final_Warning_No_Payment</template>
    </alerts>
    <alerts>
        <fullName>Maps_to_CG2</fullName>
        <description>Maps to CG2</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG2</template>
    </alerts>
    <alerts>
        <fullName>Maps_to_CG3</fullName>
        <description>Maps to CG3</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG3</template>
    </alerts>
    <alerts>
        <fullName>Maps_to_CG4</fullName>
        <description>Maps to CG4</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CG_Attorney/Request_follow_up</template>
    </alerts>
    <alerts>
        <fullName>Maps_to_CG4_1_Docs_Requested_Follow_Up</fullName>
        <ccEmails>info@cg.co.za</ccEmails>
        <description>Maps to CG4-1 Docs Requested Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CG_Attorney/Request_follow_up</template>
    </alerts>
    <alerts>
        <fullName>Maps_to_CG4_2_Challenge_Sent_Follow_Up</fullName>
        <ccEmails>info@cg.co.za</ccEmails>
        <description>Maps to CG4-2 Challenge Sent Follow Up</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Legal_Court_CG4_2</template>
    </alerts>
    <alerts>
        <fullName>NCR_1B_First_follow_up_NCR_Mandate</fullName>
        <description>NCR 1B – First follow up – NCR Mandate</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_1B_First_follow_up_NCR_Mandate</template>
    </alerts>
    <alerts>
        <fullName>NCR_1C_Second_follow_up_NCR_Mandate</fullName>
        <description>NCR 1C – Second follow up – NCR Mandate</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_1C_Second_follow_up_NCR_Mandate</template>
    </alerts>
    <alerts>
        <fullName>NCR_1D_Closing_file_No_NCR_Mandate</fullName>
        <description>NCR 1D – Closing file – No NCR Mandate</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_1D_Closing_file_No_NCR_Mandate</template>
    </alerts>
    <alerts>
        <fullName>No_Mandate_Received_Send_Close_Reminder_to_Attorney</fullName>
        <description>No Mandate Received Send Close Reminder to Attorney</description>
        <protected>false</protected>
        <recipients>
            <field>Attorney_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/NCR_5_Mandate_Not_Received</template>
    </alerts>
    <alerts>
        <fullName>No_Payment_1st_Notification_Email_Alert</fullName>
        <description>No Payment 1st Notification  Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/No_Payment_1st_Notification_1</template>
    </alerts>
    <alerts>
        <fullName>Notify_After_Care</fullName>
        <description>Notify After Care</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Notify_After_Care</template>
    </alerts>
    <alerts>
        <fullName>Notify_Consultant_of_No_payment_to_close_process</fullName>
        <ccEmails>danie.booysen@tetrad.co.za</ccEmails>
        <description>Notify Consultant of No payment to close process</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Acc_Collections_Close_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Process_Owner</fullName>
        <description>Notify Process Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Created</template>
    </alerts>
    <alerts>
        <fullName>OMB_1B_First_follow_up</fullName>
        <description>OMB 1B – First follow up</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_1B_First_follow_up</template>
    </alerts>
    <alerts>
        <fullName>OMB_1C_Second_follow_up</fullName>
        <description>OMB 1C – Second follow up</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_1C_Second_follow_up</template>
    </alerts>
    <alerts>
        <fullName>OMB_1D_Closing_file</fullName>
        <description>OMB 1D – Closing file</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_1D_Closing_file</template>
    </alerts>
    <alerts>
        <fullName>OMB_2A_Docs_requested</fullName>
        <description>OMB 2A – Docs requested</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_2A_Docs_requested</template>
    </alerts>
    <alerts>
        <fullName>Prescription_2B_update</fullName>
        <description>Prescription 2B update</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Prescription_Templates/Pres_CP_1C_2nd_follow_up_Docs_request</template>
    </alerts>
    <alerts>
        <fullName>Process_Close_Notification_No_Payment_Received_Email_Alert</fullName>
        <description>Process Close Notification No Payment Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Close_Notification</template>
    </alerts>
    <alerts>
        <fullName>Process_Created_Audit_Court</fullName>
        <description>Process Created Audit Court</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Created</template>
    </alerts>
    <alerts>
        <fullName>Process_Created_DC_Court</fullName>
        <description>Process Created DC Court</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Created</template>
    </alerts>
    <alerts>
        <fullName>Process_Created_NCR</fullName>
        <description>Process Created NCR</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Created</template>
    </alerts>
    <alerts>
        <fullName>Process_Created_Tribunal_NCT</fullName>
        <description>Process Created Tribunal NCT</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Created</template>
    </alerts>
    <alerts>
        <fullName>RL_ADR_1A</fullName>
        <description>RL ADR 1A</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/RL_ADR_1A</template>
    </alerts>
    <alerts>
        <fullName>RL_ADR_2A</fullName>
        <description>RL ADR 2A</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/RL_ADR_2A_Loan_is_reckless_CP_Challenged</template>
    </alerts>
    <alerts>
        <fullName>RL_ADR_3A</fullName>
        <description>RL ADR 3A</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/RL_ADR_3A_ADR_outcome_Successful</template>
    </alerts>
    <alerts>
        <fullName>RL_ADR_3B</fullName>
        <description>RL ADR 3B</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/RL_ADR_3B</template>
    </alerts>
    <alerts>
        <fullName>Reminder_to_Appoint_Corresponding_Attorney</fullName>
        <description>Reminder to Appoint Corresponding Attorney</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>General_Email_Templates_Folder/Test_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Second_Fees_Not_Paid_Client_Notification_Email_Alert</fullName>
        <description>Second Fees Not Paid - Client Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Second_Fees_Not_Paid_Client_Notification</template>
    </alerts>
    <alerts>
        <fullName>Second_Fees_Not_Paid_Consultant_Notification_Email_Alert</fullName>
        <description>Second Fees Not Paid - Consultant Notification Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Second_Fees_Not_Paid_Consultant</template>
    </alerts>
    <alerts>
        <fullName>Serve_Credit_Providers</fullName>
        <description>Serve Credit Providers</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Serve_Credit_Providers</template>
    </alerts>
    <alerts>
        <fullName>Serve_Credit_Providers_with_Signed_CDO</fullName>
        <description>Serve Credit Providers with Signed CDO</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Serve_CP_s_with_Signed_CDO</template>
    </alerts>
    <alerts>
        <fullName>Suspension_Notification_No_Payment_Received_Email_Alert</fullName>
        <description>Suspension Notification No Payment Received Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Process_Suspension_Notification</template>
    </alerts>
    <alerts>
        <fullName>Testing_the_email_alert</fullName>
        <ccEmails>wayde.fagan@tetrad.co.za</ccEmails>
        <description>Testing the email alert for the process builder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>General_Email_Templates_Folder/Test_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Thank_client_for_fees_paid</fullName>
        <description>Thank client for fees paid</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>General_Email_Templates_Folder/Test_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Welcome_on_Mag_Court_Email_Alert</fullName>
        <ccEmails>bianca@summitfin.co.za</ccEmails>
        <description>DC Mag 1A - Working on Mag Court application</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>DC_Mag_Court_Email_Templates/Working_on_Mag_Court_application</template>
    </alerts>
    <alerts>
        <fullName>court_second_follow_up_email</fullName>
        <description>court second follow up email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG6</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG5</fullName>
        <description>maps to CG5</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG5</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG8</fullName>
        <description>maps to CG8</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/CG8</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4B</fullName>
        <description>maps to CG Request 4B</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_4B_Request_Letter_to_CP</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4C</fullName>
        <description>maps to CG Request 4C</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_CG_Follow_up_1_Request_4C</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4D</fullName>
        <description>maps to CG Request 4D</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_CG_Follow_up_2_Request_4D</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4E</fullName>
        <description>maps to CG Request 4E</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_CG_Follow_up_1_Challenge_4E</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4E_1</fullName>
        <description>maps to CG Request 4E 1</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_CG_Follow_up_1_Challenge_4E</template>
    </alerts>
    <alerts>
        <fullName>maps_to_CG_Request_4F</fullName>
        <description>maps to CG Request 4F</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Ombud_CG_Follow_up_2_Challenge_4F</template>
    </alerts>
    <alerts>
        <fullName>maps_to_Prescription_CP_1B</fullName>
        <description>maps to  Prescription CP 1B</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Prescription_Templates/Pres_CP_1B_1st_follow_up_Docs_request</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_1A</fullName>
        <description>maps to RL NCR 1A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_2A_NCR_Complaint_sent</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_1B</fullName>
        <description>maps to RL NCR 1B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_2B_Awaiting_NCR_Response</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_2A</fullName>
        <description>maps to RL Ombud 2A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_2B_Challenge_sent</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_2A_Savings_Outcome</fullName>
        <description>maps to RL NCR 2A Savings Outcome</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_3A_NCR_Saving_Reckless_lending</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_2B</fullName>
        <description>maps to RL NCR 2B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_3D_NCR_Non_Referral_Escalate_to_NCT</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_NCR_2C</fullName>
        <description>maps to RL NCR 2C</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_3E_Other_NCR_Response</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_1A</fullName>
        <description>maps to RL Ombud 1A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_1A_Start_Court</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_3A</fullName>
        <description>maps to RL Ombud 3A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_3A_Complaints_Dept_Outcome_Saving</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_3B</fullName>
        <description>maps to RL Ombud 3B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_3B_Complaints_Dept_outcome</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_3C</fullName>
        <description>maps to RL Ombud 3C</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_3C_Complaints_Dept_outcome</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_4A</fullName>
        <description>maps to RL Ombud 4A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_4A_Ombud_Complaint_sent</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_5A</fullName>
        <description>maps to RL Ombud 5A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_5A_Ombud_outcome_Successful</template>
    </alerts>
    <alerts>
        <fullName>maps_to_RL_Ombud_5B</fullName>
        <description>maps to RL Ombud 5B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ombud_Templates/OMB_5B_Ombud_outcome_Unsuccessful</template>
    </alerts>
    <alerts>
        <fullName>maps_to_prescription_2A_and_2B</fullName>
        <description>maps to prescription 2A and 2B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Prescription_Templates/Pres_2A_Confirmed_Prescription_Challenge</template>
    </alerts>
    <alerts>
        <fullName>maps_to_prescription_2B</fullName>
        <description>maps to prescription 2B</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Prescription_Templates/Prescription_2B_No_response_from_CP</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless</fullName>
        <description>maps to RL NCR 2A</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/RL_ADR_2A_Loan_is_reckless_CP_Challenged</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless_lending_5</fullName>
        <description>maps to reckless lending 5C</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Reckless_credit_5C_Second_follow_up_NCR</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless_lending_5A</fullName>
        <description>maps to reckless lending 5A</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bianca@summitfin.co.za.production</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>NCR_Templates/NCR_1A_Start_NCR</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless_lending_5B</fullName>
        <description>maps to reckless lending 5B</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Reckless_credit_5B_First_follow_up_NCR</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless_lending_5B_1</fullName>
        <description>maps to reckless lending 5B 1</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Reckless_credit_5B_First_follow_up_NCR</template>
    </alerts>
    <alerts>
        <fullName>maps_to_reckless_lending_5C_1</fullName>
        <description>maps to reckless lending 5C 1</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <field>Contact_Consultant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Reckless_credit_5C_Second_follow_up_NCR</template>
    </alerts>
    <alerts>
        <fullName>test</fullName>
        <ccEmails>wayde.fagan@tetrad.co.za</ccEmails>
        <ccEmails>danie.booysen@tetrad.co.za</ccEmails>
        <description>test</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Process_Email_Templates_Workflow/Account_Collections_Test_1</template>
    </alerts>
    <alerts>
        <fullName>testTemplate</fullName>
        <description>testTemplate</description>
        <protected>false</protected>
        <recipients>
            <recipient>implementation.user@summitfin.co.za</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>General_Email_Templates_Folder/Test_Email_Template_1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Cancellation_DO_Not_Signed_Field_Update</fullName>
        <field>Follow_up_on_Cancellation_of_Debit_Order__c</field>
        <formula>TODAY()</formula>
        <name>Cancellation DO Not Signed Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Challenge_Date_Follow_Up_1</fullName>
        <field>Date_First_Follow_Up_on_Challenge__c</field>
        <formula>TODAY()</formula>
        <name>Challenge Date Follow Up 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Challenge_Date_Follow_Up_2</fullName>
        <field>Date_Second_Follow_Up_on_Challenge__c</field>
        <formula>TODAY()</formula>
        <name>Challenge Date Follow Up 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Challenge_Folllow_Up_Date_Update_Court</fullName>
        <description>CG4-2</description>
        <field>Date_First_Follow_Up_on_Challenge__c</field>
        <formula>TODAY()</formula>
        <name>Challenge Folllow Up Date Update Court</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Challenge_Internal_Status_Update_Court</fullName>
        <description>CG4-2</description>
        <field>Internal_Status__c</field>
        <literalValue>First challenge follow up</literalValue>
        <name>Challenge Internal Status Update Court</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Collection_Documents_Date_Follow_up</fullName>
        <field>Follow_up_on_Collections_Documents__c</field>
        <formula>TODAY()</formula>
        <name>Collection Documents Date Follow up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Collection_Documents_Internal_Status</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Could Not Verify Documents</literalValue>
        <name>Collection Documents Internal Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Court_first_follow_up_14_days</fullName>
        <field>Date_Second_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>Court first follow up 14 days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Court_first_follow_up_7_days</fullName>
        <field>Date_First_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>Court first follow up 7 days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Affidavid_Sent</fullName>
        <field>Date_Affidavit_Sent_to_Client__c</field>
        <formula>TODAY()</formula>
        <name>Date Affidavid Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Request_Follow_Up_1</fullName>
        <field>Date_First_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>Date Request Follow Up 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Request_Follow_Up_2</fullName>
        <field>Date_Second_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>Date Request Follow Up 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Docs_Req_Folllow_Up_Date_Update</fullName>
        <description>CG4-1</description>
        <field>Date_First_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>Docs Req Folllow Up Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Docs_Req_Internal_Status_Update_Court</fullName>
        <description>CG4-1</description>
        <field>Internal_Status__c</field>
        <literalValue>First request follow up</literalValue>
        <name>Docs Req Internal Status Update Court</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Status_No_Response_DO</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Client Did Not Respond to Call to Create Stop Order</literalValue>
        <name>Internal Status No Response DO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Status_Update_Ombud_1</fullName>
        <field>Internal_Status__c</field>
        <literalValue>First request follow up</literalValue>
        <name>Internal Status Update Ombud 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Status_Update_Ombud_2</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Second request follow up</literalValue>
        <name>Internal Status Update Ombud 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Status_Update_Ombud_Challenge_1</fullName>
        <field>Internal_Status__c</field>
        <literalValue>First challenge follow up</literalValue>
        <name>Internal Status Update Ombud Challenge 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Internal_Status_Update_Ombud_Challenge_2</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Second challenge follow up</literalValue>
        <name>Internal Status Update Ombud Challenge 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mandate_Follow_up_1_Date_Update</fullName>
        <field>Date_First_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>Mandate Follow up 1 Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mandate_Follow_up_2_Date_Update</fullName>
        <field>Date_Second_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>Mandate Follow up 2 Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mandate_Sent_Date_Update</fullName>
        <field>Date_Mandate_Sent_To_Client__c</field>
        <formula>TODAY()</formula>
        <name>Mandate Sent Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mandate_Status_Update_1</fullName>
        <field>Internal_Status__c</field>
        <literalValue>First follow up to client</literalValue>
        <name>Mandate Status Update 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mandate_Status_Update_2</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Second follow up to client</literalValue>
        <name>Mandate Status Update 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NCR_Status_Update_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>NCR Status Update Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Acknowledgement_Received</fullName>
        <field>Acknowledgement_Received__c</field>
        <literalValue>1</literalValue>
        <name>Update Acknowledgement Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Update Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Acknowledgement_Received</fullName>
        <field>Date_Acknowledgement_Received__c</field>
        <formula>TODAY()</formula>
        <name>Update Date Acknowledgement Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Follow_on_Second_Fees_Paid_Field</fullName>
        <field>First_Reminder_on_Second_Payment__c</field>
        <formula>TODAY()</formula>
        <name>Update Follow on Second Fees Paid Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Process</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Update Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Second_Fees_Paid</fullName>
        <field>Internal_Status__c</field>
        <literalValue>Second Fees Not Paid</literalValue>
        <name>Update Second Fees Paid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Status__c</field>
        <literalValue>Acknowledgement Received</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>internal_status_first_follow_up</fullName>
        <field>Internal_Status__c</field>
        <literalValue>First Follow to Credit Provider on Challenge</literalValue>
        <name>internal status first follow up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>internal_status_update</fullName>
        <field>Internal_Status__c</field>
        <literalValue>First Follow Up</literalValue>
        <name>internal status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>maps_to_CG6</fullName>
        <field>Date_First_Follow_Up_Sent_on_Request__c</field>
        <formula>TODAY()</formula>
        <name>maps to CG6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>prescription_1st_follow_up</fullName>
        <field>Date_First_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>prescription 1st follow up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>prescription_CP_2A_date_update</fullName>
        <field>Date_First_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>prescription CP 2A date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>prescription_CP_2B_date_update</fullName>
        <field>Date_Second_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>prescription CP 2B date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>second_follow_up</fullName>
        <field>Date_Second_Follow_Up_with_Client__c</field>
        <formula>TODAY()</formula>
        <name>second follow up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Collections Notification</fullName>
        <actions>
            <name>Account_Collections_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Serve_Credit_Providers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Tribunal (NCT)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Application Fee Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Collections Process No Payment Notification</fullName>
        <actions>
            <name>test</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 4) AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - FWA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Client Stop Order Activated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Fees Not Paid</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - Onsite</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Final_Warning_No_Payment_Received_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>No_Payment_1st_Notification_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Notify_Consultant_of_No_payment_to_close_process</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Suspension_Notification_No_Payment_Received_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Appoint Corresponding Attorney Email Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - DC Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Affidavit Sent</value>
        </criteriaItems>
        <description>DC Court
An Internal email must be sent to consultant to remind them of appointing a corresponding attorney if not done within 3 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_to_Appoint_Corresponding_Attorney</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CG Follow up Challenge</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <description>Ombud. Sent when status is &quot;challenge sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_CG_Request_4E_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Challenge_Date_Follow_Up_1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Internal_Status_Update_Ombud_Challenge_1</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_CG_Request_4F</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Challenge_Date_Follow_Up_2</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Internal_Status_Update_Ombud_Challenge_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CG Follow up on request</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Documents requested</value>
        </criteriaItems>
        <description>Omnud Sent when status is &quot;Documents Request sent&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_CG_Request_4D</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Date_Request_Follow_Up_2</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Internal_Status_Update_Ombud_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_CG_Request_4C</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Date_Request_Follow_Up_1</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Internal_Status_Update_Ombud_1</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>CG Request 4B</fullName>
        <actions>
            <name>maps_to_CG_Request_4B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Documents requested</value>
        </criteriaItems>
        <description>Ombud. Sent when status is &quot;Documents requested&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Client did Respond to Debit Order Reminder Call</fullName>
        <actions>
            <name>Client_Does_Not_Respond_to_Stop_Order_Details_Request_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - FWA,Account Collections - Onsite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Client Did Not Respond to Call to Create Stop Order</value>
        </criteriaItems>
        <description>Client did not Respond After</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Collection Documents Not Signed Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - Onsite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Follow Up Email to the contact consultant</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Collection_Documents_Not_Signed_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Collection_Documents_Date_Follow_up</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Collection_Documents_Internal_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Consent Order Not Received</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Tribunal (NCT)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>NCT Court Documents Verified</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Consent_Order_Not_Received_Email</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Court Case Affidavit sent</fullName>
        <actions>
            <name>maps_to_CG5</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Date_Affidavid_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Affidavit Sent</value>
        </criteriaItems>
        <description>Email sent when affidavit is sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Court case affidavit received</fullName>
        <actions>
            <name>maps_to_CG8</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Affidavit Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Affidavit_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Court email is sent when affidavit is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DC Mag Court Follow Up On Affidavit Sent</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - DC Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Affidavit Sent</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>DC_Mag_2B_First_follow_up_Confirmatory_Affidavit</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>DC_Mag_2C_Second_follow_up_Confirmatory_Affidavit</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>DC_Mag_2D_Suspending_File_No_Confirmatory_Affidavit</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Debit Order Not Signed Rule</fullName>
        <actions>
            <name>Cancellation_Debit_Order_Not_Signed_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Cancellation_DO_Not_Signed_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Internal_Status_No_Response_DO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - FWA,Account Collections - Onsite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Client Uploaded to Simplicity</value>
        </criteriaItems>
        <description>Debit Order Not Signed and Client did not Respond After Due Date on Task Lapsed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Legal NCR - Complaint Submitted Ignored%2FRejected</fullName>
        <actions>
            <name>Attorney_Notification_Email_NCR_Ingnored_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Complaint Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Ignored by NCR,Rejected by NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Acknowledgement_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends a reminder email to the attorney when the NCR process is Ignored or Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NCR Acknowledgement Received</fullName>
        <actions>
            <name>Update_Acknowledgement_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Date_Acknowledgement_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Acknowledgement Received</value>
        </criteriaItems>
        <description>Updates Fields When NCR Acknowledge is received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NCR Reckless Lending 5A</fullName>
        <actions>
            <name>maps_to_reckless_lending_5A</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Mandate_Sent_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Mandate Sent To Client</value>
        </criteriaItems>
        <description>Sent when mandate is sent to client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NCR reckless credit 5B</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Mandate Sent To Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Mandate_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sent when Mandate is sent to client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_reckless_lending_5B_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Mandate_Follow_up_1_Date_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Mandate_Status_Update_1</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>maps_to_reckless_lending_5C_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Mandate_Follow_up_2_Date_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Mandate_Status_Update_2</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>No_Mandate_Received_Send_Close_Reminder_to_Attorney</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>NCR_Status_Update_Closed</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Notify After Care</fullName>
        <actions>
            <name>Notify_After_Care</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Process</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Tribunal (NCT)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Client Withdrew Before Court Date Received,Client Withdrew After Court Date Received</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify Process Owner</fullName>
        <actions>
            <name>Notify_Process_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - DC Court</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RL ADR challenge sent</fullName>
        <actions>
            <name>RL_ADR_2A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - ADR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <description>Sends email when challenge is sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL ADR documents requested</fullName>
        <actions>
            <name>RL_ADR_1A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - ADR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Documents requested</value>
        </criteriaItems>
        <description>Email is sent to Contact and Contact Consultant when status = documents requested and recordType = ADR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL ADR savings received</fullName>
        <actions>
            <name>RL_ADR_3A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - ADR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Savings Received</value>
        </criteriaItems>
        <description>Email is sent when savings are received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL ADR status referred to NCT</fullName>
        <actions>
            <name>RL_ADR_3B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - ADR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred To NCT</value>
        </criteriaItems>
        <description>Sent when ADR status is referred to NCT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL NCR 1A</fullName>
        <actions>
            <name>maps_to_RL_NCR_1A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Complaint Submitted</value>
        </criteriaItems>
        <description>NCR. Sent when complains are submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL NCR 1B</fullName>
        <actions>
            <name>maps_to_RL_NCR_1B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Acknowledgement Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Acknowledgement_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>NCR. Sent when an Acknowledgement is submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL NCR 2A</fullName>
        <actions>
            <name>maps_to_reckless</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Savings Received</value>
        </criteriaItems>
        <description>NCR. Sent when Savings are received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL NCR 2B</fullName>
        <actions>
            <name>maps_to_RL_NCR_2B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred To NCT</value>
        </criteriaItems>
        <description>NCR. Sent when case is referred to NCT</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL NCR 2C</fullName>
        <actions>
            <name>maps_to_RL_NCR_2C</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - NCR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>NCR. Sent when case is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 1A WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_1A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Documents requested</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when documents are requested</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 2A WF</fullName>
        <actions>
            <name>maps_to_RL_NCR_2A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when Challenge is sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 3A WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_3A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Savings Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when savings are received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 3B WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_3B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Credit provider rejected</value>
        </criteriaItems>
        <description>Ombud - Sent when challenge is sent and internal status is &quot;credit provider rejected&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 3C WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_3C</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Internal_Status__c</field>
            <operation>equals</operation>
            <value>Credit Provider Ignored</value>
        </criteriaItems>
        <description>Ombud. Sent when Challenge is sent and internal status is &quot;Credit provider ignored&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 4A WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_4A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Complaint Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when Complaints are submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 5A WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_5A</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Savings Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when Savings are received</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RL Ombud 5B WF</fullName>
        <actions>
            <name>maps_to_RL_Ombud_5B</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Referred To NCT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Ombudsman</value>
        </criteriaItems>
        <description>Ombud. Sent when case is referred to NCR</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Second Fees Paid</fullName>
        <actions>
            <name>Second_Fees_Not_Paid_Client_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Second_Fees_Not_Paid_Consultant_Notification_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Follow_on_Second_Fees_Paid_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Second_Fees_Paid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Collections - FWA,Account Collections - Onsite</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>First Fees Paid</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>court case challenge sent follow up</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Challenge Sent</value>
        </criteriaItems>
        <description>Court email sent for challenge sent follow up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Maps_to_CG4_2_Challenge_Sent_Follow_Up</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Challenge_Folllow_Up_Date_Update_Court</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Challenge_Internal_Status_Update_Court</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>court case commissioning follow up on afidavid sent</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Commissioning</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Affidavit_Received__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Court. Sent when status is commissioning. Follow Up for Court Case Affidavit sent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Court_first_follow_up_email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Court_first_follow_up_7_days</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>court_second_follow_up_email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Court_first_follow_up_14_days</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>court case documents requested follow up</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Documents requested</value>
        </criteriaItems>
        <description>Court email sent when documents are requested follow up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Maps_to_CG4_1_Docs_Requested_Follow_Up</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Docs_Req_Folllow_Up_Date_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Docs_Req_Internal_Status_Update_Court</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>court case drafting affidavit</fullName>
        <actions>
            <name>Maps_to_CG3</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Drafting Affidavit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <description>Court. sent when Status is &quot;drafting affidavit&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>court case status update</fullName>
        <actions>
            <name>Maps_to_CG2</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Process__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Legal - Court</value>
        </criteriaItems>
        <criteriaItems>
            <field>Process__c.Status__c</field>
            <operation>equals</operation>
            <value>Open,Documents requested,Challenge Sent,Un Opposed,Opposed,Drafting Reply,Set Down,Argued,Judgement Received,Assessment in process,Serving,Served</value>
        </criteriaItems>
        <description>Sent when status is updated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>

<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Unverified_Email_Alert</fullName>
        <ccEmails>kuda@summitfin.co.za</ccEmails>
        <ccEmails>Bianca@summitfin.co.za</ccEmails>
        <ccEmails>misodzi@summitfin.co.za</ccEmails>
        <description>Account Unverified</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Account_Object_Templates/Account_Unverified</template>
    </alerts>
</Workflow>

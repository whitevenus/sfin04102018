<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>testAlert</fullName>
        <description>testAlert</description>
        <protected>false</protected>
        <recipients>
            <field>Credit_Provider_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>General_Email_Templates_Folder/testAlert</template>
    </alerts>
</Workflow>
